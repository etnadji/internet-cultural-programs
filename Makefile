.PHONY: all clean OPML README markdown

all: OPML README markdown

opml: reco.opml

clean:
	-rm README.md
	-rm Emissions.md

OPML:
	python3 opml.py
	python3 opml.py audio
	python3 opml.py video

README:
	xmlstarlet tr readme.xsl reco.xml > README.md

markdown:
	xmlstarlet tr markdown.xsl reco.xml > Emissions.md
