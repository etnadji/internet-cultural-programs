# Émissions culturelles sur Internet

## À propos

### Contenu

Ce document a pour ambition de lister la plupart des émissions
culturelles, ou tout du moins « sérieuses » présentes
sur Internet.

À ce jour, 413 émissions
font partie de cette liste. Dont 51 en
langue étrangère.

#### Langues

- Français : 361
- Anglais : 50
- Esperanto : 1

### Legende

Ce document n’est qu’une liste d’émissions avec un nombre variable de liens 
en rapport avec chaque émission.

Pour s’y retrouver, chaque lien est préfixé d’une icone.

#### Icones

- ![icone web](img/firefox.png) Sites web divers.

- ![icone rss](img/rss.png) Flux [RSS](http://sametmax.com/quest-ce-quun-flux-rss-et-a-quoi-ca-sert/) seul.

-  Youtube, liens directs vers de la vidéo.

-  Compte Twitter.

-  Moyens de soutenir l’émission: Tipee, vente de T-Shirts, livres…

### OPML

Une version OPML, mise à jour de temps en temps est 
disponible [ici](http://etnadji.fr/emissions/reco.opml).

Cette version exclut tout les liens ne pointant pas vers du RSS mais
comporte des liens RSS créés automatiquement à partir des
adresses de chaines Youtube.

Pour une obtenir une version  OPML forcément à jour, téléchargez
[le fichier XML](http://etnadji.fr/emissions/reco.xml) et transformez 
le à l’aide de [ce script Python](http://etnadji.fr/emissions/toOpml.py).

<hr/>

Dernière mise à jour : 15/12/2024.

<hr/>

## Sommaire


- [Arts](#arts)  171
    - [Architecture](#archi)  2
    - [Arts appliqués](#appli)  3
    - [Arts plastiques](#plast)  13
    - [Cinéma](#cinema)  45
    - [Création](#creative)  5
    - [Divers](#artsMisc)  8
    - [Gastronomie](#cuisine)  3
    - [Jeu-video](#jv)  15
    - [Littérature](#litterature)  21
    - [Musique](#musique)  35
    - [Séries](#series)  8
    - [Thématiques](#thematiques)  5
    - [Théâtre et jeu de rôle](#theajdr)  8

- [Collectifs](#collectifs)  9

- [Collectifs institutionnels](#institutionnels)  25

- [Culture web](#webculture)  2

- [Divers](#misc)  5

- [Politique](#politique)  33
    - [Droit](#droit)  8
    - [Philosophie](#philo)  8
    - [Scepticisme](#zetetique)  16
    - [Urbanisme](#urbanisme)  1

- [Sciences humaines](#schum)  87
    - [Anthropologie](#anthropo)  2
    - [Divers](#schum-misc)  3
    - [Ethnologie](#ethnologie)  2
    - [Géographie](#geo)  4
    - [Histoire / Archéologie](#histoire)  56
    - [Informatique](#compute1)  2
    - [Linguistique](#ling)  11
    - [Sociologie](#sociologie)  7

- [Sciences naturelles](#scnat)  81
    - [Biologie / Paléontologie](#biopaleo)  18
    - [Divers](#sciencemisc)  24
    - [Mathématiques](#maths)  5
    - [Médecine](#med)  7
    - [Physique / Astronomie](#cosmos)  16
    - [Psychologie](#psycho)  6
    - [Écologie](#ecologie)  1
    - [Électronique](#elec)  2
    - [Éthologie](#ethologie)  2

<hr/>

## Arts {#arts} 



### Architecture {#archi}


#### ARCHITEKTON {#emArchitekton} 

> 
	  Vous rêvez de comprendre l’architecture qui vous entoure, mais vous 
	  pensez que le sujet est ennuyant, trop complexe ou élitiste ? 
	  N’ayez crainte, j’ai LA solution. ARCHITEKTON, c’est la chaîne 
	  [YouTube] de vulgarisation sur l’architecture, où l’on va aborder le 
	  sujet sur plein d’angles différents : l’art, l’histoire, les 
	  matériaux, l’environnement, la philosophie, l’urbanisme, le paysage, 
	  la sociologie…
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC6RBa1Z16M0Yomt6FhKkH_g](https://www.youtube.com/channel/UC6RBa1Z16M0Yomt6FhKkH_g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC6RBa1Z16M0Yomt6FhKkH_g)

#### Le Nouveau Programme {#emLeNouveauProgramme} 

> 
	  [U]ne émission de vulgarisation architecturale qui a pour ambition de 
	  diffuser et promouvoir le patrimoine architectural du XXe siècle.
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCsUOOQLIXLNbQ9oXyIbvWVg](https://www.youtube.com/channel/UCsUOOQLIXLNbQ9oXyIbvWVg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCsUOOQLIXLNbQ9oXyIbvWVg)

### Arts appliqués {#appli}


#### Philobjet {#emPhilobjet} 

> Des émissions sur le design / la conception d’objets

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCRsW-j1P1z--sOejIRazDUw](https://www.youtube.com/channel/UCRsW-j1P1z--sOejIRazDUw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCRsW-j1P1z--sOejIRazDUw)

#### Attention, ça glyphe ! {#emACGlyphe} 

> Émission sur la typographie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCVcg0ify3ghouZVCYJMpDSg](https://www.youtube.com/channel/UCVcg0ify3ghouZVCYJMpDSg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCVcg0ify3ghouZVCYJMpDSg)

#### L’Océan des cent typos {#emOceanTypos} 

> Un podcast scénarisé, court et drôle, sur les typos.

Émission 
audio
.

- ![icone rss](img/rss.png) [Flux RSS / Atom (Soundcloud)](http://feeds.soundcloud.com/users/soundcloud:users:335518136/sounds.rss)- ![icone web](img/firefox.png) [https://soundcloud.com/studio-malomalo](https://soundcloud.com/studio-malomalo)  [![rss](img/rss.png)](https://soundcloud.com/studio-malomalo)

### Arts plastiques {#plast}


#### Esthèt’geek {#emEsthetGeek} 

> 
	  Concepts d’arts plastiques expliqués à travers des œuvres d’art et plus 
	  particulièrement des jeux-vidéos.
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/SamaiCedlart](https://www.youtube.com/user/SamaiCedlart) [RSS](https://www.youtube.com/feeds/videos.xml?user=SamaiCedlart)

#### L’Aparté {#emAparte} 

> Analyse d’œuvres d’art contemporaines.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/SamaiCedlart](https://www.youtube.com/user/SamaiCedlart) [RSS](https://www.youtube.com/feeds/videos.xml?user=SamaiCedlart)

#### Na’R’t {#emNart} 

> Émission sur l’art contemporain et quelques portraits d’artistes.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCQq9fMRQhXOyOZeageaj6ag](https://www.youtube.com/channel/UCQq9fMRQhXOyOZeageaj6ag) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCQq9fMRQhXOyOZeageaj6ag)

#### ComiXrayS {#emComixXRays} 

> Émission sur les comics.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/comixrays](https://www.tipeee.com/comixrays)- ![icone web](img/firefox.png) [http://www.comicsblog.fr/rdv.php?type=v](http://www.comicsblog.fr/rdv.php?type=v)  [![rss](img/rss.png)](http://www.comicsblog.fr/rdv.php?type=v)
- ![icone media](img/media.png) [youtube.com/user/ComiXrayS](https://www.youtube.com/user/ComiXrayS) [RSS](https://www.youtube.com/feeds/videos.xml?user=ComiXrayS)

#### Nothern Rufio {#emNorthernRufio} 

> Émission sur les mangas.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://northernrufio.blogspot.com/](http://northernrufio.blogspot.com/)  [![rss](img/rss.png)](http://northernrufio.blogspot.com/feeds/posts/default)
- ![icone media](img/media.png) [youtube.com/user/northernrufio](https://www.youtube.com/user/northernrufio) [RSS](https://www.youtube.com/feeds/videos.xml?user=northernrufio)

#### REG’art {#emRegArt} 

> Émission sur l’histoire de l’art (des beaux-arts).

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCjTZULESrd3WLIEhs6rfMQw](https://www.youtube.com/channel/UCjTZULESrd3WLIEhs6rfMQw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCjTZULESrd3WLIEhs6rfMQw)

#### Muséonaute {#emMuseonaute} 

> Les grands thèmes de l’histoire de l’art (beaux-arts).

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCZ17ceLrs__ZzLwx8IrEFkw](https://www.youtube.com/channel/UCZ17ceLrs__ZzLwx8IrEFkw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCZ17ceLrs__ZzLwx8IrEFkw)

#### The Art Assignment {#emArtAssignment} 

> L’émission va à la rencontre d’artistes pour qu’ils demandent des « devoirs » au spectateur.

Émission en 
anglais
.

Émission 
vidéo
.

Produit par PBS Digital Studio.

- ![icone web](img/firefox.png) [http://theartassignment.com/](http://theartassignment.com/)  [![rss](img/rss.png)](http://theartassignment.com/)- ![icone web](img/firefox.png) [http://theartassignment.tumblr.com/](http://theartassignment.tumblr.com/)  [![rss](img/rss.png)](http://theartassignment.tumblr.com/rss/)
- ![icone media](img/media.png) [youtube.com/user/theartassignment](https://www.youtube.com/user/theartassignment) [RSS](https://www.youtube.com/feeds/videos.xml?user=theartassignment)

#### Sous la toile {#emSLToile} 

> Sur la peinture

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCAo50V_d5wR3_boc6-rIb4Q](https://www.youtube.com/channel/UCAo50V_d5wR3_boc6-rIb4Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCAo50V_d5wR3_boc6-rIb4Q)

#### SOS Art {#emSOSA} 

> [D]es vidéos de vulgarisation portant sur l’art et sur les théories esthétiques

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCzi49Gp-XX9m5foHOWWGKeA](https://www.youtube.com/channel/UCzi49Gp-XX9m5foHOWWGKeA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCzi49Gp-XX9m5foHOWWGKeA)

#### Derrière le Masque ! {#emDerriereMasque} 

> Les effets spéciaux au cinéma, et plus particulièrement les monstres, par un maquilleur pro.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCow2gT3meZphlG8D2sa9nig](https://www.youtube.com/channel/UCow2gT3meZphlG8D2sa9nig) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCow2gT3meZphlG8D2sa9nig)

#### Funambulle {#emFunambulle} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC2PSS1ny6wjhUIR6M7DaLKg](https://www.youtube.com/channel/UC2PSS1ny6wjhUIR6M7DaLKg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC2PSS1ny6wjhUIR6M7DaLKg)

#### Art Comptant Pour Rien {#emArtComptantRien} 

> L’Art Contemporain c’est chouette (promis)

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/artcomptant](https://www.tipeee.com/artcomptant)
- ![icone media](img/media.png) [youtube.com/channel/UCL1ScW5p_7TF-uWdVo1WjTw](https://www.youtube.com/channel/UCL1ScW5p_7TF-uWdVo1WjTw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCL1ScW5p_7TF-uWdVo1WjTw)

### Cinéma {#cinema}


#### CGM {#emCGM} 

> Histoire des effets spéciaux au cinéma.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/gorkab](https://www.tipeee.com/gorkab)- ![icone soutien](img/money.png) [http://www.tostadora.fr/gorkab](http://www.tostadora.fr/gorkab)- ![icone web](img/firefox.png) [http://gorkabnitrix.com/](http://gorkabnitrix.com/)  [![rss](img/rss.png)](http://gorkabnitrix.com/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCL9j5fepWlAN3yx1LuYj1LA](https://www.youtube.com/channel/UCL9j5fepWlAN3yx1LuYj1LA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCL9j5fepWlAN3yx1LuYj1LA)

#### Parlons VF {#emParlonsVF} 

> Émission consacrée au doublage cinéma/série/jeux-vidéos.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/ParlonsVF](https://www.youtube.com/user/ParlonsVF) [RSS](https://www.youtube.com/feeds/videos.xml?user=ParlonsVF)
- ![icone media](img/media.png) [youtube.com/user/TheRealMisterfox](https://www.youtube.com/user/TheRealMisterfox) [RSS](https://www.youtube.com/feeds/videos.xml?user=TheRealMisterfox)

#### Yohann from outer space {#emYFOuterSpace} 

> Chaine consacrée aux nanards du cinéma et certaines notions du septième art.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.erenouvelle.com/DOSSAGAR.HTM](http://www.erenouvelle.com/DOSSAGAR.HTM)  [![rss](img/rss.png)](http://www.erenouvelle.com/DOSSAGAR.HTM)
- ![icone media](img/media.png) [youtube.com/channel/UCw9pOgsObfIcsrZ5n-pQq_g](https://www.youtube.com/channel/UCw9pOgsObfIcsrZ5n-pQq_g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCw9pOgsObfIcsrZ5n-pQq_g)

#### C’est pas si mal ! {#emCPSMal} 

> Présentation de bons et vieux films.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/eliot-mini-le-cinephile](https://www.tipeee.com/eliot-mini-le-cinephile)- ![icone web](img/firefox.png) [http://elfarfadosh.tumblr.com/](http://elfarfadosh.tumblr.com/)  [![rss](img/rss.png)](http://elfarfadosh.tumblr.com/rss)
- ![icone media](img/media.png) [youtube.com/user/ElFarfadosh](https://www.youtube.com/user/ElFarfadosh) [RSS](https://www.youtube.com/feeds/videos.xml?user=ElFarfadosh)

#### Le Ciné-Club {#emCineClub} 

> Émission à thème sur le cinéma.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC26kpT_AaJz8fxmAXtaGpdg](https://www.youtube.com/channel/UC26kpT_AaJz8fxmAXtaGpdg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC26kpT_AaJz8fxmAXtaGpdg)

#### Les chroniques de Vesper {#emChroniquesVesper} 

> Chroniques sur des films et des séries.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/ChroniquesDeVesper](https://www.youtube.com/user/ChroniquesDeVesper) [RSS](https://www.youtube.com/feeds/videos.xml?user=ChroniquesDeVesper)

#### La séance infuse {#emSeanceInfuse} 

> Des "après-séances" sur les sorties récentes.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.fabiencampaner.fr/](http://www.fabiencampaner.fr/)  [![rss](img/rss.png)](http://www.fabiencampaner.fr/)
- ![icone media](img/media.png) [youtube.com/user/betatestmode](https://www.youtube.com/user/betatestmode) [RSS](https://www.youtube.com/feeds/videos.xml?user=betatestmode)

#### On va faire cours {#emOVFCours} 

> Les clichés de l’Histoire au cinéma.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.fabiencampaner.fr/](http://www.fabiencampaner.fr/)  [![rss](img/rss.png)](http://www.fabiencampaner.fr/)
- ![icone media](img/media.png) [youtube.com/user/betatestmode](https://www.youtube.com/user/betatestmode) [RSS](https://www.youtube.com/feeds/videos.xml?user=betatestmode)

#### Les Clichés Ca Fait Ch### {#emClichesChiii} 

> Les clichés au cinéma.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCN2fQteC9VUUK6JmjVGr49g](https://www.youtube.com/channel/UCN2fQteC9VUUK6JmjVGr49g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCN2fQteC9VUUK6JmjVGr49g)

#### Retour vers la culture {#emRetourCulture} 

> Présentation de films plus ou moins connus.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCG-D1kb2y3HZ5m62aGcAOqg](https://www.youtube.com/channel/UCG-D1kb2y3HZ5m62aGcAOqg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCG-D1kb2y3HZ5m62aGcAOqg)

#### Le ciné-club de M.Bobine {#emMBobine} 

> Le ciné-club de M. Bobine est une émission sur le cinéma, réalisée entièrement en animation.  A travers des analyses critiques et passionnées, M. Bobine revient sur la conception, la réception et l’héritage des films qui ont marqué l’histoire du cinéma, mais aussi, à l’occasion d’épisodes spéciaux, sur la carrière de cinéastes majeurs du septième Art.  Le futur examiné au crible des œuvres de science-fiction, entre rationnalité et imaginaire.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/le-cine-club-de-m-bobine](https://www.tipeee.com/le-cine-club-de-m-bobine)- ![icone web](img/firefox.png) [http://www.senscritique.com/serie/Le_cine_club_de_M_Bobine/16870773](http://www.senscritique.com/serie/Le_cine_club_de_M_Bobine/16870773)  [![rss](img/rss.png)](http://www.senscritique.com/serie/Le_cine_club_de_M_Bobine/16870773)
- ![icone media](img/media.png) [youtube.com/channel/UCMAOGbM65D7VzbVTmpKeKgQ](https://www.youtube.com/channel/UCMAOGbM65D7VzbVTmpKeKgQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCMAOGbM65D7VzbVTmpKeKgQ)

#### Écran d’arrêt {#emEcranArret} 

> Émission sur le cinéma orientée technique.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCHk90jfHlLmr-4EbFLIaOZg](https://www.youtube.com/channel/UCHk90jfHlLmr-4EbFLIaOZg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCHk90jfHlLmr-4EbFLIaOZg)

#### Je vais ruiner votre enfance {#emJVRVE} 

> Émission consacré aux films en dessin animé.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCJu_M0LLtv8dMAFHhMHQdww](https://www.youtube.com/channel/UCJu_M0LLtv8dMAFHhMHQdww) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCJu_M0LLtv8dMAFHhMHQdww)

#### CHROMA {#emCHROMA} 

> Chronique de cinéma

Émission 
vidéo
.


- ![icone media](img/media.png) [dailymotion.com/karimdebbache](https://www.dailymotion.com/karimdebbache)  [RSS](https://www.dailymotion.com/rss/user/karimdebbache)


#### Alice in animation {#emAliceAnimation} 

> Chaine de vulgarisation sur l’animation

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCjn3Vpi26TfIu1S5vkWC4oQ](https://www.youtube.com/channel/UCjn3Vpi26TfIu1S5vkWC4oQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCjn3Vpi26TfIu1S5vkWC4oQ)

#### PODSAC {#emPODSAC} 

> Le Podcast Sérieusement Accro au Cinéma.

Émission 
audio
.

- ![icone rss](img/rss.png) [](http://feeds.djpod.com/podsac)- ![icone web](img/firefox.png) [https://djpod.com/podsac](https://djpod.com/podsac)  [![rss](img/rss.png)](https://djpod.com/podsac)

#### L’envers du raccord {#emEnversRaccord} 

> Cinéma, films, critiques, horreur, jeux de rôle

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/gillus](https://www.tipeee.com/gillus)- ![icone web](img/firefox.png) [http://www.lesuricate.org/author/binot/](http://www.lesuricate.org/author/binot/)  [![rss](img/rss.png)](http://www.lesuricate.org/author/binot/feed/)
- ![icone media](img/media.png) [youtube.com/user/Gillus99](https://www.youtube.com/user/Gillus99) [RSS](https://www.youtube.com/feeds/videos.xml?user=Gillus99)

#### Animétrage {#emAnimetrage} 

> Émission sur l’animation/le dessin animé en long métrage.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCQKBosbvkCSuNXcmk9wdl7w](https://www.youtube.com/channel/UCQKBosbvkCSuNXcmk9wdl7w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCQKBosbvkCSuNXcmk9wdl7w)

#### Confessons un film {#emConfessonsFilm} 

> 

Émission 
vidéo
.

Produit par Loki Jackal.


- ![icone media](img/media.png) [youtube.com/user/LokiJackal](https://www.youtube.com/user/LokiJackal) [RSS](https://www.youtube.com/feeds/videos.xml?user=LokiJackal)

#### God Awful Movies {#emGodAwfulMovies} 

> Critique de (mauvais) films à thèse(s) religieuse(s).

Émission en 
anglais
.

Émission 
audio
.

- ![icone rss](img/rss.png) [](http://godawfulmovies.libsyn.com/rss)- ![icone web](img/firefox.png) [http://godawfulmovies.libsyn.com/](http://godawfulmovies.libsyn.com/)  [![rss](img/rss.png)](http://godawfulmovies.libsyn.com/rss)

#### Atheists Watch {#emAtheistsWatch} 

> Critique de (mauvais) films à thèse(s) religieuse.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/TBR](https://www.patreon.com/TBR)
- ![icone media](img/media.png) [youtube.com/user/TheBibleReloaded](https://www.youtube.com/user/TheBibleReloaded) [RSS](https://www.youtube.com/feeds/videos.xml?user=TheBibleReloaded)
- ![icone media](img/media.png) [youtube.com/channel/UC1gSxXwxoIxIt9doAapuueg](https://www.youtube.com/channel/UC1gSxXwxoIxIt9doAapuueg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC1gSxXwxoIxIt9doAapuueg)

#### Mange tes navets {#emMTNavets} 

> Films nanards et navets.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCN8jtQ5bmkCVCES81VckKBQ](https://www.youtube.com/channel/UCN8jtQ5bmkCVCES81VckKBQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCN8jtQ5bmkCVCES81VckKBQ)

#### Chroniques du Désert Rouge {#emCDDR} 

> Chronique de critique de cinéma.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC9ba8F0t-wFiYUV-7ogVITw](https://www.youtube.com/channel/UC9ba8F0t-wFiYUV-7ogVITw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC9ba8F0t-wFiYUV-7ogVITw)

#### Fake Blood {#emFakeBlood} 

> Émission sur la violence au cinéma

Émission 
vidéo
.

Produit par YohannBordello.


- ![icone media](img/media.png) [youtube.com/channel/UCQLoP9kPZ92BeyqEyVH56BQ](https://www.youtube.com/channel/UCQLoP9kPZ92BeyqEyVH56BQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCQLoP9kPZ92BeyqEyVH56BQ)

#### Cinévif {#emCineVif} 

> Critique de films

Émission 
vidéo
.

Produit par YohannBordello.


- ![icone media](img/media.png) [youtube.com/channel/UCQLoP9kPZ92BeyqEyVH56BQ](https://www.youtube.com/channel/UCQLoP9kPZ92BeyqEyVH56BQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCQLoP9kPZ92BeyqEyVH56BQ)

#### Every Frame a Painting {#emEFAP} 

> Every Frame a Painting is dedicated to the analysis of film form.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/everyframeapainting](https://www.patreon.com/everyframeapainting)- ![icone web](img/firefox.png) [http://everyframeapainting.tumblr.com/](http://everyframeapainting.tumblr.com/)  [![rss](img/rss.png)](http://everyframeapainting.tumblr.com/rss)
- ![icone media](img/media.png) [youtube.com/user/everyframeapainting](https://www.youtube.com/user/everyframeapainting) [RSS](https://www.youtube.com/feeds/videos.xml?user=everyframeapainting)

#### Now you see it {#emNYSIt} 

> 
	  Now You See It explores film themes and tropes. It's like a college 
	  film analysis class minus the lecture halls, essay assignments, and 
	  student loan debts.
	

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/nowyouseeit](https://www.patreon.com/nowyouseeit)
- ![icone media](img/media.png) [youtube.com/channel/UCWTFGPpNQ0Ms6afXhaWDiRw](https://www.youtube.com/channel/UCWTFGPpNQ0Ms6afXhaWDiRw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCWTFGPpNQ0Ms6afXhaWDiRw)

#### Nicolas Martigne {#emNMartigne} 

> Sur le sound-design.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/NiKeMoOk](https://www.youtube.com/user/NiKeMoOk) [RSS](https://www.youtube.com/feeds/videos.xml?user=NiKeMoOk)

#### Le Tost’ {#emTost} 

> Fictions, émissions sur le cinéma et l’écriture de scénarios.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/le-tost](https://www.tipeee.com/le-tost)
- ![icone media](img/media.png) [youtube.com/channel/UCbbsbAqC2uYvyPX56TCBLrw](https://www.youtube.com/channel/UCbbsbAqC2uYvyPX56TCBLrw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCbbsbAqC2uYvyPX56TCBLrw)

#### Total Remake {#emKorewen} 

> L’émission de découverte "Total Remake" [est] dédiée à l’histoire du cinéma et plus précisément aux remakes, mais aussi des courts-métrages originaux, des clips vidéos et pleins d’autres projets.

Émission 
vidéo
.

Produit par Studio KOREWEN.

- ![icone soutien](img/money.png) [tipeee.com/total-remake](https://www.tipeee.com/total-remake)
- ![icone media](img/media.png) [youtube.com/channel/UCiZtAqN-MPqkFg4uJiZbDpw](https://www.youtube.com/channel/UCiZtAqN-MPqkFg4uJiZbDpw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCiZtAqN-MPqkFg4uJiZbDpw)

#### L’archiviste du cinéma {#emArchivisteCinema} 

> Chroniques sur le cinéma et projections de nanars.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCntDC5Hp_XYNy-7shfkUtnw](https://www.youtube.com/channel/UCntDC5Hp_XYNy-7shfkUtnw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCntDC5Hp_XYNy-7shfkUtnw)

#### Scinéma {#emScinema} 

> Vulgarisation scientifique et technique à partir du cinéma

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/scinema](https://www.tipeee.com/scinema)- ![icone web](img/firefox.png) [https://jbscinema.wordpress.com/](https://jbscinema.wordpress.com/)  [![rss](img/rss.png)](https://jbscinema.wordpress.com/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCPF7JOWMDWZmnP_cQfyAezg](https://www.youtube.com/channel/UCPF7JOWMDWZmnP_cQfyAezg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCPF7JOWMDWZmnP_cQfyAezg)

#### La manie du cinéma {#emManieCinema} 

> Cinq émissions : Cinécanapé, Cinédivan, #PassionMakingOf, MASH-UP, Cinéphilons avec.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [utip.io/https://www.utip.io/lamanieducinema](https://utip.io/https://www.utip.io/lamanieducinema)
- ![icone media](img/media.png) [youtube.com/channel/UCJVtE8BwsjVxt6W-lTA-8sA](https://www.youtube.com/channel/UCJVtE8BwsjVxt6W-lTA-8sA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCJVtE8BwsjVxt6W-lTA-8sA)

#### RIGOLO {#emCalmos} 

> Analyse de comédies françaises.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.calmos.net/](http://www.calmos.net/)  [![rss](img/rss.png)](http://www.calmos.net/)
- ![icone media](img/media.png) [youtube.com/channel/UCYBr8enT5x4-JIxcPY6y-_A](https://www.youtube.com/channel/UCYBr8enT5x4-JIxcPY6y-_A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCYBr8enT5x4-JIxcPY6y-_A)

#### Films &amp; Stuff {#emFilmStuff} 

> [V]ideo essays on blockbuster films

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/FilmsandStuff](https://www.patreon.com/FilmsandStuff)
- ![icone media](img/media.png) [youtube.com/channel/UCcddcRNcQfVwCMmvV2QWf8Q](https://www.youtube.com/channel/UCcddcRNcQfVwCMmvV2QWf8Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCcddcRNcQfVwCMmvV2QWf8Q)

#### VHS et canapé {#emVHSCanape} 

> 

Émission 
audio
.

- ![icone rss](img/rss.png) [](https://feed.ausha.co/yvqjfkQe80ro)- ![icone web](img/firefox.png) [https://podcast.ausha.co/vhs-canape-2](https://podcast.ausha.co/vhs-canape-2)  [![rss](img/rss.png)](https://podcast.ausha.co/vhs-canape-2)

#### Nanarland {#emNanarland} 

> 

Émission 
audio
.

- ![icone rss](img/rss.png) [](https://feed.ausha.co/B1PpCX8958vb)- ![icone web](img/firefox.png) [https://podcast.ausha.co/nanarland-le-podcast-4](https://podcast.ausha.co/nanarland-le-podcast-4)  [![rss](img/rss.png)](https://podcast.ausha.co/nanarland-le-podcast-4)

#### Cinéma et politique {#emCinemaPolitique} 

> 

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/cinema-et-politique](https://www.tipeee.com/cinema-et-politique)- ![icone soutien](img/money.png) [utip.io/cinemaetpolitique](https://utip.io/cinemaetpolitique)
- ![icone media](img/media.png) [youtube.com/channel/UC2_IrMwxhXRc24x9O85gDEw](https://www.youtube.com/channel/UC2_IrMwxhXRc24x9O85gDEw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC2_IrMwxhXRc24x9O85gDEw)

#### Sorociné {#emSorocine} 

> 
	  De tous horizons, des femmes viennent parler, échanger et débattre 
	  sur un film, un·e réalisateur·rice, une thématique récurrente, et 
	  plus encore, au sein du 7e art.
	

Émission 
audio
.

- ![icone rss](img/rss.png) [Flux RSS / Atom (Soundcloud)](https://feeds.soundcloud.com/users/soundcloud:users:420003258/sounds.rss)- ![icone web](img/firefox.png) [https://soundcloud.com/user-420003258](https://soundcloud.com/user-420003258)  [![rss](img/rss.png)](https://soundcloud.com/user-420003258)

#### Demoiselles d’horreur {#emdemoiselleshorreur} 

> 
	  [m]ettre un coup de projecteur sur les femmes dans les films 
	  d’horreur !
	

Émission 
vidéo
.

- ![icone soutien](img/money.png) [utip.io/demoisellesdhorreur](https://utip.io/demoisellesdhorreur)
- ![icone media](img/media.png) [youtube.com/channel/ucrdhe-fruu6cldmm4o_3whq](https://www.youtube.com/channel/ucrdhe-fruu6cldmm4o_3whq) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=ucrdhe-fruu6cldmm4o_3whq)

#### Cinéphile facile {#emCinephileFacile} 

> [C]ritique de films qui ont des choses à dire.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/amazing-lucy](https://www.tipeee.com/amazing-lucy)
- ![icone media](img/media.png) [youtube.com/channel/UCVdlvOXPHiH4ySKSqfoQYhQ](https://www.youtube.com/channel/UCVdlvOXPHiH4ySKSqfoQYhQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCVdlvOXPHiH4ySKSqfoQYhQ)

#### Symbolik {#emSweetBerry} 

> Analyse des symboles au cinéma.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/sweetberry](https://www.tipeee.com/sweetberry)- ![icone soutien](img/money.png) [utip.io/sweetberry](https://utip.io/sweetberry)- ![icone web](img/firefox.png) [http://sweetberry.fr/](http://sweetberry.fr/)  [![rss](img/rss.png)](http://sweetberry.fr/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCVdlvOXPHiH4ySKSqfoQYhQ](https://www.youtube.com/channel/UCVdlvOXPHiH4ySKSqfoQYhQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCVdlvOXPHiH4ySKSqfoQYhQ)

#### Zoétrope {#emZoetrope} 

> Cinéma, féminisme et sociologie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC0ZqOppfo8bcvq_oFJYpb8A](https://www.youtube.com/channel/UC0ZqOppfo8bcvq_oFJYpb8A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC0ZqOppfo8bcvq_oFJYpb8A)

#### Vidéodrome {#emVideodrome} 

> Cinéma et sciences sociales.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/videodrome](https://www.tipeee.com/videodrome)
- ![icone media](img/media.png) [youtube.com/channel/UCVcECecc_kaV9PeZVfm6TiA](https://www.youtube.com/channel/UCVcECecc_kaV9PeZVfm6TiA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCVcECecc_kaV9PeZVfm6TiA)

#### Bollywood Versus {#emBollywoodVersus} 

> 
	  [P]odcast qui passe en revue le cinéma bollywood et sa vision du 
	  féminisme, du terrorisme, du colonialisme… ou qui le confronte aux 
	  films de la culture pop comme Pirates des Caraïbes, Ocean's Eleven 
	  ou Iron-Man !
	

Émission 
audio
.

- ![icone rss](img/rss.png) [](https://bollywoodversus.lepodcast.fr/rss)- ![icone web](img/firefox.png) [https://bollywoodversus.lepodcast.fr/](https://bollywoodversus.lepodcast.fr/)  [![rss](img/rss.png)](https://bollywoodversus.lepodcast.fr/)

### Création {#creative}


#### Pitch {#emPitch} 

> Deux mots, un média, et il ne reste plus qu’à imaginer l’œuvre à partir de ça.

Émission 
vidéo
.

Produit par Ockam Razor.

- ![icone soutien](img/money.png) [tipeee.com/ockam-razor](https://www.tipeee.com/ockam-razor)- ![icone soutien](img/money.png) [https://society6.com/ockam](https://society6.com/ockam)- ![icone web](img/firefox.png) [http://www.ockamrazor.com/](http://www.ockamrazor.com/)  [![rss](img/rss.png)](http://www.ockamrazor.com/)
- ![icone media](img/media.png) [youtube.com/user/OckamRazor](https://www.youtube.com/user/OckamRazor) [RSS](https://www.youtube.com/feeds/videos.xml?user=OckamRazor)

#### Le tropeur de l’extrême {#emTropeur} 

> Émission sur les tropes et leurs caractéristiques.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCrpqKTyAxJWSpwmA_BfWTjw](https://www.youtube.com/channel/UCrpqKTyAxJWSpwmA_BfWTjw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCrpqKTyAxJWSpwmA_BfWTjw)

#### La folle histoire {#emLFolleHistoire} 

> Histoire et univers imaginaires.

Émission 
vidéo
.

- ![icone live](img/live.png) [twitch.tv/dovathib](https://www.twitch.tv/dovathib)
- ![icone media](img/media.png) [youtube.com/channel/UCBmGPY1gJ4XnXfek849BjTA](https://www.youtube.com/channel/UCBmGPY1gJ4XnXfek849BjTA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCBmGPY1gJ4XnXfek849BjTA)

#### Artifexian {#emArtifexian} 

> [C]onstruct an entire fictional universe from scratch

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone rss](img/rss.png) [](https://edgar-grunewald.squarespace.com/rss)- ![icone web](img/firefox.png) [http://www.artifexian.com/](http://www.artifexian.com/)  [![rss](img/rss.png)](http://www.artifexian.com/?format=rss)
- ![icone media](img/media.png) [youtube.com/user/Artifexian](https://www.youtube.com/user/Artifexian) [RSS](https://www.youtube.com/feeds/videos.xml?user=Artifexian)

#### Développeuse Du Dimanche {#emDevDimanche} 

> 
	  Si les coulisses du développement de jeu indépendant vous intéressent 
	  vous devriez vous plaire ici !
	

Émission 
vidéo
.

- ![icone live](img/live.png) [twitch.tv/developpeusedudimanche](https://www.twitch.tv/developpeusedudimanche)
- ![icone media](img/media.png) [youtube.com/channel/UCVf2py0nEmhiUH7pryhZdyg](https://www.youtube.com/channel/UCVf2py0nEmhiUH7pryhZdyg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCVf2py0nEmhiUH7pryhZdyg)

### Divers {#artsMisc}


#### BiTS, le magazine de la Culture Pop {#emBits} 

> En s’appuyant sur l’actualité et en révélant les ponts entre culture traditionnelle et culture populaire, BiTS montre chaque semaine à quel point la culture pop est devenue une culture à part entière.

Émission 
vidéo
.

Produit par ARTE &amp; la Générale de Production.

- ![icone web](img/firefox.png) [http://bits.arte.tv/](http://bits.arte.tv/)  [![rss](img/rss.png)](http://bits.arte.tv/)
- ![icone media](img/media.png) [youtube.com/user/bitsartemagazine](https://www.youtube.com/user/bitsartemagazine) [RSS](https://www.youtube.com/feeds/videos.xml?user=bitsartemagazine)

#### Iconologie {#emIconologie} 

> Émission sur la symbolique, la sémiologie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCLsjb-32UWEOlsKggOfxnIg](https://www.youtube.com/channel/UCLsjb-32UWEOlsKggOfxnIg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCLsjb-32UWEOlsKggOfxnIg)

#### The Nerdies Factory {#emNerdiesFactory} 

> Critique généraliste

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCGC_55TkgH38j5p0mEiqhXQ](https://www.youtube.com/channel/UCGC_55TkgH38j5p0mEiqhXQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCGC_55TkgH38j5p0mEiqhXQ)

#### Castor Mother {#emCastorMother} 

> Castor Mother vous raconte des histoires autour des animaux.  Reproduction, historique, coutume, superstition, bestiaire medieval…

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/castor-mother](https://www.tipeee.com/castor-mother)
- ![icone media](img/media.png) [youtube.com/channel/UCfO4BQD5_S1272GVmuXmTxA](https://www.youtube.com/channel/UCfO4BQD5_S1272GVmuXmTxA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCfO4BQD5_S1272GVmuXmTxA)

#### On est pas des lumières {#emEPDL} 

> Arts et histoire

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCspuMVdNaEpVE2NqGqYSjZA](https://www.youtube.com/channel/UCspuMVdNaEpVE2NqGqYSjZA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCspuMVdNaEpVE2NqGqYSjZA)

#### Nerdwriter {#emNerdWriter} 

> Épisodes analytiques sur différents arts.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/nerdwriter](https://www.patreon.com/nerdwriter)- ![icone web](img/firefox.png) [http://thenerdwriter.tumblr.com/](http://thenerdwriter.tumblr.com/)  [![rss](img/rss.png)](http://thenerdwriter.tumblr.com/rss)
- ![icone media](img/media.png) [youtube.com/user/Nerdwriter1](https://www.youtube.com/user/Nerdwriter1) [RSS](https://www.youtube.com/feeds/videos.xml?user=Nerdwriter1)

#### Grand écArt {#emGdEcart} 

> La chaîne qui fait le Grand écArt entre Pop Culture et Histoire de l’Art.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCq9HhrwCKJZcYbLtsk1wErw](https://www.youtube.com/channel/UCq9HhrwCKJZcYbLtsk1wErw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCq9HhrwCKJZcYbLtsk1wErw)

#### La mécanique du livre {#emMecaniqueLivre} 

> Depuis la relation à l’auteur en passant par la promotion de l’œuvre, de la gestion quotidienne d’une maison d’édition à la place du livre dans le numérique, nous vous livrerons tous les secrets de la chaîne du livre.

Émission 
audio
.

- ![icone web](img/firefox.png) [https://mecaniquedulivre.lepodcast.fr/](https://mecaniquedulivre.lepodcast.fr/)  [![rss](img/rss.png)](https://mecaniquedulivre.lepodcast.fr/rss)

### Gastronomie {#cuisine}


#### Gastronogeek {#emGastronogeek} 

> 

Émission 
vidéo
.

- ![icone live](img/live.png) [twitch.tv/gastronogeek](https://www.twitch.tv/gastronogeek)- ![icone soutien](img/money.png) [tipeee.com/gastronogeek](https://www.tipeee.com/gastronogeek)
- ![icone media](img/media.png) [youtube.com/channel/UCfI1q93ZYNR_mJYKFEqxfrA](https://www.youtube.com/channel/UCfI1q93ZYNR_mJYKFEqxfrA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCfI1q93ZYNR_mJYKFEqxfrA)

#### Tasting History {#emTastingHistory} 

> 
	  Love Food? Can't get enough of History? Then you've come to the 
	  right place.
	

Émission en 
anglais
.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCsaGKqPZnGp_7N80hcHySGQ](https://www.youtube.com/channel/UCsaGKqPZnGp_7N80hcHySGQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCsaGKqPZnGp_7N80hcHySGQ)

#### L’Inwhiskus {#emInwhiskus} 

> Histoire du whisky.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCX1ljGirVEGldkjRzVFLyLw](https://www.youtube.com/channel/UCX1ljGirVEGldkjRzVFLyLw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCX1ljGirVEGldkjRzVFLyLw)

### Jeu-video {#jv}


#### Indie Mag {#emIndieMag} 

> Actualité du jeu indépendant.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [https://www.indiemag.fr/](https://www.indiemag.fr/)  [![rss](img/rss.png)](https://www.indiemag.fr/)
- ![icone media](img/media.png) [youtube.com/user/IndieMagFR](https://www.youtube.com/user/IndieMagFR) [RSS](https://www.youtube.com/feeds/videos.xml?user=IndieMagFR)

#### Ça Cartouche {#emCaCartouche} 

> Émissions analytiques et critiques sur le jeu-vidéo.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.ca-cartouche.org/](http://www.ca-cartouche.org/)  [![rss](img/rss.png)](http://www.ca-cartouche.org/)
- ![icone media](img/media.png) [youtube.com/channel/UC2kauIt-F5zjQVjQU4V-bkA](https://www.youtube.com/channel/UC2kauIt-F5zjQVjQU4V-bkA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC2kauIt-F5zjQVjQU4V-bkA)

#### Théorie des jeux vidéo {#emTheorieJV} 

> Cette chaîne propose de développer les principales théories des jeux vidéo (game design, histoire, esthétique, etc.). Il y aura environ un épisode par mois. ». Réalisé par Sebastien Genvo, « enseignant-chercheur à l’université de Lorraine et game designer. ». Le flux RSS de ludologique.com est tronqué.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.ludologique.com/](http://www.ludologique.com/)  [![rss](img/rss.png)](http://www.ludologique.com/wordpress/?feed=rss)
- ![icone media](img/media.png) [youtube.com/user/SebastienGenvo](https://www.youtube.com/user/SebastienGenvo) [RSS](https://www.youtube.com/feeds/videos.xml?user=SebastienGenvo)

#### PAUSE PROCESS {#emPauseProcess} 

> Émission sur les techniques du jeu-vidéo et leurs conséquences, notamment esthétiques

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC0JUkXAVVA4qWH1BQRs5N3A](https://www.youtube.com/channel/UC0JUkXAVVA4qWH1BQRs5N3A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC0JUkXAVVA4qWH1BQRs5N3A)

#### Game Anatomy {#emGameAnatomy} 

> Émission sur le game design.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/DocGeraud](https://www.youtube.com/user/DocGeraud) [RSS](https://www.youtube.com/feeds/videos.xml?user=DocGeraud)

#### Game Spectrum {#emGameSpectrum} 

> Critique vidéoludique

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/game-spectrum](https://www.tipeee.com/game-spectrum)
- ![icone media](img/media.png) [youtube.com/user/XxIxostxX](https://www.youtube.com/user/XxIxostxX) [RSS](https://www.youtube.com/feeds/videos.xml?user=XxIxostxX)

#### Game Next Door {#emGameNextDoor} 

> Émission sur le game design.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/game-next-door](https://www.tipeee.com/game-next-door)
- ![icone media](img/media.png) [youtube.com/channel/UCIN4UmOd7x7Q3SRCo8quGzw](https://www.youtube.com/channel/UCIN4UmOd7x7Q3SRCo8quGzw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCIN4UmOd7x7Q3SRCo8quGzw)

#### Merci Dorian! {#emDorian} 

> Analyse des mécaniques propres au jeu-vidéo

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/Brandlmousse](https://www.youtube.com/user/Brandlmousse) [RSS](https://www.youtube.com/feeds/videos.xml?user=Brandlmousse)

#### Un bot pourrait faire ça {#emUBotPFC} 

> Sur les aller-retours entre le jeu-vidéo et les autres arts.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCpfe3Yjy2jGJFRk-D7DVrXw](https://www.youtube.com/channel/UCpfe3Yjy2jGJFRk-D7DVrXw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCpfe3Yjy2jGJFRk-D7DVrXw)

#### Damastès {#emDamastes} 

> Critique vidéo-ludique.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCeLaThEwJRMUg_4HSp8KZEQ](https://www.youtube.com/channel/UCeLaThEwJRMUg_4HSp8KZEQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCeLaThEwJRMUg_4HSp8KZEQ)

#### Touch my level one {#emTMLO} 

> Analyse d’un jeu-vidéo en rapport avec son premier niveau. Flux RSS déconseillé (spam).

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.ashikara.fr/](http://www.ashikara.fr/)  [![rss](img/rss.png)](http://www.ashikara.fr/feed)
- ![icone media](img/media.png) [youtube.com/user/Ashikara](https://www.youtube.com/user/Ashikara) [RSS](https://www.youtube.com/feeds/videos.xml?user=Ashikara)

#### Game Developers Conference {#emGDQ} 

> Conférence sur le jeu-vidéo par des développeurs.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.gdconf.com/](http://www.gdconf.com/)  [![rss](img/rss.png)](http://www.gdconf.com/)
- ![icone media](img/media.png) [youtube.com/user/UC0JB7TSe49lg56u6qH8y_MQ](https://www.youtube.com/user/UC0JB7TSe49lg56u6qH8y_MQ) [RSS](https://www.youtube.com/feeds/videos.xml?user=UC0JB7TSe49lg56u6qH8y_MQ)

#### Fin du game {#emFinDuGame} 

> 
	  L’émission qui décrypte les jeux vidéo que vous avez finis (ou non). 
	  ExServ, Hugo et Maxime analysent les jeux dans leur intégralité afin 
	  de comprendre leur place dans le paysage vidéoludique.
	

Émission 
audio
.

- ![icone rss](img/rss.png) [Flux RSS / Atom (pippa.io)](https://feed.pippa.io/public/shows/findugame)- ![icone rss](img/rss.png) [Flux RSS / Atom (Soundcloud)](http://feeds.soundcloud.com/users/soundcloud:users:547829478/sounds.rss)- ![icone web](img/firefox.png) [https://shows.pippa.io/findugame](https://shows.pippa.io/findugame)  [![rss](img/rss.png)](https://shows.pippa.io/findugame)

#### Pixel life stories {#emPixelLifeStories} 

> 
	  [U]ne chaîne consacrée à l’art vidéoludique, c’est à dire l’art du 
	  jeu vidéo. […] [E]lle souhaite faire découvrir aux publics le talent 
	  et les savoir-faire des artistes issus du secteur vidéoludique !
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCpm-LP2niaWpmMvvyI6M9vg](https://www.youtube.com/channel/UCpm-LP2niaWpmMvvyI6M9vg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCpm-LP2niaWpmMvvyI6M9vg)

#### Game Maker's Toolkit {#emGMToolkit} 

> 
	  Game Maker's Toolkit is a deep dive into game design, level design, 
	  and game production […]
	

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/GameMakersToolkit](https://www.patreon.com/GameMakersToolkit)
- ![icone media](img/media.png) [youtube.com/channel/UCqJ-Xo29CKyLTjn6z2XwYAw](https://www.youtube.com/channel/UCqJ-Xo29CKyLTjn6z2XwYAw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCqJ-Xo29CKyLTjn6z2XwYAw)

### Littérature {#litterature}


#### Alchimie d’un roman {#emAlchimieRoman} 

> Présentation et analyse de grands romans.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/JPDepotte/](https://www.youtube.com/user/JPDepotte/) [RSS](https://www.youtube.com/feeds/videos.xml?user=JPDepotte/)

#### L’homme littéraire {#emHommeLitteraire} 

> une chaîne consacrée essentiellement à la littérature et au monde du livre en général

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCkv82c0G9Qn1vUZ2mBcOn4A](https://www.youtube.com/channel/UCkv82c0G9Qn1vUZ2mBcOn4A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCkv82c0G9Qn1vUZ2mBcOn4A)

#### La brigade du livre {#emBrigadeDuLivre} 

> Chroniques littéraires enrobées dans de la fiction.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/la-brigade-du-livre](https://www.tipeee.com/la-brigade-du-livre)
- ![icone media](img/media.png) [youtube.com/channel/UCO-YDXoZJNVJFmF1UJ17rEw](https://www.youtube.com/channel/UCO-YDXoZJNVJFmF1UJ17rEw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCO-YDXoZJNVJFmF1UJ17rEw)

#### Miss Book {#emMissBook} 

> Chronique littéraire.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC_9Z28lA28JxAgFv-m4_nlw](https://www.youtube.com/channel/UC_9Z28lA28JxAgFv-m4_nlw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC_9Z28lA28JxAgFv-m4_nlw)

#### Cordélia aime {#emCordelia} 

> Chronique littéraire.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.mademoisellecordelia.fr/](http://www.mademoisellecordelia.fr/)  [![rss](img/rss.png)](http://www.mademoisellecordelia.fr/feed/)
- ![icone media](img/media.png) [youtube.com/user/cordeliaaime](https://www.youtube.com/user/cordeliaaime) [RSS](https://www.youtube.com/feeds/videos.xml?user=cordeliaaime)

#### Ça peut pas faire de mal {#emCPPFDMal} 

> Émission de lecture.

Émission 
audio
.

Produit par France Inter.

- ![icone rss](img/rss.png) [](http://radiofrance-podcast.net/podcast09/rss_11262.xml)- ![icone web](img/firefox.png) [http://www.franceinter.fr/emission-ca-peut-pas-faire-de-mal-2](http://www.franceinter.fr/emission-ca-peut-pas-faire-de-mal-2)  [![rss](img/rss.png)](http://www.franceinter.fr/emission-ca-peut-pas-faire-de-mal-2)- ![icone web](img/firefox.png) [http://www.franceinter.fr/reecouter-diffusions/665554](http://www.franceinter.fr/reecouter-diffusions/665554)  [![rss](img/rss.png)](http://www.franceinter.fr/reecouter-diffusions/665554)

#### Le Marque-Page {#emMarquePage} 

> Présentation de bouquins.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCJgYXYDJV9UZfxMcKuvc-0g](https://www.youtube.com/channel/UCJgYXYDJV9UZfxMcKuvc-0g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCJgYXYDJV9UZfxMcKuvc-0g)

#### Les renards hâtifs {#emRenardsHatifs} 

> RenardsHâtifs, c’est un rassemblement de passionnés de littérature sous toutes ses coutures !

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://renardshatifs.fr/](http://renardshatifs.fr/)  [![rss](img/rss.png)](http://renardshatifs.fr/?feed=rss2)
- ![icone media](img/media.png) [youtube.com/channel/UC2OcP_onh0h94tlX6xbYOyQ](https://www.youtube.com/channel/UC2OcP_onh0h94tlX6xbYOyQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC2OcP_onh0h94tlX6xbYOyQ)

#### Le Mock {#emMock} 

> 
	  Le Mock est une émission de critiques de la littérature classique 
	  française.
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC2XIqez2q8sk2bd8YcAok5Q](https://www.youtube.com/channel/UC2XIqez2q8sk2bd8YcAok5Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC2XIqez2q8sk2bd8YcAok5Q)

#### L’Instant Lecture {#emInstantLecture} 

> Lectures et critiques en animation image par image.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCiaA_n27bw1I2zc4tWU2K8Q](https://www.youtube.com/channel/UCiaA_n27bw1I2zc4tWU2K8Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCiaA_n27bw1I2zc4tWU2K8Q)

#### Boîte à Lettres {#emBoiteLettres} 

> Quelques émissions sur des auteurs et une de leurs œuvres.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCu8EOaw52LhS3b8X-N0N_5A](https://www.youtube.com/channel/UCu8EOaw52LhS3b8X-N0N_5A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCu8EOaw52LhS3b8X-N0N_5A)

#### L’univers de Tolkien {#emUniversTolkien} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCOwuxI6Z3uLANkwER-ZPM5Q](https://www.youtube.com/channel/UCOwuxI6Z3uLANkwER-ZPM5Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCOwuxI6Z3uLANkwER-ZPM5Q)

#### Les littératrices {#emLitteratrices} 

> 
	  Découvr[ez] des autrices, poétesses, romancières, dramaturges 
	  et essayistes dont vous n’avez certainement jamais entendu 
	  parler en cours de français !
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCslIRp_iqIcPRa1jBLdzkqQ](https://www.youtube.com/channel/UCslIRp_iqIcPRa1jBLdzkqQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCslIRp_iqIcPRa1jBLdzkqQ)

#### Mediaclasse.fr {#emLitteratrices} 

> Résumés d’œuvres littéraires.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/mediaclasse](https://www.youtube.com/user/mediaclasse) [RSS](https://www.youtube.com/feeds/videos.xml?user=mediaclasse)

#### Rendez-vous contes ! {#emRDVContes} 

> Un conte par vidéo.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCVsO79cFjOcoei3PywQEWDA](https://www.youtube.com/channel/UCVsO79cFjOcoei3PywQEWDA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCVsO79cFjOcoei3PywQEWDA)

#### John Katal {#emJKatal} 

> Vidéos de linguistique et de littérature générale.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCby-z5MMfbJVfecN6pTb8Aw](https://www.youtube.com/channel/UCby-z5MMfbJVfecN6pTb8Aw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCby-z5MMfbJVfecN6pTb8Aw)

#### Le podcast dont vous êtes le héros {#emLPDVELH} 

> 
	  Xavier &amp; Fred discutent de l’histoire et l’origine des livres 
	  jeux et de leurs aventures vécues à travers toutes les 
	  collections : Sorcellerie, défis fantastiques, loup solitaire, k
	  dragon d’or, astre d’or, loup ardent…
	

Émission 
audio
.

- ![icone rss](img/rss.png) [](http://feeds.soundcloud.com/users/soundcloud:users:301794171/sounds.rss)- ![icone web](img/firefox.png) [http://tau-ceti.org/index.html](http://tau-ceti.org/index.html)  [![rss](img/rss.png)](http://tau-ceti.org/index.html)

#### Qu’est-ce qu’on lit ? {#emQQLit} 

> 
	  Cette chaîne s’adresse à tous et cherche à étendre nos horizons 
	  littéraires et à vulgariser la littérature.
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCJd1AFjN9vIgVDDB6UE2UQg](https://www.youtube.com/channel/UCJd1AFjN9vIgVDDB6UE2UQg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCJd1AFjN9vIgVDDB6UE2UQg)

#### Tes grands auteurs en petits morceaux {#emAuteursMorceaux} 

> Vidéos sur des auteurs et autrices.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCjdpk8jgWwkFaf781O-gN0w](https://www.youtube.com/channel/UCjdpk8jgWwkFaf781O-gN0w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCjdpk8jgWwkFaf781O-gN0w)

#### Bookmakers {#emBookMakers} 

> 
	  Chaque mois, Bookmakers écoute les plus grand·e·s écrivain·e·s 
	  d’aujourd’hui raconter, hors de toute promotion, l’étincelle 
	  initiale, les recherches, la discipline, les obstacles, le 
	  découragement, les coups de collier, la solitude, la première 
	  phrase, les relectures… mais aussi le rôle de l’éditeur, de 
	  l’argent, la réception critique et publique, le regard sur le texte 
	  des années plus tard.
	

Émission 
audio
.

Produit par ARTE Radio.

- ![icone rss](img/rss.png) [](https://www.arteradio.com/xml_sound_serie?seriename=%22BOOKMAKERS%22)- ![icone web](img/firefox.png) [https://www.arteradio.com/serie/bookmakers](https://www.arteradio.com/serie/bookmakers)  [![rss](img/rss.png)](https://www.arteradio.com/serie/bookmakers)

#### Littératures buissonnières {#emLitteraturesBuissonnieres} 

> 
	  [U]ne chaîne de vulgarisation qui parle des littératures dont vous 
	  n’avez probablement jamais entendu parler.
	

Émission 
vidéo
.


- [fiamoa_mcbenson](fiamoa_mcbenson)

- ![icone media](img/media.png) [youtube.com/channel/UCWxGGNOL2uQJMzteo-LG8JQ](https://www.youtube.com/channel/UCWxGGNOL2uQJMzteo-LG8JQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCWxGGNOL2uQJMzteo-LG8JQ)

### Musique {#musique}


#### Partoche {#emPartoche} 

> Analyse de musique.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/tyllou](https://www.tipeee.com/tyllou)
- ![icone media](img/media.png) [youtube.com/channel/UCW2y2m-mgYg4Y7VLyjIQWWg](https://www.youtube.com/channel/UCW2y2m-mgYg4Y7VLyjIQWWg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCW2y2m-mgYg4Y7VLyjIQWWg)

#### Le set barré {#emSetBarre} 

> Analyse de musique (surtout du jeu-vidéo).

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/LeSetBarre](https://www.youtube.com/user/LeSetBarre) [RSS](https://www.youtube.com/feeds/videos.xml?user=LeSetBarre)

#### Révisons nos classiques {#emRevisonsClassiques} 

> Émission sur des compositeurs de « musique classique ».

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/revisons-nos-classiques](https://www.tipeee.com/revisons-nos-classiques)
- ![icone media](img/media.png) [youtube.com/channel/UCZHPwKyeypWwU8SNJSzQhCw](https://www.youtube.com/channel/UCZHPwKyeypWwU8SNJSzQhCw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCZHPwKyeypWwU8SNJSzQhCw)

#### Les fous du violent {#emFousViolent} 

> Émissions sur le rock, le métal…

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCODBGqOjzWVUwbCsRyYMxJA](https://www.youtube.com/channel/UCODBGqOjzWVUwbCsRyYMxJA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCODBGqOjzWVUwbCsRyYMxJA)

#### 2guys1tv {#em2guys1tv} 

> Émission sur la musique Metal.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/2guys1tv](https://www.tipeee.com/2guys1tv)
- ![icone media](img/media.png) [youtube.com/user/2guys1tv](https://www.youtube.com/user/2guys1tv) [RSS](https://www.youtube.com/feeds/videos.xml?user=2guys1tv)

#### Enjoy the noise {#emEnjoyNoise} 

> Présentation de groupes rock/metal.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [http://enjoythenoise.spreadshirt.fr/](http://enjoythenoise.spreadshirt.fr/)- ![icone web](img/firefox.png) [http://www.hitbox.tv/EnjoyTheNoise](http://www.hitbox.tv/EnjoyTheNoise)  [![rss](img/rss.png)](http://www.hitbox.tv/EnjoyTheNoise)
- ![icone media](img/media.png) [youtube.com/user/Ticheurte](https://www.youtube.com/user/Ticheurte) [RSS](https://www.youtube.com/feeds/videos.xml?user=Ticheurte)

#### Breakfast in backstage {#emBreakfastBackstage} 

> Présentation de groupes/chanteurs rock.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCIGBwNwDKxjCOUE-0HxtVLw](https://www.youtube.com/channel/UCIGBwNwDKxjCOUE-0HxtVLw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCIGBwNwDKxjCOUE-0HxtVLw)

#### After Bit {#emAfterBit} 

> Analyse des musiques de jeux-vidéos

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://bit-the-bit.com/](http://bit-the-bit.com/)  [![rss](img/rss.png)](http://bit-the-bit.com/)
- ![icone media](img/media.png) [youtube.com/user/ChroniqueAfterBit](https://www.youtube.com/user/ChroniqueAfterBit) [RSS](https://www.youtube.com/feeds/videos.xml?user=ChroniqueAfterBit)

#### Open MetalCast {#emOpenMetalcast} 

> Musique metal en Creative Commons.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone rss](img/rss.png) [](http://feeds.feedburner.com/OpenMetalcast/ogg)- ![icone web](img/firefox.png) [http://openmetalcast.com/](http://openmetalcast.com/)  [![rss](img/rss.png)](http://openmetalcast.com/)

#### Les démons du MIDI {#emDemonMIDI} 

> Émission de musique de jeux-vidéos

Émission 
vidéo
.

- ![icone rss](img/rss.png) [](http://feeds.radiokawa.com/podcast_les-demons-du-midi.xml)- ![icone web](img/firefox.png) [http://www.radiokawa.com/episode/les-demons-du-midi/](http://www.radiokawa.com/episode/les-demons-du-midi/)  [![rss](img/rss.png)](http://www.radiokawa.com/episode/les-demons-du-midi/)

#### Le disquaire {#emDisquaire} 

> Chroniques traitant de divers sujets musicaux

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/TheRumol](https://www.youtube.com/user/TheRumol) [RSS](https://www.youtube.com/feeds/videos.xml?user=TheRumol)

#### Poil Au Rock {#emPoilAuRock} 

> Chroniques sur la musique rock et metal. Diffuse maintenant des reprises.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCzbkYuexVaoZZx-6wm-RwDg](https://www.youtube.com/channel/UCzbkYuexVaoZZx-6wm-RwDg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCzbkYuexVaoZZx-6wm-RwDg)

#### Point de Synchro {#emPoint2Synchro} 

> Analyse de bandes originales.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCk-gLaqukzL7MCVSv6oOhLA](https://www.youtube.com/channel/UCk-gLaqukzL7MCVSv6oOhLA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCk-gLaqukzL7MCVSv6oOhLA)

#### Ne me parlez pas de musique « classique » {#emNMPDMusiqueClassique} 

> Ici, on découvre et on analyse des chefs d’œuvres musicaux en s’amusant!

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/otomj](https://www.youtube.com/user/otomj) [RSS](https://www.youtube.com/feeds/videos.xml?user=otomj)

#### Mermaids Tears {#emMermaidsTears} 

> Les femmes dans la musique.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCEYT6wDOMBgy9yL7Ks1rvtQ](https://www.youtube.com/channel/UCEYT6wDOMBgy9yL7Ks1rvtQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCEYT6wDOMBgy9yL7Ks1rvtQ)

#### Scherzando {#emScherzando} 

> Vulgarisation musicale, parle notamment de musique classique, mais aussi de la musique de la Renaissance, des troubadours…

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/truffiparker](https://www.youtube.com/user/truffiparker) [RSS](https://www.youtube.com/feeds/videos.xml?user=truffiparker)

#### L’instrumentarium de l’Insolite {#emInstrumentarium} 

> Vidéos sur les instruments de musique peu connus.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCSdQBJnDbYBnanpOfJMlyYw](https://www.youtube.com/channel/UCSdQBJnDbYBnanpOfJMlyYw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCSdQBJnDbYBnanpOfJMlyYw)

#### Music Antik {#emMusicAntik} 

> Histoire de la musique, vinyles et pochettes.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC44aWFGjp0gao1QMMq5TV_w](https://www.youtube.com/channel/UC44aWFGjp0gao1QMMq5TV_w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC44aWFGjp0gao1QMMq5TV_w)

#### Kemon {#emKemon} 

> Quelques vidéos sur les genres musicaux.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/KemonStudio](https://www.youtube.com/user/KemonStudio) [RSS](https://www.youtube.com/feeds/videos.xml?user=KemonStudio)

#### MidiZik {#emMidiZik} 

> Des biographies d’artistes musicaux.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCKUZzfs2J8t1RY6CpzXfbUQ](https://www.youtube.com/channel/UCKUZzfs2J8t1RY6CpzXfbUQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCKUZzfs2J8t1RY6CpzXfbUQ)

#### Première écoute {#emPremiereEcoute} 

> Découverte de musique metal en première écoute.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCKUZzfs2J8t1RY6CpzXfbUQ](https://www.youtube.com/channel/UCKUZzfs2J8t1RY6CpzXfbUQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCKUZzfs2J8t1RY6CpzXfbUQ)

#### Krono Muzic {#emKronoMuzic} 

> intéresser à toutes sortes de sujets en rapport avec le monde de la musique dans un laps de temps le plus court possible

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC5C9-4yOwA1NR2NHzq_r69g](https://www.youtube.com/channel/UC5C9-4yOwA1NR2NHzq_r69g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC5C9-4yOwA1NR2NHzq_r69g)

#### La portée historique {#emPorteeHistorique} 

> Histoire de la musique.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCYGsp80XkjBRK3jARDVFCNA](https://www.youtube.com/channel/UCYGsp80XkjBRK3jARDVFCNA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCYGsp80XkjBRK3jARDVFCNA)

#### React'son {#emReactSon} 

> Analyse de musique de la pop culture.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCpG-NVLQzQhaRlQNuoO1Hgw](https://www.youtube.com/channel/UCpG-NVLQzQhaRlQNuoO1Hgw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCpG-NVLQzQhaRlQNuoO1Hgw)

#### Intermezzo {#emIntermezzo} 

> Portraits de compositeurs.

Émission 
vidéo
.

Produit par Rapaz.

- ![icone soutien](img/money.png) [tipeee.com/rapaz](https://www.tipeee.com/rapaz)
- ![icone media](img/media.png) [youtube.com/channel/UCFLrKHR80X6avG9CHZFIh8Q](https://www.youtube.com/channel/UCFLrKHR80X6avG9CHZFIh8Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCFLrKHR80X6avG9CHZFIh8Q)

#### Versus {#emVersus} 

> Étude comparée de compositeurs.

Émission 
vidéo
.

Produit par Rapaz.

- ![icone soutien](img/money.png) [tipeee.com/rapaz](https://www.tipeee.com/rapaz)
- ![icone media](img/media.png) [youtube.com/channel/UCFLrKHR80X6avG9CHZFIh8Q](https://www.youtube.com/channel/UCFLrKHR80X6avG9CHZFIh8Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCFLrKHR80X6avG9CHZFIh8Q)

#### Ciné'Tempo {#emCineTempo} 

> Critique de films ayant pour thème la musique.

Émission 
vidéo
.

Produit par Rapaz.

- ![icone soutien](img/money.png) [tipeee.com/rapaz](https://www.tipeee.com/rapaz)
- ![icone media](img/media.png) [youtube.com/channel/UCFLrKHR80X6avG9CHZFIh8Q](https://www.youtube.com/channel/UCFLrKHR80X6avG9CHZFIh8Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCFLrKHR80X6avG9CHZFIh8Q)

#### Temporis {#emTemporis} 

> Temporis, c’est une vue sur la Musique, son Histoire et sa Vie, décomplexée et humoristique.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/temporislaserie](https://www.youtube.com/user/temporislaserie) [RSS](https://www.youtube.com/feeds/videos.xml?user=temporislaserie)

#### Fortissimo {#emFortissimo} 

> Sur la musique « classique

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCudZr8BIW2sUNXb5BJoDOBA](https://www.youtube.com/channel/UCudZr8BIW2sUNXb5BJoDOBA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCudZr8BIW2sUNXb5BJoDOBA)

#### Les AnecNotes {#emAnecNotes} 

> Anecdotes sur des groupes de musique

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCEE9FbcjgG3EtctdfOndgtQ](https://www.youtube.com/channel/UCEE9FbcjgG3EtctdfOndgtQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCEE9FbcjgG3EtctdfOndgtQ)

#### Metal Oh! {#emMetalOh} 

> [É]mission de chroniques sur les groupe de Métal trop peu connus

Émission 
vidéo
.

- ![icone soutien](img/money.png) [http://stormyheallven.bandcamp.com/album/fucked-up-world](http://stormyheallven.bandcamp.com/album/fucked-up-world)- ![icone soutien](img/money.png) [http://www.redbubble.com/people/metaloh/works/15884205-d-bardeur-and-t-shirts-metal-oh-simple-h-f#](http://www.redbubble.com/people/metaloh/works/15884205-d-bardeur-and-t-shirts-metal-oh-simple-h-f#)
- ![icone media](img/media.png) [youtube.com/user/steemansonarner](https://www.youtube.com/user/steemansonarner) [RSS](https://www.youtube.com/feeds/videos.xml?user=steemansonarner)

#### Vague noire {#emVagueNoire} 

> Histoire de la musique, plus particulièrement du rock.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/vaguenoire](https://www.tipeee.com/vaguenoire)
- ![icone media](img/media.png) [youtube.com/channel/UC0cKjDlgkqkH3rR3nTeFeNg](https://www.youtube.com/channel/UC0cKjDlgkqkH3rR3nTeFeNg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC0cKjDlgkqkH3rR3nTeFeNg)

#### Val so Classic {#emValSoClassic} 

> Vulgarisation de la musique classique et plus particulièrement de l’opéra.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCm-ULGoQGYY8I3kLc5lf6XQ](https://www.youtube.com/channel/UCm-ULGoQGYY8I3kLc5lf6XQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCm-ULGoQGYY8I3kLc5lf6XQ)

#### Le rap en mieux {#emRapEnMieux} 

> Chaîne sur le rap.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCkJx6AB7gWde0VHWq_VV2HQ](https://www.youtube.com/channel/UCkJx6AB7gWde0VHWq_VV2HQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCkJx6AB7gWde0VHWq_VV2HQ)

#### Do you Know Music ? {#emDoYouKnowMusic} 

> Documentaires sur des artistes musicaux.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCu03P4Rm--o8ojuRg948gAA](https://www.youtube.com/channel/UCu03P4Rm--o8ojuRg948gAA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCu03P4Rm--o8ojuRg948gAA)

### Séries {#series}


#### Pilote {#emPilote} 

> Présentation et analyse d’une série au regard de son seul épisode pilote.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCvHft8Duo4NaHsxQr2l9TOA](https://www.youtube.com/channel/UCvHft8Duo4NaHsxQr2l9TOA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCvHft8Duo4NaHsxQr2l9TOA)

#### Les showrunners {#emShowrunners} 

> Présentation de séries.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UClQVvXqYz7hyFNtvechntbA](https://www.youtube.com/channel/UClQVvXqYz7hyFNtvechntbA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UClQVvXqYz7hyFNtvechntbA)

#### Broadcasting Bino {#emBBino} 

> Présentation de séries.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCAAF-RcPGghehhrzvHI--sw](https://www.youtube.com/channel/UCAAF-RcPGghehhrzvHI--sw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCAAF-RcPGghehhrzvHI--sw)

#### Y’a plus de saisons {#emYAPDSaisons} 

> Présentation de séries finies.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCXeFGMYmnHQJd7Yu4B_gxXA](https://www.youtube.com/channel/UCXeFGMYmnHQJd7Yu4B_gxXA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCXeFGMYmnHQJd7Yu4B_gxXA)

#### Buffy Mars {#emBuffyMars} 

> Séries télévisées et sociologie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC3yJG3DNkuLOo1MUfiQrQDw](https://www.youtube.com/channel/UC3yJG3DNkuLOo1MUfiQrQDw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC3yJG3DNkuLOo1MUfiQrQDw)

#### Serie Maniacs {#emSerieManiacs} 

> Parler de séries télévisées entre amis.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCR0FZhY2ZXEGY591DDLWrKg](https://www.youtube.com/channel/UCR0FZhY2ZXEGY591DDLWrKg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCR0FZhY2ZXEGY591DDLWrKg)

#### La Pensine {#emPensine} 

> 
	  La Pensine est une émission qui parle de dessin-animés des 
	  années 2000.
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCmikbaOEuG9oqfKGDql3HxQ](https://www.youtube.com/channel/UCmikbaOEuG9oqfKGDql3HxQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCmikbaOEuG9oqfKGDql3HxQ)

#### SPOILERS {#emSpoilers} 

> 
	  [É]mission sur les séries TV fantastiques et de science-fiction.
	

Émission 
audio
.

- ![icone rss](img/rss.png) [](https://spoilers.lepodcast.fr/rss)- ![icone web](img/firefox.png) [https://www.festival-spoilers.fr/](https://www.festival-spoilers.fr/)  [![rss](img/rss.png)](https://www.festival-spoilers.fr/)
- ![icone media](img/media.png) [youtube.com/channel/UC56A0xL-_9AJmtYxQ0Hgu0w](https://www.youtube.com/channel/UC56A0xL-_9AJmtYxQ0Hgu0w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC56A0xL-_9AJmtYxQ0Hgu0w)

### Thématiques {#thematiques}


#### NEXUS VI {#emNexus6} 

> Chroniques consacrées à la science-fiction.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://fenschtoast.com/portfolio_page/nexus-vi/](http://fenschtoast.com/portfolio_page/nexus-vi/)  [![rss](img/rss.png)](http://fenschtoast.com/portfolio_page/nexus-vi/)
- ![icone media](img/media.png) [youtube.com/channel/UC8-UThnwzBI5ApzVG4MY7VQ](https://www.youtube.com/channel/UC8-UThnwzBI5ApzVG4MY7VQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC8-UThnwzBI5ApzVG4MY7VQ)

#### TRUQUISTE {#emTruquiste} 

> Le trucage au cinéma, par une « association de prestations audiovisuelles ». Les épisodes sont scénarisés. Flux RSS tronqué.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.castawayprod.com/](http://www.castawayprod.com/)  [![rss](img/rss.png)](http://www.castawayprod.com/feed.xml)
- ![icone media](img/media.png) [youtube.com/channel/UC9r_WRdRJ_VN2W69ACJTtbA](https://www.youtube.com/channel/UC9r_WRdRJ_VN2W69ACJTtbA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC9r_WRdRJ_VN2W69ACJTtbA)

#### New Wave {#emNewWave} 

> Sound design dans le jeu-vidéo

Émission 
vidéo
.

Produit par Kago HyperBrother.

- ![icone web](img/firefox.png) [http://hyperbrotherkgo.bandcamp.com/](http://hyperbrotherkgo.bandcamp.com/)  [![rss](img/rss.png)](http://hyperbrotherkgo.bandcamp.com/feed)
- ![icone media](img/media.png) [youtube.com/user/HyperBrotherKGO](https://www.youtube.com/user/HyperBrotherKGO) [RSS](https://www.youtube.com/feeds/videos.xml?user=HyperBrotherKGO)

#### Fréquence 9¾ {#emFrequence934} 

> un podcast hebdomadaire qui explore l’univers d’Harry Potter, chapitre par chapitre

Émission 
audio
.

- ![icone rss](img/rss.png) [](https://feed.ausha.co/yedVduG7MLN0)- ![icone web](img/firefox.png) [https://podcast.ausha.co/frequence934](https://podcast.ausha.co/frequence934)  [![rss](img/rss.png)](https://podcast.ausha.co/frequence934)

#### C’est plus que de la SF {#emPlusQueSF} 

> [U]ne interview hebdomadaire autour de la science-fiction avec des auteurs, des scénaristes, des scientifiques, ou des universitaires.

Émission 
audio
.

- ![icone rss](img/rss.png) [](https://feed.ausha.co/oLq8aCg6RamE)- ![icone web](img/firefox.png) [https://podcast.ausha.co/c-est-plus-que-de-la-sf](https://podcast.ausha.co/c-est-plus-que-de-la-sf)  [![rss](img/rss.png)](https://podcast.ausha.co/c-est-plus-que-de-la-sf)

### Théâtre et jeu de rôle {#theajdr}


#### Mandala {#emMandala} 

> La chronique qui questionne le jeu de rôle

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCWhNDgW-Dy8N9YQtJ5hqXIQ](https://www.youtube.com/channel/UCWhNDgW-Dy8N9YQtJ5hqXIQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCWhNDgW-Dy8N9YQtJ5hqXIQ)

#### Les chroniques du dragon {#emChroniquesDragon} 

> Émission de vulgarisation sur le jeu de rôle

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/anything-today-net](https://www.tipeee.com/anything-today-net)
- ![icone media](img/media.png) [youtube.com/channel/UCwrSIvGrXBNDosRzLXL5-Fw](https://www.youtube.com/channel/UCwrSIvGrXBNDosRzLXL5-Fw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCwrSIvGrXBNDosRzLXL5-Fw)

#### Les trésors d’Arpheore {#emArpheore} 

> Émission sur le jeu de rôle de la chaine Arkey Production, qui s’intéresse également au jeu-vidéo indépendant.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC1YakKmeYcWwc2duhkBHLfw](https://www.youtube.com/channel/UC1YakKmeYcWwc2duhkBHLfw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC1YakKmeYcWwc2duhkBHLfw)

#### Rôliste TV {#emRolisteTV} 

> Diverses émissions sur le jeu de rôle - présentation de produits, interviews, conseils… Le flux RSS du site web est tronqué.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://rolistetv.com/](http://rolistetv.com/)  [![rss](img/rss.png)](http://rolistetv.com/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCbM4Z7OzdBTip4fvMHvwG6A](https://www.youtube.com/channel/UCbM4Z7OzdBTip4fvMHvwG6A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCbM4Z7OzdBTip4fvMHvwG6A)

#### Icosaèdre {#emIcosaedre} 

> Histoire et théorie du jeu de rôle

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCLwJptPSToTH4IjGTwA9V0A](https://www.youtube.com/channel/UCLwJptPSToTH4IjGTwA9V0A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCLwJptPSToTH4IjGTwA9V0A)

#### Le clou du spectacle {#emClouDuSpectacle} 

> Le spectacle vivant pour tous, et surtout pour toi !

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCsBn7qgVkx3jAtWQi-1stlQ](https://www.youtube.com/channel/UCsBn7qgVkx3jAtWQi-1stlQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCsBn7qgVkx3jAtWQi-1stlQ)

#### C’est pas un drame {#emPasUnDrame} 

> Les rouages du Théâtre vous échappent ? C’est pas un drame.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCe4hvDqpoxbbDfw0vpEi9Ow](https://www.youtube.com/channel/UCe4hvDqpoxbbDfw0vpEi9Ow) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCe4hvDqpoxbbDfw0vpEi9Ow)

#### Les p’tites Leds {#emPtitesLeds} 

> 
	  Les P’tites Leds, des vidéos autour du théâtre et de plein d’autres trucs chouettes.
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCKlALB5Gf4mxD1u47f2viqA](https://www.youtube.com/channel/UCKlALB5Gf4mxD1u47f2viqA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCKlALB5Gf4mxD1u47f2viqA)
<hr/>

## Collectifs {#collectifs} 



### La vidéothèque d’Alexandrie {#colVideothequeAlexandrie} 

> Regroupement de vidéastes culturels.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://videothequealexandrie.fr/](http://videothequealexandrie.fr/)  [![rss](img/rss.png)](http://videothequealexandrie.fr/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCsqvprYnU8J8K449VAQZhsQ](https://www.youtube.com/channel/UCsqvprYnU8J8K449VAQZhsQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCsqvprYnU8J8K449VAQZhsQ)

### The VidCommons Project – Documentaries {#colVidCommonsDocs} 

> 
          The VidCommons Project is an initiative to collect, curate, and 
          distribute content released under permissive licenses, and distribute 
          said media across the fediverse using PeerTube's peer-to-peer media 
          streaming architecture. This includes films, television shows, skits, 
          documentaries, and more.
      

Émission en 
anglais
.

Émission 
vidéo
.

Produit par The VidCommons Project.


- [documentaries](documentaries)


### Head Bang {#colHeadBang} 

> Conférences culturelles / scientifiques organisées par les créateurs des Geek Faëries.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://headbang.science/](http://headbang.science/)  [![rss](img/rss.png)](http://headbang.science/)- ![icone web](img/firefox.png) [http://www.geekfaeries.fr/](http://www.geekfaeries.fr/)  [![rss](img/rss.png)](http://www.geekfaeries.fr/)
- ![icone media](img/media.png) [youtube.com/channel/UCfsKk7Rd5wEkGMPdZvqvHmA](https://www.youtube.com/channel/UCfsKk7Rd5wEkGMPdZvqvHmA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCfsKk7Rd5wEkGMPdZvqvHmA)

### Play Azur Festival {#colPlayAzur} 

> [Une] rencontre entre abonnés et vidéastes de la Côte d’Azur.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.playazur.fr/](http://www.playazur.fr/)  [![rss](img/rss.png)](http://www.playazur.fr/)
- ![icone media](img/media.png) [youtube.com/channel/UCsgaxJN14u6gohbDSHdrF-g](https://www.youtube.com/channel/UCsgaxJN14u6gohbDSHdrF-g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCsgaxJN14u6gohbDSHdrF-g)

### NeoCast {#colNeoCast} 

> Conférences du festival des vidéastes web.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.neocast.fr/](http://www.neocast.fr/)  [![rss](img/rss.png)](http://www.neocast.fr/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCg7xnXR_lGNxM_uEQFBF5gw](https://www.youtube.com/channel/UCg7xnXR_lGNxM_uEQFBF5gw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCg7xnXR_lGNxM_uEQFBF5gw)

### VideoSciences {#colVideoSciences} 

> VideoSciences est une plateforme de vidéos de science en français !

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://videosciences.cafe-sciences.org/](http://videosciences.cafe-sciences.org/)  [![rss](img/rss.png)](http://videosciences.cafe-sciences.org/feed/)

### Mediapason {#colMediapason} 

> Le collectif qui rassemble les vidéastes qui parlent de la musique dans tous ses états!

Émission 
vidéo
.

- ![icone web](img/firefox.png) [https://mediapason.wordpress.com/](https://mediapason.wordpress.com/)  [![rss](img/rss.png)](https://mediapason.wordpress.com/feed/)

### Le Vortex {#colVortex} 

> Émission de vulgarisation scientifique basée sur le concept d’une colocation de vulgarisatrices et de vulgarisateurs connus.

Émission 
vidéo
.

Produit par ARTE.


- ![icone media](img/media.png) [dailymotion.com/le_vortex](https://www.dailymotion.com/le_vortex)  [RSS](https://www.dailymotion.com/rss/user/le_vortex)

- ![icone media](img/media.png) [youtube.com/channel/UCZxLew-WXWm5dhRZBgEFl-Q](https://www.youtube.com/channel/UCZxLew-WXWm5dhRZBgEFl-Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCZxLew-WXWm5dhRZBgEFl-Q)

### Le labo des savoirs {#colLaboSavoirs} 

> Le Labo des savoirs est une émission radio hebdomadaire consacrée aux sciences et à la culture scientifique. Tous les champs du savoir scientifique sont les bienvenus dans ce programme destiné au grand public.

Émission 
audio
.

- ![icone web](img/firefox.png) [https://labodessavoirs.fr](https://labodessavoirs.fr)  [![rss](img/rss.png)](https://labodessavoirs.fr)
<hr/>

## Collectifs institutionnels {#institutionnels} 



### CNES - Centre National d’Études Spatiales {#colCNES} 

> Vulgarisation scientifique, surtout en physique/chimie, mais parfois de la biologie…

Émission 
vidéo
.


- ![icone media](img/media.png) [dailymotion.com/CNES](https://www.dailymotion.com/CNES)  [RSS](https://www.dailymotion.com/rss/user/CNES)
- ![icone rss](img/rss.png) [Flux RSS / Atom (Actualités)](https://cnes.fr/fr/news/feed/1)- ![icone rss](img/rss.png) [Flux RSS / Atom (Vidéos)](https://cnes.fr/fr/videos/feed)- ![icone rss](img/rss.png) [Flux RSS / Atom (Événements)](https://cnes.fr/fr/events/feed/1)- ![icone web](img/firefox.png) [http://www.cnes.fr/web/CNES-fr/6919-cnes-tout-sur-l-espace.php](http://www.cnes.fr/web/CNES-fr/6919-cnes-tout-sur-l-espace.php)  [![rss](img/rss.png)](http://www.cnes.fr/web/CNES-fr/6919-cnes-tout-sur-l-espace.php)

### Musée du louvre {#colLouvre} 

> 

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.louvre.fr/](http://www.louvre.fr/)  [![rss](img/rss.png)](http://www.louvre.fr/rss/louvre.xml)
- ![icone media](img/media.png) [youtube.com/user/louvre](https://www.youtube.com/user/louvre) [RSS](https://www.youtube.com/feeds/videos.xml?user=louvre)

### Confér’ENS {#colConferENS} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC_ZChHhr5nDrUymz7qsRqRw](https://www.youtube.com/channel/UC_ZChHhr5nDrUymz7qsRqRw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC_ZChHhr5nDrUymz7qsRqRw)

### Productions vidéo du Centre d’Histoire Sociale {#colCHS} 

> Vidéos produites par le Centre d’Histoire Sociale du XXe siècle (CHS), unité mixte de recherche CNRS / Université Paris 1.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/MenjouletJeanne](https://www.youtube.com/user/MenjouletJeanne) [RSS](https://www.youtube.com/feeds/videos.xml?user=MenjouletJeanne)

### Espace des sciences {#colEspaceSciences} 

> L’Espace des sciences est une association créée en 1984 à Rennes, labellisée en 2008 "Science et culture, innovation" par le ministère de l’Enseignement supérieur et de la Recherche.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.espace-sciences.org/](http://www.espace-sciences.org/)  [![rss](img/rss.png)](http://www.espace-sciences.org/flux)
- ![icone media](img/media.png) [youtube.com/user/Espacedessciences](https://www.youtube.com/user/Espacedessciences) [RSS](https://www.youtube.com/feeds/videos.xml?user=Espacedessciences)

### École nationale des chartes {#colChartes} 

> Conférences. Flux RSS du site tronqué.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.enc.sorbonne.fr/](http://www.enc.sorbonne.fr/)  [![rss](img/rss.png)](http://www.enc.sorbonne.fr/fr/rss)
- ![icone media](img/media.png) [youtube.com/user/ecoledeschartes](https://www.youtube.com/user/ecoledeschartes) [RSS](https://www.youtube.com/feeds/videos.xml?user=ecoledeschartes)

### Comité d’histoire de la ville de Paris {#colHistParis} 

> Conférences sur l’histoire de Paris.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.paris.fr/services-et-infos-pratiques/culture-et-patrimoine/histoire-et-patrimoine/histoire-et-memoire-2419#conferences-en-ligne_10](http://www.paris.fr/services-et-infos-pratiques/culture-et-patrimoine/histoire-et-patrimoine/histoire-et-memoire-2419#conferences-en-ligne_10)  [![rss](img/rss.png)](http://www.paris.fr/services-et-infos-pratiques/culture-et-patrimoine/histoire-et-patrimoine/histoire-et-memoire-2419#conferences-en-ligne_10)
- ![icone media](img/media.png) [youtube.com/channel/UCP1pc5Ug6bylHWAk7Z2ky1g](https://www.youtube.com/channel/UCP1pc5Ug6bylHWAk7Z2ky1g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCP1pc5Ug6bylHWAk7Z2ky1g)

### NASA {#colNASA} 

> La NASA.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.nasa.gov/](http://www.nasa.gov/)  [![rss](img/rss.png)](http://www.nasa.gov/)
- ![icone media](img/media.png) [youtube.com/user/NASAtelevision](https://www.youtube.com/user/NASAtelevision) [RSS](https://www.youtube.com/feeds/videos.xml?user=NASAtelevision)

### Ina Histoire {#colINA} 

> [D]es reportages sur la première et seconde guerre mondiale, la guerre d’Algérie, mai 68, la guerre d’Afghanistan, la guerre du Koweit… diffusés sur la télévision française.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.ina.fr/](http://www.ina.fr/)  [![rss](img/rss.png)](http://www.ina.fr/)
- ![icone media](img/media.png) [youtube.com/channel/UCFdEMYmiimq2uKmOMaBfPLg](https://www.youtube.com/channel/UCFdEMYmiimq2uKmOMaBfPLg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCFdEMYmiimq2uKmOMaBfPLg)

### Observatoire de Paris {#colObservParis} 

> [V]idéos de phénomènes astronomiques, reportages, interviews, animations, conférences, etc.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCOpKYDc1QFXfXH6OmfqJzFg](https://www.youtube.com/channel/UCOpKYDc1QFXfXH6OmfqJzFg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCOpKYDc1QFXfXH6OmfqJzFg)

### #dariahTeach {#colDariahTeach} 

> [A]n online platform for teaching Digital Humanities

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://dariah.eu/teach/](http://dariah.eu/teach/)  [![rss](img/rss.png)](http://dariah.eu/teach/index.php/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCScSbG7XjiXbZVgilEp0Pkw](https://www.youtube.com/channel/UCScSbG7XjiXbZVgilEp0Pkw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCScSbG7XjiXbZVgilEp0Pkw)

### Zeste de Science {#colZesteDeScience} 

> 
	Cette chaîne propose de parler de science dans un format court et vulgarisé. 
	Avec une pincée d’humour et une dose de rigueur, chaque épisode de Zeste de Science, 
	validé par les chercheurs concernés, décrypte les recherches actuelles en s’appuyant 
	sur les publications scientifiques, dont les références sont données en description, 
	et sur les images fascinantes, belles ou intrigantes issues de leurs travaux.
      

Émission 
vidéo
.

Produit par CNRS.


- ![icone media](img/media.png) [youtube.com/channel/UCAxljKT0ujiJZhGC8Ood7mw](https://www.youtube.com/channel/UCAxljKT0ujiJZhGC8Ood7mw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCAxljKT0ujiJZhGC8Ood7mw)

### Que reste-t-il à découvrir ? {#colForumCNRS} 

> 

Émission 
vidéo
.

Produit par CNRS.


- ![icone media](img/media.png) [youtube.com/channel/UCRHBpvcASQs852H0t4mt1zA](https://www.youtube.com/channel/UCRHBpvcASQs852H0t4mt1zA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCRHBpvcASQs852H0t4mt1zA)

### Ma thèse en 180 secondes {#colMT180FR} 

> 

Émission 
vidéo
.

Produit par CNRS.


- ![icone media](img/media.png) [youtube.com/channel/UCvWoYjTzOe-dC0xFNQI-TGg](https://www.youtube.com/channel/UCvWoYjTzOe-dC0xFNQI-TGg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCvWoYjTzOe-dC0xFNQI-TGg)

### PRIM {#colPrimTours} 

> Conférences de l’« équipe Prim, “Pratiques et ressources de l’information et des médiations”, de l’Université de Tours ».

Émission 
vidéo
.

Produit par Université de Tours.


- ![icone media](img/media.png) [youtube.com/channel/UCSB2ND_Nqei7Jo-2UhEahmg](https://www.youtube.com/channel/UCSB2ND_Nqei7Jo-2UhEahmg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCSB2ND_Nqei7Jo-2UhEahmg)

### Vidéos de Fabrice Kordon {#colKordon} 

> 

Émission 
vidéo
.

Produit par LIP6 Sorbonne.


- ![icone media](img/media.png) [youtube.com/channel/UCTstt_Xt0080xHjLJVhlZnw](https://www.youtube.com/channel/UCTstt_Xt0080xHjLJVhlZnw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCTstt_Xt0080xHjLJVhlZnw)

### Doranum BSN {#colDoranumBSN} 

> 

Émission 
vidéo
.

Produit par Inist-CNRS.

- ![icone web](img/firefox.png) [http://doranum.fr/](http://doranum.fr/)  [![rss](img/rss.png)](http://doranum.fr/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCiQKWx7D5wPJr4TG2zN7BJA](https://www.youtube.com/channel/UCiQKWx7D5wPJr4TG2zN7BJA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCiQKWx7D5wPJr4TG2zN7BJA)

### Société Française d’Exobiologie {#colSFE} 

> 

Émission 
vidéo
.

Produit par Société Française d’Exobiologie.


- ![icone media](img/media.png) [youtube.com/channel/UCAdvOFo8AkWq3tp_UeReZ3w](https://www.youtube.com/channel/UCAdvOFo8AkWq3tp_UeReZ3w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCAdvOFo8AkWq3tp_UeReZ3w)

### UNIL TV {#colUNIL} 

> 

Émission 
vidéo
.

Produit par UNIL (Université de Lausanne).


- ![icone media](img/media.png) [youtube.com/user/UNILTV](https://www.youtube.com/user/UNILTV) [RSS](https://www.youtube.com/feeds/videos.xml?user=UNILTV)

### Centre d’études supérieures de civilisation médiévale {#colCESCMPoitiers} 

> Conférences universitaires

Émission 
vidéo
.

Produit par Université de Poitiers.


- ![icone media](img/media.png) [youtube.com/channel/UCboIOSoVy8EdC1x6axdKNTA](https://www.youtube.com/channel/UCboIOSoVy8EdC1x6axdKNTA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCboIOSoVy8EdC1x6axdKNTA)

### L’Agora des savoirs {#colAgoraSavoirs} 

> Conférences scientifiques au Centre Rabelais de Montpellier.

Émission 
vidéo
.

Produit par Université de Montpellier.


- ![icone media](img/media.png) [youtube.com/channel/UCl8XXEKOg3r9du5Xb8zFcYQ](https://www.youtube.com/channel/UCl8XXEKOg3r9du5Xb8zFcYQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCl8XXEKOg3r9du5Xb8zFcYQ)

### Tutos Gallo {#colSaintRomainEnGal} 

> 
	Le but de ces TG est de présenter de manière humoristique, 
	ludique et originale des ustensiles et autres outils qui peuplaient 
	la vie quotidienne des (gallo-)Romains, tout en bousculant les idées 
	reçues.
      

Émission 
vidéo
.

Produit par Musée de Saint-Romain-en-Gal.


- ![icone media](img/media.png) [youtube.com/channel/UCfXay1lNwuZmXrA-icaBUIw](https://www.youtube.com/channel/UCfXay1lNwuZmXrA-icaBUIw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCfXay1lNwuZmXrA-icaBUIw)

### L’Agora des savoirs {#colAgoraSavoirs} 

> Conférences scientifiques au Centre Rabelais de Montpellier.

Émission 
vidéo
.

Produit par Université de Montpellier.


- ![icone media](img/media.png) [youtube.com/channel/UCl8XXEKOg3r9du5Xb8zFcYQ](https://www.youtube.com/channel/UCl8XXEKOg3r9du5Xb8zFcYQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCl8XXEKOg3r9du5Xb8zFcYQ)

### CEA Recherche {#colCEA} 

> Conférences et émissions de vulgarisation scientifique diverses.

Émission 
vidéo
.

Produit par Commissariat à l’énergie atomique et aux énergies alternatives.


- ![icone media](img/media.png) [youtube.com/user/CEAsciences](https://www.youtube.com/user/CEAsciences) [RSS](https://www.youtube.com/feeds/videos.xml?user=CEAsciences)

### British Museum {#colBritishMuseum} 

> 

Émission en 
anglais
.

Émission 
vidéo
.

Produit par British Museum.

- ![icone web](img/firefox.png) [https://www.britishmuseum.org/](https://www.britishmuseum.org/)  [![rss](img/rss.png)](https://www.britishmuseum.org/feed)
- ![icone media](img/media.png) [youtube.com/user/britishmuseum](https://www.youtube.com/user/britishmuseum) [RSS](https://www.youtube.com/feeds/videos.xml?user=britishmuseum)
<hr/>

## Culture web {#webculture} 



### Mémons-nous! {#emMemonsNous} 

> Émission sur la culture web, et plus particulièrement les mèmes

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/jeannot-nymouce](https://www.tipeee.com/jeannot-nymouce)
- ![icone media](img/media.png) [youtube.com/user/JeannotNymouce](https://www.youtube.com/user/JeannotNymouce) [RSS](https://www.youtube.com/feeds/videos.xml?user=JeannotNymouce)

### La chronique facile {#emCFacile} 

> Émission sur les mèmes

Émission 
vidéo
.

- ![icone web](img/firefox.png) [https://www.youtube.com/watch?v=VQ1ZXWkreak&amp;list=PLlYgX9G76J0wfNohysCegJONYLyxreBx4](https://www.youtube.com/watch?v=VQ1ZXWkreak&amp;list=PLlYgX9G76J0wfNohysCegJONYLyxreBx4)  [![rss](img/rss.png)](https://www.youtube.com/watch?v=VQ1ZXWkreak&amp;list=PLlYgX9G76J0wfNohysCegJONYLyxreBx4)
- ![icone media](img/media.png) [youtube.com/user/Superbemaddog](https://www.youtube.com/user/Superbemaddog) [RSS](https://www.youtube.com/feeds/videos.xml?user=Superbemaddog)
<hr/>

## Divers {#misc} 



### Stupid Economics {#emStupidEconomics} 

> Des vidéos sur l’économie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCyJDHgrsUKuWLe05GvC2lng](https://www.youtube.com/channel/UCyJDHgrsUKuWLe05GvC2lng) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCyJDHgrsUKuWLe05GvC2lng)

### Things you might not know {#emThingsYMNK} 

> Des choses que vous ne savez peut-être pas.

Émission en 
anglais
.

Émission 
vidéo
.

Produit par Tom Scott.

- ![icone web](img/firefox.png) [https://www.tomscott.com/](https://www.tomscott.com/)  [![rss](img/rss.png)](https://www.tomscott.com/updates.xml)
- ![icone media](img/media.png) [youtube.com/user/enyay](https://www.youtube.com/user/enyay) [RSS](https://www.youtube.com/feeds/videos.xml?user=enyay)

### Eva t’expliquer {#emEvaExpliquer} 

> De courtes vidéos de vulgarisation s’adressant aux plus jeunes.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/eva-t-expliquer](https://www.tipeee.com/eva-t-expliquer)
- ![icone media](img/media.png) [youtube.com/channel/UCKoORlGBEUFZRiltR6-PsXQ](https://www.youtube.com/channel/UCKoORlGBEUFZRiltR6-PsXQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCKoORlGBEUFZRiltR6-PsXQ)

### L’Établi {#emEtabli} 

> [F]aire connaître à tous le monde […] de l’électronique, de l’informatique et de l’Internet.

Émission 
vidéo
.


- [benjamin](benjamin)
- ![icone web](img/firefox.png) [https://etabli.tv/](https://etabli.tv/)  [![rss](img/rss.png)](https://etabli.tv/feed/)

### Cygnification {#emCygnification} 

> Émission sur la Communication.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/cygnification](https://www.tipeee.com/cygnification)- ![icone web](img/firefox.png) [http://www.cygnification.com/](http://www.cygnification.com/)  [![rss](img/rss.png)](http://www.cygnification.com/)
- ![icone media](img/media.png) [youtube.com/user/CygnificationCNV](https://www.youtube.com/user/CygnificationCNV) [RSS](https://www.youtube.com/feeds/videos.xml?user=CygnificationCNV)
<hr/>

## Politique {#politique} 



### Droit {#droit}


#### Lex Tutor {#emLexTutor} 

> Lex Tutor est une chaîne de vulgarisation du Droit.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/NunyaFR](https://www.youtube.com/user/NunyaFR) [RSS](https://www.youtube.com/feeds/videos.xml?user=NunyaFR)

#### Juris World {#emJurisWorld} 

> Chaine de vulgarisation du droit.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCb6025XQHwwjuX0AVLBnxvw](https://www.youtube.com/channel/UCb6025XQHwwjuX0AVLBnxvw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCb6025XQHwwjuX0AVLBnxvw)

#### Angle Droit {#emAngleDroit} 

> Angle Droit propose des vidéos de vulgarisation en matière de droit.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC_KidpuCqhbvqZedgq2DPpA](https://www.youtube.com/channel/UC_KidpuCqhbvqZedgq2DPpA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC_KidpuCqhbvqZedgq2DPpA)

#### 911 Avocat {#em911Avocat} 

> [U]ne émission de vulgarisation juridique. Son but: expliquer de façon simple et légère le droit qui s’applique aux vidéos sur internet.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC1g4lZctFqTpZDrJEiw9Qug](https://www.youtube.com/channel/UC1g4lZctFqTpZDrJEiw9Qug) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC1g4lZctFqTpZDrJEiw9Qug)

#### Juris Planet {#emJurisPlanet} 

> Le droit marrant et loufoque à la fois!

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC8ZFaiEUQEdZyZkMQ4gcXLQ](https://www.youtube.com/channel/UC8ZFaiEUQEdZyZkMQ4gcXLQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC8ZFaiEUQEdZyZkMQ4gcXLQ)

#### Code &amp; Co. {#emCodeEtCo} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCJYKp6eGV9FhPoiBUzoztSg](https://www.youtube.com/channel/UCJYKp6eGV9FhPoiBUzoztSg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCJYKp6eGV9FhPoiBUzoztSg)

#### Besoin de rien, envie de droit {#emEnvieDeDroit} 

> 

Émission 
audio
.

- ![icone rss](img/rss.png) [](https://rss.acast.com/besoin-de-rien-envie-de-droit)
- ![icone media](img/media.png) [youtube.com/channel/UCFbewV0s_9DFg9b8xfSZOcg](https://www.youtube.com/channel/UCFbewV0s_9DFg9b8xfSZOcg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCFbewV0s_9DFg9b8xfSZOcg)

#### Curiosités juridiques {#emCuriositesJuridiques} 

> 

Émission 
audio
.

- ![icone web](img/firefox.png) [https://www.curiositesjuridiques.fr/](https://www.curiositesjuridiques.fr/)  [![rss](img/rss.png)](https://www.curiositesjuridiques.fr/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCfbeJTi8J9rBwzgS7aYcdww](https://www.youtube.com/channel/UCfbeJTi8J9rBwzgS7aYcdww) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCfbeJTi8J9rBwzgS7aYcdww)

### Philosophie {#philo}


#### Le coup de Phil {#emCoupDePhil} 

> Explications de concepts philosophiques précis, par exemple, l’éternel retour de Nietzsche.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/LeCoupdePhil](https://www.youtube.com/user/LeCoupdePhil) [RSS](https://www.youtube.com/feeds/videos.xml?user=LeCoupdePhil)

#### La philo en petits morceaux {#emPhiloMorceaux} 

> Les grands problèmes et concepts de la philosophie des sciences en films d’animation.

Émission 
vidéo
.

Produit par Laboratoire d’Histoire et de Philosophie des Sciences - Archives Henri Poincaré (CNRS / Université de Lorraine).

- ![icone web](img/firefox.png) [http://uoh.fr/front/notice?id=b2ca8812-34b3-4a1e-88f5-571f69808081](http://uoh.fr/front/notice?id=b2ca8812-34b3-4a1e-88f5-571f69808081)  [![rss](img/rss.png)](http://uoh.fr/front/notice?id=b2ca8812-34b3-4a1e-88f5-571f69808081)- ![icone web](img/firefox.png) [http://poincare.univ-lorraine.fr/](http://poincare.univ-lorraine.fr/)  [![rss](img/rss.png)](http://poincare.univ-lorraine.fr/fr/rss.xml)
- ![icone media](img/media.png) [youtube.com/user/CLMPS2011](https://www.youtube.com/user/CLMPS2011) [RSS](https://www.youtube.com/feeds/videos.xml?user=CLMPS2011)

#### Sciencia Huasca {#emScienciaHuasca} 

> Philosophie des sciences (naturelles).

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCNMns3zbytLPooWwNmMh9_w](https://www.youtube.com/channel/UCNMns3zbytLPooWwNmMh9_w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCNMns3zbytLPooWwNmMh9_w)

#### Monsieur Phi {#emMPhi} 

> 

Émission 
vidéo
.


- [monsieurphi](monsieurphi)

- ![icone media](img/media.png) [youtube.com/channel/UCqA8H22FwgBVcF3GJpp0MQw](https://www.youtube.com/channel/UCqA8H22FwgBVcF3GJpp0MQw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCqA8H22FwgBVcF3GJpp0MQw)

#### Clémentine Haynes {#emCHaynes} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCLVy1G4ns5D4qPttvE4Cmcg](https://www.youtube.com/channel/UCLVy1G4ns5D4qPttvE4Cmcg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCLVy1G4ns5D4qPttvE4Cmcg)

#### micro-philo {#emMPhilo} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC4szzzU_Xpb6zeFussmIqWw](https://www.youtube.com/channel/UC4szzzU_Xpb6zeFussmIqWw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC4szzzU_Xpb6zeFussmIqWw)

#### Parle-moi de philo {#emParlePhilo} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCLVy1G4ns5D4qPttvE4Cmcg](https://www.youtube.com/channel/UCLVy1G4ns5D4qPttvE4Cmcg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCLVy1G4ns5D4qPttvE4Cmcg)

#### Skholé {#emSkhole} 

> 
		  Σχολή (Skholé) est une baladodiffusion conçue par la Chaire de 
		  recherche du Canada sur les écritures numériques, qui explore 
		  l’idée que la théorie ne sert à rien, si ce n’est qu’elle a 
		  l’avantage de nous faire perdre beaucoup de temps. Dans chaque 
		  épisode, un·e invité·e du milieu de la recherche scientifique 
		  ou universitaire présente un concept théorique en répondant à 
		  ces trois questions : 1) Comment définissez-vous le concept 
		  présenté ? […] 2) Quels sont des exemples de ce concept ? 
		  3) En quoi le concept ne sert-il à rien ? Expliquez comment 
		  il nous fait dysfonctionner et perdre du temps. Aussi Σχολή 
		  s’inscrit-elle dans la valorisation du savoir contemplatif et 
		  non productif, et fait-elle l’éloge du dysfonctionnement.
		

Émission en 

.

Émission 
audio
.

- ![icone web](img/firefox.png) [https://skhole.ecrituresnumeriques.ca](https://skhole.ecrituresnumeriques.ca)  [![rss](img/rss.png)](https://skhole.ecrituresnumeriques.ca/rss.xml)

### Scepticisme {#zetetique}


#### Le monde merveilleux du scepticisme {#emLMMDS} 

> 

Émission 
audio
.

- ![icone rss](img/rss.png) [](http://www.lmmds.ca/feed/)- ![icone web](img/firefox.png) [http://www.lmmds.ca/](http://www.lmmds.ca/)  [![rss](img/rss.png)](http://www.lmmds.ca/feed/)

#### Scepticisme scientifique {#emSScientifique} 

> Flux RSS tronqués.

Émission 
audio
.

- ![icone rss](img/rss.png) [](http://www.scepticisme-scientifique.com/feed/)- ![icone web](img/firefox.png) [http://www.scepticisme-scientifique.com/](http://www.scepticisme-scientifique.com/)  [![rss](img/rss.png)](http://www.scepticisme-scientifique.com/feed/)

#### Mycéliums {#emMyceliums} 

> Vulgarisation de zététique.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://myceliums.fr/](http://myceliums.fr/)  [![rss](img/rss.png)](http://myceliums.fr/feed/rss)
- ![icone media](img/media.png) [youtube.com/channel/UCJ9kIKgFuAB23GkEko1hHDg](https://www.youtube.com/channel/UCJ9kIKgFuAB23GkEko1hHDg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCJ9kIKgFuAB23GkEko1hHDg)

#### Instant Sceptique {#emInstantSceptique} 

> Aggrégateur de vidéos zététiques.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC2bt55_J3pQCImOfjO17Q-w](https://www.youtube.com/channel/UC2bt55_J3pQCImOfjO17Q-w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC2bt55_J3pQCImOfjO17Q-w)

#### Irna {#emIrna} 

> Articles et conférences zététiques. Flux du site tronqué.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://irna.lautre.net/](http://irna.lautre.net/)  [![rss](img/rss.png)](http://irna.lautre.net/spip.php?page=backend)
- ![icone media](img/media.png) [youtube.com/channel/UC2_1qDYgBgIfcKbWkyUh4gQ](https://www.youtube.com/channel/UC2_1qDYgBgIfcKbWkyUh4gQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC2_1qDYgBgIfcKbWkyUh4gQ)

#### Gollum Illuminati {#emGIlluminati} 

> Démontage de théories historiques fumeuses.

Émission 
vidéo
.


- [gollumilluminati](gollumilluminati)

- ![icone media](img/media.png) [youtube.com/channel/UChC_ODkF5GvvGTM8rxGrtmg](https://www.youtube.com/channel/UChC_ODkF5GvvGTM8rxGrtmg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UChC_ODkF5GvvGTM8rxGrtmg)

#### DEFAKATOR {#emDefakator} 

> Démontage de théories fumeuses.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCU0FhLr6fr7U9GOn6OiQHpQ](https://www.youtube.com/channel/UCU0FhLr6fr7U9GOn6OiQHpQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCU0FhLr6fr7U9GOn6OiQHpQ)

#### Ératosphère {#emEratosphere} 

> Debunk de la théorie de la terre plate.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/systmk](https://www.youtube.com/user/systmk) [RSS](https://www.youtube.com/feeds/videos.xml?user=systmk)

#### Fred Aves {#emFredAves} 

> Debunk de la théorie de la terre plate.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC8v0zDOvrG5g5xdf9ovUj0g](https://www.youtube.com/channel/UC8v0zDOvrG5g5xdf9ovUj0g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC8v0zDOvrG5g5xdf9ovUj0g)

#### Esprit Critique {#emEspritCritique} 

> Vulgarisation/réfléxion politico-philosophico-historico-scientifico-betterave 

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/esprit-critique](https://www.tipeee.com/esprit-critique)
- [esprit_critique](esprit_critique)
- ![icone web](img/firefox.png) [https://www.tzitzimitl.net/liens/](https://www.tzitzimitl.net/liens/)  [![rss](img/rss.png)](https://www.tzitzimitl.net/liens/)
- ![icone media](img/media.png) [youtube.com/channel/UC0yPCUmdMZIGtnxSnx5_ifA](https://www.youtube.com/channel/UC0yPCUmdMZIGtnxSnx5_ifA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC0yPCUmdMZIGtnxSnx5_ifA)

#### Bug mental {#emBugMental} 

> 

Émission 
vidéo
.


- [spline](spline)

- ![icone media](img/media.png) [youtube.com/channel/UCLdmnkqdcTPHvVZ8aNdbf5A](https://www.youtube.com/channel/UCLdmnkqdcTPHvVZ8aNdbf5A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCLdmnkqdcTPHvVZ8aNdbf5A)

#### Un monde riant {#emBugMental} 

> 

Émission 
vidéo
.


- [unmonderiant](unmonderiant)

- ![icone media](img/media.png) [youtube.com/user/TheBigpeha](https://www.youtube.com/user/TheBigpeha) [RSS](https://www.youtube.com/feeds/videos.xml?user=TheBigpeha)

#### Horizon Gull {#emHorizonGull} 

> 

Émission 
vidéo
.


- ![icone mastodon](img/mastodon.png) [Hackingsocial@framapiaf.org](https://framapiaf.org/@Hackingsocial)
- ![icone soutien](img/money.png) [tipeee.com/hacking-social](https://www.tipeee.com/hacking-social)
- [hackingsocialfr](hackingsocialfr)

- [hacking_social](hacking_social)

- ![icone media](img/media.png) [youtube.com/channel/UCGeFgMJfWclTWuPw8Ok5FUQ](https://www.youtube.com/channel/UCGeFgMJfWclTWuPw8Ok5FUQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCGeFgMJfWclTWuPw8Ok5FUQ)

#### Aude WTFake {#emAudeWTFake} 

> 

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/aude](https://www.tipeee.com/aude)
- ![icone media](img/media.png) [youtube.com/channel/UC8Ux-LOyEXeioYQ4LFzpBXw](https://www.youtube.com/channel/UC8Ux-LOyEXeioYQ4LFzpBXw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC8Ux-LOyEXeioYQ4LFzpBXw)

#### La mal biaisée {#emMalBiaisee} 

> 
	  [A]border l’esprit critique, le féminisme et plus globalement 
	  l’histoire des luttes sociales d’un point de vue sceptique afin 
	  d’acquérir de bonnes "armes" argumentatives pour lutter.
	

Émission 
vidéo
.


- [miz_pauline](miz_pauline)
- ![icone web](img/firefox.png) [https://ecriblog.home.blog/](https://ecriblog.home.blog/)  [![rss](img/rss.png)](https://ecriblog.home.blog/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCY2w3Jdy5e65LZUepePht0g](https://www.youtube.com/channel/UCY2w3Jdy5e65LZUepePht0g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCY2w3Jdy5e65LZUepePht0g)

#### Pour ne rien dire {#emPourNeRienDire} 

> 
	  [U]ne série de vidéos consacrée à l’analyse de discours et 
	  d’argumentation.
	

Émission 
vidéo
.


- [https://sysdiscours.hypotheses.org/](https://sysdiscours.hypotheses.org/)

- ![icone media](img/media.png) [youtube.com/channel/UCPj4ckDvI4fKxikz54w2UwQ](https://www.youtube.com/channel/UCPj4ckDvI4fKxikz54w2UwQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCPj4ckDvI4fKxikz54w2UwQ)

### Urbanisme {#urbanisme}


#### Le Quatrième Mur {#em4EMur} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCIogkaC9Y6sRbzcz04_66oA](https://www.youtube.com/channel/UCIogkaC9Y6sRbzcz04_66oA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCIogkaC9Y6sRbzcz04_66oA)
<hr/>

## Sciences humaines {#schum} 



### Anthropologie {#anthropo}


#### Passé sauvage {#emPasseSauvage} 

> 
	  Des vidéos de sciences humaines où l’on parle d’archéologie, 
	  d’histoire, d’anthropologie ». Flux RSS du site tronqué.
	

Émission 
vidéo
.


- [passesauvage](passesauvage)
- ![icone web](img/firefox.png) [http://passe-sauvage.com/](http://passe-sauvage.com/)  [![rss](img/rss.png)](http://passe-sauvage.com/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCLhOJJbPciPdocXTaAk2SdA](https://www.youtube.com/channel/UCLhOJJbPciPdocXTaAk2SdA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCLhOJJbPciPdocXTaAk2SdA)

#### Le Bizzareum {#emBizarreum} 

> 
	  Je vulgarise la Mort sur Youtube. Principalement par le biais 
	  anthropologique, archéologique mais aussi historique avec plusieurs 
	  formats : Le format archéo plus long et la minute creepy plus court 
	  où je vous présente un objet, un fait mortuaire, une coutume…
	

Émission 
vidéo
.

- ![icone web](img/firefox.png) [https://lebizarreumblog.wordpress.com/](https://lebizarreumblog.wordpress.com/)  [![rss](img/rss.png)](https://lebizarreumblog.wordpress.com/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCj3ZOi3rawL27HS2Qs3epiA](https://www.youtube.com/channel/UCj3ZOi3rawL27HS2Qs3epiA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCj3ZOi3rawL27HS2Qs3epiA)

### Divers {#schum-misc}


#### Avides de recherche {#emAvidesRecherche} 

> 
	  Émission de vulgarisation sur la recherche en sciences humaines, 
	  par l’exemple. Produit par le magazine numérique Mondes Sociaux, 
	  porté par le LabEx Structuration des Mondes Sociaux, de Toulouse.
	

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://sms.hypotheses.org](http://sms.hypotheses.org)  [![rss](img/rss.png)](http://sms.hypotheses.orgfeed/)
- ![icone media](img/media.png) [youtube.com/channel/UC4Jrte_YtwWfANKNyzse5iA](https://www.youtube.com/channel/UC4Jrte_YtwWfANKNyzse5iA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC4Jrte_YtwWfANKNyzse5iA)

#### Conversations… avec un article {#emCAUArticle} 

> [P]résente en audio des publications récentes en SHS.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.marcjahjah.net/](http://www.marcjahjah.net/)  [![rss](img/rss.png)](http://www.marcjahjah.net/feed)
- ![icone media](img/media.png) [youtube.com/channel/UCsp_h2CCICDQn-cmk-h-A-w](https://www.youtube.com/channel/UCsp_h2CCICDQn-cmk-h-A-w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCsp_h2CCICDQn-cmk-h-A-w)

#### Space Montaigne {#emSpaceMontaigne} 

> 
	  Les Sciences Humaines et Sociales avec exigence et facétie.
	

Émission 
audio
.

- ![icone rss](img/rss.png) [Flux RSS / Atom](https://space-montaigne.lepodcast.fr/rss)- ![icone rss](img/rss.png) [Flux RSS / Atom (Soundcloud)](https://feeds.soundcloud.com/users/soundcloud:users:526733778/sounds.rss)

### Ethnologie {#ethnologie}


#### Le ScionsseCast {#emScionsse} 

> Podcast / site web d’ethnographie.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [https://scionssecast.wordpress.com/](https://scionssecast.wordpress.com/)  [![rss](img/rss.png)](https://scionssecast.wordpress.com/feed/)

#### Les EthnoChroniques {#emEthnoChroniques} 

> Ethnologie, anthropologie &amp; vulgarisation scientifique.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [lesethnochroniques](lesethnochroniques)
- ![icone media](img/media.png) [youtube.com/channel/UCistL402uixsBBcDwVicKmg](https://www.youtube.com/channel/UCistL402uixsBBcDwVicKmg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCistL402uixsBBcDwVicKmg)

### Géographie {#geo}


#### Point G {#emPointG} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCBo6bcRr2D9UnkMbgeEEMbw](https://www.youtube.com/channel/UCBo6bcRr2D9UnkMbgeEEMbw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCBo6bcRr2D9UnkMbgeEEMbw)

#### Terra Incognita {#emTerraIncognita} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCd_ho3fwV39cuki6hKUSwZg](https://www.youtube.com/channel/UCd_ho3fwV39cuki6hKUSwZg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCd_ho3fwV39cuki6hKUSwZg)

#### Map Men {#emMapMen} 

> Deux comédiens parlent avec humour de cartographie.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/jayforeman](https://www.tipeee.com/jayforeman)
- ![icone media](img/media.png) [youtube.com/user/jayforeman51](https://www.youtube.com/user/jayforeman51) [RSS](https://www.youtube.com/feeds/videos.xml?user=jayforeman51)

#### La Geozone {#emGeozone} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCp6rG9KqxwsdwgVY1D2dqWA](https://www.youtube.com/channel/UCp6rG9KqxwsdwgVY1D2dqWA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCp6rG9KqxwsdwgVY1D2dqWA)

### Histoire / Archéologie {#histoire}


#### Epic teaching of the history {#emETOFTH} 

> Apprendre des choses sur l’Histoire de façon… spéciale…

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/epic-teaching-of-the-history](https://www.tipeee.com/epic-teaching-of-the-history)
- ![icone media](img/media.png) [youtube.com/user/EpicTeachingHistory](https://www.youtube.com/user/EpicTeachingHistory) [RSS](https://www.youtube.com/feeds/videos.xml?user=EpicTeachingHistory)

#### Histoire brève {#emHistoireBreve} 

> Apprendre des choses sur l’Histoire de façon rapide. Et avec des dessins.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/histoire-breve](https://www.tipeee.com/histoire-breve)
- ![icone media](img/media.png) [youtube.com/user/histoirebreve](https://www.youtube.com/user/histoirebreve) [RSS](https://www.youtube.com/feeds/videos.xml?user=histoirebreve)

#### Nota Bene {#emNB} 

> LA chaine Youtube sur l’histoire. Avec une barbe.

Émission 
vidéo
.

Produit par Benjamin Brillaud.

- ![icone soutien](img/money.png) [tipeee.com/nota-bene](https://www.tipeee.com/nota-bene)
- [notabene_channel](notabene_channel)

- ![icone media](img/media.png) [youtube.com/user/notabenemovies](https://www.youtube.com/user/notabenemovies) [RSS](https://www.youtube.com/feeds/videos.xml?user=notabenemovies)

#### Nota Bene, le podcast {#emNBPodcast} 

> 
	  A travers des émissions thématiques et des entretiens avec des
	  historiens, archéologues et chercheurs, voyagez de l'Antiquité à nos
	  jours et redécouvrez la richesse de cette Histoire !
	

Émission 
audio
.

Produit par Benjamin Brillaud.

- ![icone soutien](img/money.png) [tipeee.com/nota-bene](https://www.tipeee.com/nota-bene)- ![icone rss](img/rss.png) [](https://feeds.acast.com/public/shows/notabenemovies)

#### Parlons Y-stoire {#emYstoire} 

> Chaine d’histoire faite par un prof d’histoire.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/parlonsystoire](https://www.tipeee.com/parlonsystoire)- ![icone soutien](img/money.png) [http://shop.spreadshirt.fr/ParlonsYstoire/](http://shop.spreadshirt.fr/ParlonsYstoire/)
- ![icone media](img/media.png) [youtube.com/user/ParlonsYstoire](https://www.youtube.com/user/ParlonsYstoire) [RSS](https://www.youtube.com/feeds/videos.xml?user=ParlonsYstoire)

#### La prof {#emLaProf} 

> Une chaîne pour expliquer l’Histoire en s’amusant, sans prise de tête et sans interro surprise !

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCgk2_ngj9EVfQdnWNtOPINg](https://www.youtube.com/channel/UCgk2_ngj9EVfQdnWNtOPINg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCgk2_ngj9EVfQdnWNtOPINg)

#### Confessions d’histoire {#emConfessionsHistoire} 

> Confessions d’Histoire, c’est l’Histoire racontée par ceux qui l’ont vécue !

Émission 
vidéo
.

- ![icone soutien](img/money.png) [http://www.kisskissbankbank.com/confessions-d-histoire-l-histoire-racontee-par-ceux-qui-l-ont-vecue](http://www.kisskissbankbank.com/confessions-d-histoire-l-histoire-racontee-par-ceux-qui-l-ont-vecue)- ![icone web](img/firefox.png) [http://confessionsdhistoire.fr/](http://confessionsdhistoire.fr/)  [![rss](img/rss.png)](http://confessionsdhistoire.fr/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCzj9-ZfpJ74vYv1RZzAWTVg](https://www.youtube.com/channel/UCzj9-ZfpJ74vYv1RZzAWTVg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCzj9-ZfpJ74vYv1RZzAWTVg)

#### C’est une autre histoire {#emCUneAutreHistoire} 

> La chaîne youtube qui te parle d’histoire autrement.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCKjDY4joMPcoRMmd-G1yz1Q](https://www.youtube.com/channel/UCKjDY4joMPcoRMmd-G1yz1Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCKjDY4joMPcoRMmd-G1yz1Q)

#### Le comptoir du futur {#emComptoirFutur} 

> Le futur examiné au crible des œuvres de science-fiction, entre rationnalité et imaginaire.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.lecomptoirdufutur.com/](http://www.lecomptoirdufutur.com/)  [![rss](img/rss.png)](http://www.lecomptoirdufutur.com/)
- ![icone media](img/media.png) [youtube.com/channel/UCLfQGVZGvsYCvEIwGwFrElg](https://www.youtube.com/channel/UCLfQGVZGvsYCvEIwGwFrElg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCLfQGVZGvsYCvEIwGwFrElg)

#### Analepse {#emAnalepse} 

> Émission sur les rapports entre Histoire et fiction.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCDB2qWL_20CgNC22fgPz00w](https://www.youtube.com/channel/UCDB2qWL_20CgNC22fgPz00w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCDB2qWL_20CgNC22fgPz00w)

#### La forge d’Aslak {#emForgeAslak} 

> [Les] armes et […] leurs histoires, leurs fabrications et leurs utilisations à travers les âges. Les évolutions et améliorations qu’elles ont pu subir.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/la-forge-d-aslak](https://www.tipeee.com/la-forge-d-aslak)
- ![icone media](img/media.png) [youtube.com/channel/UCqJmbIy4EIxonGjXzntcemg](https://www.youtube.com/channel/UCqJmbIy4EIxonGjXzntcemg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCqJmbIy4EIxonGjXzntcemg)

#### Jeux Vidéo &amp; Histoire {#emJVH} 

> L’Histoire et sa représentation dans le jeu-vidéo

Émission 
vidéo
.

- ![icone web](img/firefox.png) [https://www.clionautes.org/spip.php?rubrique260](https://www.clionautes.org/spip.php?rubrique260)  [![rss](img/rss.png)](https://www.clionautes.org/spip.php?rubrique260)
- ![icone media](img/media.png) [youtube.com/user/JNSretro](https://www.youtube.com/user/JNSretro) [RSS](https://www.youtube.com/feeds/videos.xml?user=JNSretro)

#### Histoire en Jeux {#emHistoireEnJeux} 

> Les jeux vidéo permettent-ils d’apprendre l’Histoire ?

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/histoire-en-jeux](https://www.tipeee.com/histoire-en-jeux)- ![icone web](img/firefox.png) [http://histoireenjeux.fr/](http://histoireenjeux.fr/)  [![rss](img/rss.png)](http://histoireenjeux.fr/index.php/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCRThjUeXnxL_BKUhqJv5XBg](https://www.youtube.com/channel/UCRThjUeXnxL_BKUhqJv5XBg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCRThjUeXnxL_BKUhqJv5XBg)

#### History Board {#emHistoryBoard} 

> Un cocktail suave à base de jeu vidéo et d’Histoire. » Flux RSS du site tronqué.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.kekow.fr/](http://www.kekow.fr/)  [![rss](img/rss.png)](http://www.kekow.fr/feed)
- ![icone media](img/media.png) [youtube.com/user/KekowHimself](https://www.youtube.com/user/KekowHimself) [RSS](https://www.youtube.com/feeds/videos.xml?user=KekowHimself)

#### Herodot’com {#emHerodotCom} 

> L’Histoire de fond en comble, avec de gros bouquins, des vannes moisies, de grosses bibliographies, des références louches, des costumes loufoques et un présentateur mentalement instable.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/herodot-com](https://www.tipeee.com/herodot-com)
- ![icone media](img/media.png) [youtube.com/channel/UCWWzB99AURYo2KLzCReWqmA](https://www.youtube.com/channel/UCWWzB99AURYo2KLzCReWqmA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCWWzB99AURYo2KLzCReWqmA)

#### Horror humanum est {#emH2E} 

> Websérie de courts documentaires animés sur des anecdotes historiques.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCLbgxBMMEo6mAYe87esxGmg](https://www.youtube.com/channel/UCLbgxBMMEo6mAYe87esxGmg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCLbgxBMMEo6mAYe87esxGmg)

#### Virago {#emVirago} 

> Les femmes dans l’histoire.

Émission 
vidéo
.

Produit par Aude Gogny-Goubert.


- ![icone media](img/media.png) [youtube.com/channel/UC_Ll41JS0VQO9L1iOv6LDQg](https://www.youtube.com/channel/UC_Ll41JS0VQO9L1iOv6LDQg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC_Ll41JS0VQO9L1iOv6LDQg)

#### C’est pas sourcé {#emCPasSource} 

> Histoire des religions

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCKYxU5ZkjTHwnhoGGYfwjPw](https://www.youtube.com/channel/UCKYxU5ZkjTHwnhoGGYfwjPw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCKYxU5ZkjTHwnhoGGYfwjPw)

#### Passion Médiévistes {#emPMedievistes} 

> Podcast mensuel sur l’histoire médiévale

Émission 
audio
.

- ![icone rss](img/rss.png) [](http://feeds.soundcloud.com/users/soundcloud:users:147058082/sounds.rss)- ![icone web](img/firefox.png) [https://passionmedievistes.fr/](https://passionmedievistes.fr/)  [![rss](img/rss.png)](https://passionmedievistes.fr/)

#### Passion Modernistes {#emPModernistes} 

> Des interviews de jeunes historiens qui travaillent sur l’histoire moderne

Émission 
audio
.

- ![icone rss](img/rss.png) [](https://passionmedievistes.fr/feed/podcast/passionmodernistes)- ![icone web](img/firefox.png) [https://passionmedievistes.fr/passion-modernistes/](https://passionmedievistes.fr/passion-modernistes/)  [![rss](img/rss.png)](https://passionmedievistes.fr/passion-modernistes/)

#### Passion Antiquités {#emPAntiquites} 

> Des interviews de jeunes historiens qui travaillent sur l’Antiquité.

Émission 
audio
.

- ![icone rss](img/rss.png) [](https://passionmedievistes.fr/feed/podcast/passion-antiquites)- ![icone web](img/firefox.png) [https://passionmedievistes.fr/tag/passion-antiquites/](https://passionmedievistes.fr/tag/passion-antiquites/)  [![rss](img/rss.png)](https://passionmedievistes.fr/tag/passion-antiquites/)

#### L’histoire nous le dira {#emHistoireNousLeDira} 

> Un peu de culture générale en histoire, le tout distillé en quelques capsules pour le plaisir des yeux et des oreilles… Plus sérieusement, on parle d’histoire et on s’amuse avec !

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCN4TCCaX-gqBNkrUqXdgGRA](https://www.youtube.com/channel/UCN4TCCaX-gqBNkrUqXdgGRA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCN4TCCaX-gqBNkrUqXdgGRA)

#### Sur le champ {#emSurLeChamp} 

> Histoire de la tactique et de la stratégie militaire.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCiWrr_SnP8eK-_9yJWFzbEA](https://www.youtube.com/channel/UCiWrr_SnP8eK-_9yJWFzbEA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCiWrr_SnP8eK-_9yJWFzbEA)

#### Historia Civilis {#emHistoriaCivilis} 

> Histoire romaine.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/HistoriaCivilis](https://www.patreon.com/HistoriaCivilis)- ![icone soutien](img/money.png) [https://teespring.com/stores/historiacivilis](https://teespring.com/stores/historiacivilis)- ![icone web](img/firefox.png) [https://www.historiacivilis.com/](https://www.historiacivilis.com/)  [![rss](img/rss.png)](https://www.historiacivilis.com/)
- ![icone media](img/media.png) [youtube.com/channel/UCv_vLHiWVBh_FR9vbeuiY-A](https://www.youtube.com/channel/UCv_vLHiWVBh_FR9vbeuiY-A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCv_vLHiWVBh_FR9vbeuiY-A)

#### Boneless {#emBoneless} 

> Ici le Dr. Kerner, préhistorienne et thanato-archéologue qui vous parle de faits préhistoriques et mortuaires étonnants.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC7ktqoCpxEbP9TV-xQLTonQ](https://www.youtube.com/channel/UC7ktqoCpxEbP9TV-xQLTonQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC7ktqoCpxEbP9TV-xQLTonQ)

#### Généalogie de ton armoire {#emArmoireGenea} 

> Histoire de la mode.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCUP3KJnzQLN6aA_Rmd2U17w](https://www.youtube.com/channel/UCUP3KJnzQLN6aA_Rmd2U17w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCUP3KJnzQLN6aA_Rmd2U17w)

#### Histoirede {#emHistoirede} 

> Chaine d’histoire généraliste.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCa4J1WaK7Md_I1Distxv-sQ](https://www.youtube.com/channel/UCa4J1WaK7Md_I1Distxv-sQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCa4J1WaK7Md_I1Distxv-sQ)

#### Histony {#emHistony} 

> L’histoire vue par les peuples.

Émission 
vidéo
.


- [histony](histony)
- ![icone web](img/firefox.png) [https://histony.lepodcast.fr/](https://histony.lepodcast.fr/)  [![rss](img/rss.png)](https://histony.lepodcast.fr/rss)
- ![icone media](img/media.png) [youtube.com/channel/UCt8ctlakIflnSG0ebFps7cw](https://www.youtube.com/channel/UCt8ctlakIflnSG0ebFps7cw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCt8ctlakIflnSG0ebFps7cw)

#### Les chroniques historiques {#emChroniquesHistoriques} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCZSTjnVQ10Ss8-ad65cCUSA](https://www.youtube.com/channel/UCZSTjnVQ10Ss8-ad65cCUSA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCZSTjnVQ10Ss8-ad65cCUSA)

#### L’histoire par les cartes {#emHistoireParCartes} 

> Animations cartographiques.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCQ3ZULRPs2T-hlCBd9-1vtA](https://www.youtube.com/channel/UCQ3ZULRPs2T-hlCBd9-1vtA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCQ3ZULRPs2T-hlCBd9-1vtA)

#### Questions d’histoire {#emQuestionsHistoire} 

> 
	  Questions d’Histoire est une chaine de vulgarisation historique 
	  traitant de nombreuses thématiques et problématiques.
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCCGRtSqLfljpX9mzCYDsQIg](https://www.youtube.com/channel/UCCGRtSqLfljpX9mzCYDsQIg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCCGRtSqLfljpX9mzCYDsQIg)

#### Salon de l’histoire {#emSalonHistoire} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCtPPkaXk1DXerZEvfPb4ssA](https://www.youtube.com/channel/UCtPPkaXk1DXerZEvfPb4ssA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCtPPkaXk1DXerZEvfPb4ssA)

#### Part en thèse {#emPartEnThese} 

> 
	    Part en thèse c’est un journal de thèse en podcast. L’histoire d’une 
	    rédaction. J’ai un an pour finir ma thèse et je vous raconte comment ça 
	    se passe.
	

Émission 
vidéo
.

- ![icone rss](img/rss.png) [](https://feed.ausha.co/9oKQDfx2kOWb)- ![icone web](img/firefox.png) [https://podcastfrance.fr/podcasts/culture-infos/part-en-these/](https://podcastfrance.fr/podcasts/culture-infos/part-en-these/)  [![rss](img/rss.png)](https://podcastfrance.fr/podcasts/culture-infos/part-en-these/)

#### Histoire appliquée {#emHistoireApplique} 

> 
	  Une chaîne où l’on teste l’Histoire, et on la rend vivante !
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCcT7B4zCzrfywO2Q19OJIzA](https://www.youtube.com/channel/UCcT7B4zCzrfywO2Q19OJIzA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCcT7B4zCzrfywO2Q19OJIzA)

#### D-Mystif {#emDMystif} 

> 
	  L’Histoire de France, d’Europe et du Monde est rafraîchie par D-Mystif’, loin des idées reçues !
	

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/d-mystif](https://www.tipeee.com/d-mystif)- ![icone soutien](img/money.png) [utip.io/dmystif](https://utip.io/dmystif)
- ![icone media](img/media.png) [youtube.com/channel/UCG9zyR-87bhdydUnqDvPmWg](https://www.youtube.com/channel/UCG9zyR-87bhdydUnqDvPmWg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCG9zyR-87bhdydUnqDvPmWg)

#### Archéologie &amp; Mystères {#emThomasLaurent} 

> 
	  [J]e reprends les codes des documentaires à suspens, sur des sujets historiques et archéologiques variés… MAIS avec rigueur scientifique !
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCi0s6lgm7BCdM50X6oKmNwg](https://www.youtube.com/channel/UCi0s6lgm7BCdM50X6oKmNwg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCi0s6lgm7BCdM50X6oKmNwg)

#### Sam O'Nella Academy {#emSamONella} 

> 

Émission en 
anglais
.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC1DTYW241WD64ah5BFWn4JA](https://www.youtube.com/channel/UC1DTYW241WD64ah5BFWn4JA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC1DTYW241WD64ah5BFWn4JA)

#### Alternate History Hub {#emAlternateHistoryHub} 

> 

Émission en 
anglais
.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/AlternateHistoryHub](https://www.youtube.com/user/AlternateHistoryHub) [RSS](https://www.youtube.com/feeds/videos.xml?user=AlternateHistoryHub)

#### Le Phare à On {#emPhareAOn} 

> Plusieurs émissions consacrées à l’Égypte antique, par un docteur en égyptologie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCAGS6NZLc6IgKawROgYG3oA](https://www.youtube.com/channel/UCAGS6NZLc6IgKawROgYG3oA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCAGS6NZLc6IgKawROgYG3oA)

#### Les DurES à cuire de l’Histoire {#emDuresACuire} 

> [L]es femmes ignorées par les livres d’Histoire

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCT4fTWzm9NCu7qfwORwO8yA](https://www.youtube.com/channel/UCT4fTWzm9NCu7qfwORwO8yA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCT4fTWzm9NCu7qfwORwO8yA)

#### Égypto’Logique {#emEgyptoLogique} 

> 
	  [J]e te fais découvrir l’Égypte ancienne sur cette chaîne, au 
	  programme momies, mythologie, pyramide.
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCR8EfsSH2VFFuIyQIlv5Ybg](https://www.youtube.com/channel/UCR8EfsSH2VFFuIyQIlv5Ybg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCR8EfsSH2VFFuIyQIlv5Ybg)

#### Les revues du monde {#emRevuesDuMonde} 

> 
	  Émission culturelle sur l’archéologie, l’Histoire, l’Anthropologie, 
	  les découvertes.
	

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/les-revues-du-monde](https://www.tipeee.com/les-revues-du-monde)
- [mirrors](mirrors)

- ![icone media](img/media.png) [youtube.com/channel/UCnf0fDz1vTYW-sl36wbVMbg](https://www.youtube.com/channel/UCnf0fDz1vTYW-sl36wbVMbg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCnf0fDz1vTYW-sl36wbVMbg)

#### Ave'Roes {#emAveRoes} 

> 
	  Plusieurs émissions sur l’Islam au Moyen-Âge, avec une perspective 
	  comparatiste Orient / Occident.
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCMGzPGJMKKDjnproamSIC1A](https://www.youtube.com/channel/UCMGzPGJMKKDjnproamSIC1A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCMGzPGJMKKDjnproamSIC1A)

#### L’Histoire trouve toujours un chemin {#emLTTUC} 

> 

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/l-histoire-trouve-toujours-un-chemin](https://www.tipeee.com/l-histoire-trouve-toujours-un-chemin)- ![icone soutien](img/money.png) [utip.io/lhistoiretrouvetoujoursunchemin](https://utip.io/lhistoiretrouvetoujoursunchemin)
- ![icone media](img/media.png) [youtube.com/channel/UChD5UkL8LtcLk_D0-obwW8w](https://www.youtube.com/channel/UChD5UkL8LtcLk_D0-obwW8w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UChD5UkL8LtcLk_D0-obwW8w)

#### L’Atelier d’Histoire {#emAtelierHistoire} 

> Histoire contemporaine.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC8wlWi7zobWKldbsP4NFa7g](https://www.youtube.com/channel/UC8wlWi7zobWKldbsP4NFa7g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC8wlWi7zobWKldbsP4NFa7g)

#### Nora Minion {#emNoraMinion} 

> Trois émissions sur l’histoire.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [utip.io/noraminion](https://utip.io/noraminion)
- ![icone media](img/media.png) [youtube.com/channel/UCcSnmRhEM3kLM3fAa-aW2-g](https://www.youtube.com/channel/UCcSnmRhEM3kLM3fAa-aW2-g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCcSnmRhEM3kLM3fAa-aW2-g)

#### Vintage Egyptologist {#emVintageEgypto} 

> Émission d’égyptologie tenue par deux égyptologues.

Émission en 
anglais
.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC3PrBNQ3sqbHKXYgZX7aN4Q](https://www.youtube.com/channel/UC3PrBNQ3sqbHKXYgZX7aN4Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC3PrBNQ3sqbHKXYgZX7aN4Q)

#### La douve {#emDouve} 

> [C]auser cinéma et histoire(s) sans se prendre le chou[.]

Émission 
audio
.


- ![icone media](img/media.png) [youtube.com/channel/UCV0UcBS4OrG59Ub3TA3utGw](https://www.youtube.com/channel/UCV0UcBS4OrG59Ub3TA3utGw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCV0UcBS4OrG59Ub3TA3utGw)

#### Histoires crépues {#emHistoireCrepues} 

> [O]n parle d’Histoires Coloniales parce que c’est notre histoire, l’histoire de France, l’histoire du monde.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/histoires-crepues](https://www.tipeee.com/histoires-crepues)
- ![icone media](img/media.png) [youtube.com/channel/UCVuMMUfqEI448VKbkpuNPHQ](https://www.youtube.com/channel/UCVuMMUfqEI448VKbkpuNPHQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCVuMMUfqEI448VKbkpuNPHQ)

#### Dis Clio {#emDisClio} 

> 
	  Quand Clio, la muse de l’histoire, débarque sur nos téléphones […] 
	  sous forme d’application… paf, ça fait des vidéos !
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCHA6nRbYD62v3MITQWyGbhg](https://www.youtube.com/channel/UCHA6nRbYD62v3MITQWyGbhg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCHA6nRbYD62v3MITQWyGbhg)

#### Oui d’accord {#emOuiDAccord} 

> Histoire du capitalisme.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC4wFTE-fzVmghgtOcD5CQkA](https://www.youtube.com/channel/UC4wFTE-fzVmghgtOcD5CQkA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC4wFTE-fzVmghgtOcD5CQkA)

#### Passeport pour hier {#emPasseportHier} 

> Géoarchéologie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCL5b33bwR_taY3MdRyQZUww](https://www.youtube.com/channel/UCL5b33bwR_taY3MdRyQZUww) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCL5b33bwR_taY3MdRyQZUww)

#### Sortie d’usine {#emSortieUsine} 

> Histoire de l’industrie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCyEgkeeUK5_eJuo72PvINSA](https://www.youtube.com/channel/UCyEgkeeUK5_eJuo72PvINSA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCyEgkeeUK5_eJuo72PvINSA)

#### Truelle la Vie ! {#emTruelleLaVie} 

> Histoire et archéologie du japon par une doctoresse en archéologie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC0gIjm7TayHTvESXm5riqMA](https://www.youtube.com/channel/UC0gIjm7TayHTvESXm5riqMA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC0gIjm7TayHTvESXm5riqMA)

#### L'histoire avec une grande hache {#emHistoireGrandeHache} 

> La chaîne qui dépoussière l'histoire et la culture avec une grande hache

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCrcrAlH1kD3ETbgv9G0CGTQ](https://www.youtube.com/channel/UCrcrAlH1kD3ETbgv9G0CGTQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCrcrAlH1kD3ETbgv9G0CGTQ)

#### Historio {#emHistorio} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCjXVY_q2iTlumu3JXhqeD_A](https://www.youtube.com/channel/UCjXVY_q2iTlumu3JXhqeD_A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCjXVY_q2iTlumu3JXhqeD_A)

### Informatique {#compute1}


#### Computerphile {#emComputerphile} 

> Videos all about computers and computer stuff

Émission en 
anglais
.

Émission 
vidéo
.

Produit par Tom Scott.


- ![icone media](img/media.png) [youtube.com/user/Computerphile](https://www.youtube.com/user/Computerphile) [RSS](https://www.youtube.com/feeds/videos.xml?user=Computerphile)

#### ChompChomp {#emChompChomp} 

> Épisodes animés sur la programmation.

Émission en 
anglais
.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCAcWL60lM0FwdExJwqtAa1A](https://www.youtube.com/channel/UCAcWL60lM0FwdExJwqtAa1A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCAcWL60lM0FwdExJwqtAa1A)

### Linguistique {#ling}


#### Code MU {#emCodeMU} 

> Histoire des mots / Étymologie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/codemutv](https://www.youtube.com/user/codemutv) [RSS](https://www.youtube.com/feeds/videos.xml?user=codemutv)

#### Linguisticae {#emLinguisticae} 

> Histoire des mots / Étymologie.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/linguisticae](https://www.tipeee.com/linguisticae)- ![icone web](img/firefox.png) [http://linguisticae.com/](http://linguisticae.com/)  [![rss](img/rss.png)](http://linguisticae.com/)
- ![icone media](img/media.png) [youtube.com/channel/UCofQxJWd4qkqc7ZgaLkZfcw](https://www.youtube.com/channel/UCofQxJWd4qkqc7ZgaLkZfcw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCofQxJWd4qkqc7ZgaLkZfcw)

#### The Art of Language Invention {#emTAOLI} 

> 
	  Chaine consacrée à la construction de langues (langues construites, 
	  conlangs), par le créateur des langues de Game of Thrones.
	

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://dedalvs.tumblr.com/](http://dedalvs.tumblr.com/)  [![rss](img/rss.png)](http://dedalvs.tumblr.com/rss)- ![icone web](img/firefox.png) [http://artoflanguageinvention.com/](http://artoflanguageinvention.com/)  [![rss](img/rss.png)](http://artoflanguageinvention.com/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCgJSf-fmdfUsSlcr7A92-aA](https://www.youtube.com/channel/UCgJSf-fmdfUsSlcr7A92-aA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCgJSf-fmdfUsSlcr7A92-aA)

#### L’insolente linguiste {#emInsolenteLinguiste} 

> 
	  De la linguistique sans censure ! Du français québécois 
	  désacralisé, dépittoresqué, décomplexé, décorsetté, affirmé, assumé 
	  et assuré.
	

Émission en 
anglais
.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCeEb8pfFV5zWNX2fCiG6UDw](https://www.youtube.com/channel/UCeEb8pfFV5zWNX2fCiG6UDw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCeEb8pfFV5zWNX2fCiG6UDw)

#### NativLang {#emNativLang} 

> Épisodes animés sur des questions linguistiques.

Émission en 
anglais
.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/NativLang](https://www.youtube.com/user/NativLang) [RSS](https://www.youtube.com/feeds/videos.xml?user=NativLang)

#### Elles Comme Linguistes {#emLCLinguistes} 

> Plusieurs émissions sur la linguistique.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC8miF2H8VfLDDGeslNaWHfQ](https://www.youtube.com/channel/UC8miF2H8VfLDDGeslNaWHfQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC8miF2H8VfLDDGeslNaWHfQ)

#### La Minute Belge {#emMinuteBelge} 

> Vidéos expliquant certaines variations dans le français belge (« belgicismes »).

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCV7LqtLm0-NW4x1W5AUu-QA](https://www.youtube.com/channel/UCV7LqtLm0-NW4x1W5AUu-QA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCV7LqtLm0-NW4x1W5AUu-QA)

#### Lyokoï Kun {#emLyokoiKun} 

> 
	  Chaîne linguistique. On va parler des mots, des rares, des bizarres, 
	  des longs, des courts, et de toutes la variabilité que peut nous 
	  offrir la langue française. On parlera aussi d’autres langues, de 
	  leurs histoires et de leurs particularités.
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCoB1M5fbdQRGW05ubC3WjmA](https://www.youtube.com/channel/UCoB1M5fbdQRGW05ubC3WjmA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCoB1M5fbdQRGW05ubC3WjmA)

#### Parler comme jamais {#emParlerCommeJamais} 

> 
	  Laélia Véron s’interroge sur les langages et leurs usages, sur ce 
	  qu’ils disent de nous.
	

Émission 
audio
.

- ![icone rss](img/rss.png) [](https://rss.acast.com/parler-comme-jamais)- ![icone web](img/firefox.png) [https://www.binge.audio/tag/parler-comme-jamais/](https://www.binge.audio/tag/parler-comme-jamais/)  [![rss](img/rss.png)](https://www.binge.audio/tag/parler-comme-jamais/)

#### Com’ vous dites ? {#emComVousDites} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCimH04-nJebV2AR5mK1otkQ](https://www.youtube.com/channel/UCimH04-nJebV2AR5mK1otkQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCimH04-nJebV2AR5mK1otkQ)

#### La tête froide {#emTeteFroide} 

> La chaîne YouTube qui te parle interprétation en langue des signes.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [utip.io/latetefroide](https://utip.io/latetefroide)
- ![icone media](img/media.png) [youtube.com/channel/UCQ_zWj81Y_jbLFms5QfRYHQ](https://www.youtube.com/channel/UCQ_zWj81Y_jbLFms5QfRYHQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCQ_zWj81Y_jbLFms5QfRYHQ)

### Sociologie {#sociologie}


#### SOCA {#emSOCA} 

> Présentation de livres de sociologie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCZhZD-E7eJg0iAzCfRwFrlg](https://www.youtube.com/channel/UCZhZD-E7eJg0iAzCfRwFrlg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCZhZD-E7eJg0iAzCfRwFrlg)

#### Sociologeek {#emSociologeek} 

> Concepts de sociologie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC7qZfJt-S0RvP9H-5_FLETw](https://www.youtube.com/channel/UC7qZfJt-S0RvP9H-5_FLETw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC7qZfJt-S0RvP9H-5_FLETw)

#### Patchwork {#emPatchwork} 

> 
	  Une chaîne où qu’on [sic] parle de sciences, scepticisme, politique[.]
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCAHFBzfPdUFER6toE0UfRdg](https://www.youtube.com/channel/UCAHFBzfPdUFER6toE0UfRdg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCAHFBzfPdUFER6toE0UfRdg)

#### Consciences sociales {#emConsciencesSociales} 

> Émission de sociologie politique.

Émission 
vidéo
.

- ![icone live](img/live.png) [twitch.tv/modiiie](https://www.twitch.tv/modiiie)
- ![icone media](img/media.png) [youtube.com/channel/UCTfiIC_hBBoCwQhUztKAFGQ](https://www.youtube.com/channel/UCTfiIC_hBBoCwQhUztKAFGQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCTfiIC_hBBoCwQhUztKAFGQ)

#### Les couilles sur la table {#emCouilles} 

> 
	  Un jeudi sur deux, Victoire Tuaillon parle en profondeur d’un aspect 
	  des masculinités contemporaines avec un·e invité·e. Parce qu’on ne 
	  naît pas homme, on le devient.
	

Émission 
audio
.

Produit par Binge Audio.

- ![icone rss](img/rss.png) [](https://rss.acast.com/les-couilles-sur-la-table)- ![icone web](img/firefox.png) [https://binge.audio/podcast/les-couilles-sur-la-table](https://binge.audio/podcast/les-couilles-sur-la-table)  [![rss](img/rss.png)](https://binge.audio/podcast/les-couilles-sur-la-table)
- ![icone media](img/media.png) [youtube.com/channel/UCmsEKbOV5Drze1NdtQcsSNg](https://www.youtube.com/channel/UCmsEKbOV5Drze1NdtQcsSNg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCmsEKbOV5Drze1NdtQcsSNg)

#### Le cœur sur la table {#emCoeur} 

> 
	  Parce que s’aimer est l’une des façons de faire la révolution. Une
	  fois par semaine, un épisode pour réinventer nos relations
	  amoureuses, nos liens avec nos ami·e·s, nos parent·e·s et nos
	  amant·e·s.
	

Émission 
audio
.

Produit par Binge Audio.

- ![icone rss](img/rss.png) [](https://rss.acast.com/binge-coeur-sur-table)- ![icone web](img/firefox.png) [https://binge.audio/podcast/le-coeur-sur-la-table](https://binge.audio/podcast/le-coeur-sur-la-table)  [![rss](img/rss.png)](https://binge.audio/podcast/le-coeur-sur-la-table)
- ![icone media](img/media.png) [youtube.com/channel/UCT_vT5yKDtkcQKocp0YGrGQ](https://www.youtube.com/channel/UCT_vT5yKDtkcQKocp0YGrGQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCT_vT5yKDtkcQKocp0YGrGQ)

#### Gregoire Simpson {#emGregoireSimpson} 

> Vidéos sur la sociologie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCRhYUx9NzhSd5xM38yshGQA](https://www.youtube.com/channel/UCRhYUx9NzhSd5xM38yshGQA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCRhYUx9NzhSd5xM38yshGQA)
<hr/>

## Sciences naturelles {#scnat} 



### Biologie / Paléontologie {#biopaleo}


#### Biologie Tout Compris {#emBiologieToutCompris} 

> Un concept biologique, moins de six minutes, une vidéo !

Émission 
vidéo
.

- ![icone web](img/firefox.png) [https://www.linkedin.com/in/tania-louis-145a2a89](https://www.linkedin.com/in/tania-louis-145a2a89)  [![rss](img/rss.png)](https://www.linkedin.com/in/tania-louis-145a2a89)
- ![icone media](img/media.png) [youtube.com/channel/UC9JQQll_r5k1O_Y18ymTp4g](https://www.youtube.com/channel/UC9JQQll_r5k1O_Y18ymTp4g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC9JQQll_r5k1O_Y18ymTp4g)

#### Bio Logique {#emBioLogique} 

> Une chaîne essentiellement basée sur les fondamentaux en biologie, mais pas que… l’idée étant pour les étudiant(e)s qu’ils/elles puissent revoir certains thèmes ou notions en détail, avec une “pause” possible grâce à la vidéo.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://biologiquevideo.com/](http://biologiquevideo.com/)  [![rss](img/rss.png)](http://biologiquevideo.com/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCG-O7s_-BlT_mWGlYw-SEHg](https://www.youtube.com/channel/UCG-O7s_-BlT_mWGlYw-SEHg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCG-O7s_-BlT_mWGlYw-SEHg)

#### Motorsport Gigantoraptor {#emGigantoraptor} 

> Biologie et évolution.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/AstroPaleoBMW](https://www.youtube.com/user/AstroPaleoBMW) [RSS](https://www.youtube.com/feeds/videos.xml?user=AstroPaleoBMW)

#### Les dinosaures de France {#emDinosauresFrance} 

> Émission sur les dinosaures ayant vécu dans l’espace géographique français.

Émission 
vidéo
.


- ![icone media](img/media.png) [dailymotion.com/DinoFrance](https://www.dailymotion.com/DinoFrance)  [RSS](https://www.dailymotion.com/rss/user/DinoFrance)
- ![icone soutien](img/money.png) [tipeee.com/les-dinosaures-de-france](https://www.tipeee.com/les-dinosaures-de-france)

#### PaleoWorld {#emPaleoWorld} 

> Une chaîne de vulgarisation autour de la paléontologie, l’évolution, la géologie, le tout raconté par un homme à chapeau » Par le créateur des Dinosaures de France.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/les-dinosaures-de-france](https://www.tipeee.com/les-dinosaures-de-france)
- ![icone media](img/media.png) [youtube.com/channel/UCetUcOmlHbepmh6NLea9l5g](https://www.youtube.com/channel/UCetUcOmlHbepmh6NLea9l5g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCetUcOmlHbepmh6NLea9l5g)

#### Gross Science {#emGrossScience} 

> Chaine sur les « choses dégoûtantes ».

Émission en 
anglais
.

Émission 
vidéo
.

Produit par PBS Digital Studio.


- ![icone media](img/media.png) [youtube.com/user/grossscienceshow](https://www.youtube.com/user/grossscienceshow) [RSS](https://www.youtube.com/feeds/videos.xml?user=grossscienceshow)

#### Deep Look {#emDeepLook} 

> Vidéos de biologie avec usage de microscopes.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://kqed.org/deeplook](http://kqed.org/deeplook)  [![rss](img/rss.png)](http://kqed.org/deeplook)
- ![icone media](img/media.png) [youtube.com/user/KQEDDeepLook](https://www.youtube.com/user/KQEDDeepLook) [RSS](https://www.youtube.com/feeds/videos.xml?user=KQEDDeepLook)

#### BrainCraft {#emBrainCraft} 

> Neurosciences et psychologie.

Émission en 
anglais
.

Émission 
vidéo
.

Produit par PBS Digital Studio.


- ![icone media](img/media.png) [youtube.com/user/braincraftvideo](https://www.youtube.com/user/braincraftvideo) [RSS](https://www.youtube.com/feeds/videos.xml?user=braincraftvideo)

#### The brain scoop {#emBrainScoop} 

> 

Émission en 
anglais
.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/thebrainscoop](https://www.youtube.com/user/thebrainscoop) [RSS](https://www.youtube.com/feeds/videos.xml?user=thebrainscoop)

#### Macroscopie {#emMacroscopie} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCCSN5zJTKjl6UKvPcLzLuiw](https://www.youtube.com/channel/UCCSN5zJTKjl6UKvPcLzLuiw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCCSN5zJTKjl6UKvPcLzLuiw)

#### Strange Stuff and Funky Things {#emMacroscopie} 

> Flux RSS du blog tronqué.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.ssaft.com/Blog/dotclear/](http://www.ssaft.com/Blog/dotclear/)  [![rss](img/rss.png)](http://www.ssaft.com/Blog/dotclear/index.php?feed/atom)
- ![icone media](img/media.png) [youtube.com/channel/UCmQylwGI9yS9HtxA8f9WMTw](https://www.youtube.com/channel/UCmQylwGI9yS9HtxA8f9WMTw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCmQylwGI9yS9HtxA8f9WMTw)

#### Curiosity {#emMelvakCuriosity} 

> Biologie marine

Émission 
vidéo
.

Produit par Melvak.


- ![icone media](img/media.png) [youtube.com/user/StriderEscape](https://www.youtube.com/user/StriderEscape) [RSS](https://www.youtube.com/feeds/videos.xml?user=StriderEscape)

#### Curiosea {#emMelvakCuriosea} 

> Biologie marine

Émission 
vidéo
.

Produit par Melvak.


- ![icone media](img/media.png) [youtube.com/user/StriderEscape](https://www.youtube.com/user/StriderEscape) [RSS](https://www.youtube.com/feeds/videos.xml?user=StriderEscape)

#### The Dinosaur Show {#emDinosaurShow} 

> Émission sur les dinosaures.

Émission en 
anglais
.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCWIknNOGwPjdmRAXwNkhqDw](https://www.youtube.com/channel/UCWIknNOGwPjdmRAXwNkhqDw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCWIknNOGwPjdmRAXwNkhqDw)

#### Eons {#emEons} 

> a journey through the history of life on Earth

Émission en 
anglais
.

Émission 
vidéo
.

Produit par PBS Digital Studio.


- ![icone media](img/media.png) [youtube.com/channel/UCzR-rom72PHN9Zg7RML9EbA](https://www.youtube.com/channel/UCzR-rom72PHN9Zg7RML9EbA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCzR-rom72PHN9Zg7RML9EbA)

#### L’envers de la blouse {#emEnversBlouse} 

> Biologie de l’évolution.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/l-envers-de-la-blouse](https://www.tipeee.com/l-envers-de-la-blouse)
- ![icone media](img/media.png) [youtube.com/channel/UCZnVzAaoJu37Tkz6YJn4EUQ](https://www.youtube.com/channel/UCZnVzAaoJu37Tkz6YJn4EUQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCZnVzAaoJu37Tkz6YJn4EUQ)

#### Risque Alpha {#emRisqueA} 

> Histoire de la médecine et épidémiologie

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCJ7_Ld2cIVY5MM3NcKW3D8A](https://www.youtube.com/channel/UCJ7_Ld2cIVY5MM3NcKW3D8A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCJ7_Ld2cIVY5MM3NcKW3D8A)

#### La boîte à curiosités {#emBoiteCuriosites} 

> 
	  À mi-chemin entre vulgarisation et fiction, je souhaite sensibiliser 
	  chacun autour de la nature, pour vous pousser à l’apprécier en 
	  découvrant ses curiosités les plus farfelues !
	

Émission 
vidéo
.

- ![icone web](img/firefox.png) [https://www.laboiteacuriosites.fr/](https://www.laboiteacuriosites.fr/)  [![rss](img/rss.png)](https://www.laboiteacuriosites.fr/blog-feed.xml)
- ![icone media](img/media.png) [youtube.com/channel/UCg6ex3inSHm-O1R7QfOnd6A](https://www.youtube.com/channel/UCg6ex3inSHm-O1R7QfOnd6A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCg6ex3inSHm-O1R7QfOnd6A)

### Divers {#sciencemisc}


#### Tu mourras moins bête {#emTMMB} 

> Biologie de l’évolution.

Émission 
vidéo
.

Produit par ARTE Futur.

- ![icone web](img/firefox.png) [https://tumourrasmoinsbete.blogspot.com/](https://tumourrasmoinsbete.blogspot.com/)  [![rss](img/rss.png)](https://tumourrasmoinsbete.blogspot.com/feeds/posts/default)
- ![icone media](img/media.png) [youtube.com/channel/UCKtG_lXZk4pRJkapfK0eprA](https://www.youtube.com/channel/UCKtG_lXZk4pRJkapfK0eprA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCKtG_lXZk4pRJkapfK0eprA)

#### Epenser {#emEpenser} 

> Vulgarisation scientifique, surtout en physique/chimie, mais parfois de la biologie…

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/e-penser](https://www.tipeee.com/e-penser)- ![icone web](img/firefox.png) [http://e-penser.com/](http://e-penser.com/)  [![rss](img/rss.png)](http://e-penser.com/)
- ![icone media](img/media.png) [youtube.com/user/epenser1](https://www.youtube.com/user/epenser1) [RSS](https://www.youtube.com/feeds/videos.xml?user=epenser1)

#### Vsauce {#emVsauce1} 

> Vulgarisation en physique/biologie…

Émission en 
anglais
.

Émission 
vidéo
.

Produit par Vsauce.


- ![icone media](img/media.png) [youtube.com/user/Vsauce](https://www.youtube.com/user/Vsauce) [RSS](https://www.youtube.com/feeds/videos.xml?user=Vsauce)

#### Vsauce2 {#emVsauce2} 

> Mind-Blowing Science, Technology &amp; People

Émission en 
anglais
.

Émission 
vidéo
.

Produit par Vsauce.


- ![icone media](img/media.png) [youtube.com/user/Vsauce2](https://www.youtube.com/user/Vsauce2) [RSS](https://www.youtube.com/feeds/videos.xml?user=Vsauce2)

#### Vsauce3 {#emVsauce3} 

> Sciences et fiction

Émission en 
anglais
.

Émission 
vidéo
.

Produit par Vsauce.


- ![icone media](img/media.png) [youtube.com/user/Vsauce3](https://www.youtube.com/user/Vsauce3) [RSS](https://www.youtube.com/feeds/videos.xml?user=Vsauce3)

#### Kurzgesagt / In a Nutshell {#emKurzgesagt} 

> Vulgarisation scientifique, épisodes courts à l’animation splendide.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/Kurzgesagt](https://www.patreon.com/Kurzgesagt)- ![icone web](img/firefox.png) [http://www.kurzgesagt.org/](http://www.kurzgesagt.org/)  [![rss](img/rss.png)](http://www.kurzgesagt.org/)- ![icone web](img/firefox.png) [http://www.reddit.com/r/kurzgesagt](http://www.reddit.com/r/kurzgesagt)  [![rss](img/rss.png)](http://www.reddit.com/r/kurzgesagt)
- ![icone media](img/media.png) [youtube.com/user/Kurzgesagt](https://www.youtube.com/user/Kurzgesagt) [RSS](https://www.youtube.com/feeds/videos.xml?user=Kurzgesagt)

#### C.G.P.Grey {#emGGPGrey} 

> Vulgarisation scientifique, épisodes courts.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/cgpgrey](https://www.patreon.com/cgpgrey)- ![icone soutien](img/money.png) [http://store.dftba.com/collections/cgp-grey](http://store.dftba.com/collections/cgp-grey)
- ![icone media](img/media.png) [youtube.com/user/CGPGrey](https://www.youtube.com/user/CGPGrey) [RSS](https://www.youtube.com/feeds/videos.xml?user=CGPGrey)

#### Veritasium {#emVeritasium} 

> Vulgarisation scientifique, émission inspiratrice, entre autres d’Epenser…

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/veritasium](https://www.patreon.com/veritasium)
- ![icone media](img/media.png) [youtube.com/user/1veritasium](https://www.youtube.com/user/1veritasium) [RSS](https://www.youtube.com/feeds/videos.xml?user=1veritasium)

#### Sciencium {#emSciencium} 

> Vulgarisation scientifique, avec des émissions courtes, par le créateur de Veritasium.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/veritasium](https://www.patreon.com/veritasium)
- ![icone media](img/media.png) [youtube.com/user/sciencium](https://www.youtube.com/user/sciencium) [RSS](https://www.youtube.com/feeds/videos.xml?user=sciencium)

#### Dr Nozman {#emDrNozman} 

> 

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/dr-nozman](https://www.tipeee.com/dr-nozman)- ![icone web](img/firefox.png) [http://drnozman.com/](http://drnozman.com/)  [![rss](img/rss.png)](http://drnozman.com/)
- ![icone media](img/media.png) [youtube.com/user/DrNozman](https://www.youtube.com/user/DrNozman) [RSS](https://www.youtube.com/feeds/videos.xml?user=DrNozman)

#### La minute science {#emMinuteScience} 

> De la vulgarisation scientifique, en bref.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.boite-aux-curiosites.com/](http://www.boite-aux-curiosites.com/)  [![rss](img/rss.png)](http://www.boite-aux-curiosites.com/feed/)
- ![icone media](img/media.png) [youtube.com/user/LaMinuteScience](https://www.youtube.com/user/LaMinuteScience) [RSS](https://www.youtube.com/feeds/videos.xml?user=LaMinuteScience)

#### La science dans la fiction {#emLaScienceDLFiction} 

> Vulgarisation scientifique par le biais du cinéma

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCRq9T1K8rNdcnNSiWFGYQwA](https://www.youtube.com/channel/UCRq9T1K8rNdcnNSiWFGYQwA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCRq9T1K8rNdcnNSiWFGYQwA)

#### Professeur Sims {#emProfSims} 

> Vulgarisation de notions scientifiques diverses, surtout en chimie.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/professeur-sims](https://www.tipeee.com/professeur-sims)- ![icone web](img/firefox.png) [http://professeursims.wordpress.com/](http://professeursims.wordpress.com/)  [![rss](img/rss.png)](http://professeursims.wordpress.com/feed/)
- ![icone media](img/media.png) [youtube.com/user/ProfesseurSims](https://www.youtube.com/user/ProfesseurSims) [RSS](https://www.youtube.com/feeds/videos.xml?user=ProfesseurSims)

#### Prof Okita {#emProfOkita} 

> Vulgarisation scientifique à partir de la fiction.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/prof-okita](https://www.tipeee.com/prof-okita)
- ![icone media](img/media.png) [youtube.com/user/okita32](https://www.youtube.com/user/okita32) [RSS](https://www.youtube.com/feeds/videos.xml?user=okita32)

#### La main baladeuse {#emMainBaladeuse} 

> Vulgarisation scientifique avec du dessin/animation en stop-motion.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC4V8l0YMJ3IAOToCX5SdVug](https://www.youtube.com/channel/UC4V8l0YMJ3IAOToCX5SdVug) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC4V8l0YMJ3IAOToCX5SdVug)

#### Science de comptoir {#emScienceDeComptoir} 

> Biologie, géologie, astro, réflexions autour de la communication scientifique, un soupçon de zététique…

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://sciencedecomptoir.cafe-sciences.org/](http://sciencedecomptoir.cafe-sciences.org/)  [![rss](img/rss.png)](http://sciencedecomptoir.cafe-sciences.org/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCE_LWXcsdp7cWbo4DR4ZA-A](https://www.youtube.com/channel/UCE_LWXcsdp7cWbo4DR4ZA-A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCE_LWXcsdp7cWbo4DR4ZA-A)

#### String Theory FR {#emStringTheoryFR} 

> Vidéos scientifiques sur des sujets variés, par un collectif de vidéastes scientifiques.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.stringtheory.fr/](http://www.stringtheory.fr/)  [![rss](img/rss.png)](http://www.stringtheory.fr/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCfxwT02Bu5R7l21uMAu8H1w](https://www.youtube.com/channel/UCfxwT02Bu5R7l21uMAu8H1w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCfxwT02Bu5R7l21uMAu8H1w)

#### Experimentboy {#emExperimentBoy} 

> Vidéos scientifiques sur des sujets variés, par un collectif de vidéastes scientifiques.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/experimentboy](https://www.tipeee.com/experimentboy)- ![icone soutien](img/money.png) [https://www.amazon.fr/Experimentboy-Baptiste-Mortier-Dumont/dp/2081394782/](https://www.amazon.fr/Experimentboy-Baptiste-Mortier-Dumont/dp/2081394782/)
- ![icone media](img/media.png) [youtube.com/channel/UCfxwT02Bu5R7l21uMAu8H1w](https://www.youtube.com/channel/UCfxwT02Bu5R7l21uMAu8H1w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCfxwT02Bu5R7l21uMAu8H1w)

#### SciShow {#emSciShow} 

> SciShow explores the unexpected. Seven days a week, Hank Green, Michael Aranda, and Olivia Gordon delve into the scientific subjects that defy our expectations and make us even more curious !

Émission en 
anglais
.

Émission 
vidéo
.

Produit par SciShow.

- ![icone soutien](img/money.png) [patreon.com/scishow](https://www.patreon.com/scishow)- ![icone web](img/firefox.png) [http://scishow.tumblr.com/](http://scishow.tumblr.com/)  [![rss](img/rss.png)](http://scishow.tumblr.com/)
- ![icone media](img/media.png) [youtube.com/user/scishow](https://www.youtube.com/user/scishow) [RSS](https://www.youtube.com/feeds/videos.xml?user=scishow)

#### SciShow Kids {#emSciShowKids} 

> SciShow Kids explores all those curious topics that make us ask "why?"

Émission en 
anglais
.

Émission 
vidéo
.

Produit par SciShow.

- ![icone soutien](img/money.png) [patreon.com/scishow](https://www.patreon.com/scishow)- ![icone web](img/firefox.png) [http://scishow.tumblr.com/](http://scishow.tumblr.com/)  [![rss](img/rss.png)](http://scishow.tumblr.com/)
- ![icone media](img/media.png) [youtube.com/user/scishowkids](https://www.youtube.com/user/scishowkids) [RSS](https://www.youtube.com/feeds/videos.xml?user=scishowkids)

#### SciShow Space {#emSciShowSpace} 

> Every Tuesday and Friday, SciShow Space explores the universe a few minutes at a time.

Émission en 
anglais
.

Émission 
vidéo
.

Produit par SciShow.

- ![icone soutien](img/money.png) [patreon.com/scishow](https://www.patreon.com/scishow)- ![icone web](img/firefox.png) [http://scishow.tumblr.com/](http://scishow.tumblr.com/)  [![rss](img/rss.png)](http://scishow.tumblr.com/)
- ![icone media](img/media.png) [youtube.com/user/scishowspace](https://www.youtube.com/user/scishowspace) [RSS](https://www.youtube.com/feeds/videos.xml?user=scishowspace)

#### Billes de sciences {#emBillesSciences} 

> Vulgarisation des sciences naturelles par les expériences.

Émission 
vidéo
.

Produit par Fondation La main à la pâte.


- ![icone media](img/media.png) [youtube.com/channel/UCy1BMhZGRdFiOde2DqeQE0w](https://www.youtube.com/channel/UCy1BMhZGRdFiOde2DqeQE0w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCy1BMhZGRdFiOde2DqeQE0w)

#### Jensky – Conférence et intervention scientifique {#emJensky} 

> De tout sur la science : Conférence / Documentaire / Intervention scientifique.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCY7WOOsRoHAXSO4hhhAkVVQ](https://www.youtube.com/channel/UCY7WOOsRoHAXSO4hhhAkVVQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCY7WOOsRoHAXSO4hhhAkVVQ)

#### Scivolemo {#emScivolemo} 

> 

Émission en 
espéranto
.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [https://scivolemo.wordpress.com/](https://scivolemo.wordpress.com/)  [![rss](img/rss.png)](https://scivolemo.wordpress.com/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UCJy1LAuTnZjrov6QFWSagug](https://www.youtube.com/channel/UCJy1LAuTnZjrov6QFWSagug) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCJy1LAuTnZjrov6QFWSagug)

### Mathématiques {#maths}


#### Micmaths {#emMicmaths} 

> Vidéos sur les maths.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/micmaths](https://www.tipeee.com/micmaths)- ![icone web](img/firefox.png) [http://www.micmaths.com/](http://www.micmaths.com/)  [![rss](img/rss.png)](http://www.micmaths.com/feed/)
- ![icone media](img/media.png) [youtube.com/user/Micmaths](https://www.youtube.com/user/Micmaths) [RSS](https://www.youtube.com/feeds/videos.xml?user=Micmaths)

#### Numberphile {#emNumberphile} 

> Videos about numbers - it's that simple. 

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/numberphile](https://www.patreon.com/numberphile)- ![icone web](img/firefox.png) [http://www.numberphile.com/](http://www.numberphile.com/)  [![rss](img/rss.png)](http://www.numberphile.com/)
- ![icone media](img/media.png) [youtube.com/user/numberphile](https://www.youtube.com/user/numberphile) [RSS](https://www.youtube.com/feeds/videos.xml?user=numberphile)

#### Vi Hart {#emViHart} 

> 

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://vihart.com/](http://vihart.com/)  [![rss](img/rss.png)](http://vihart.com/feed/)
- ![icone media](img/media.png) [youtube.com/user/Vihart](https://www.youtube.com/user/Vihart) [RSS](https://www.youtube.com/feeds/videos.xml?user=Vihart)

#### Infinite Series {#emInfiniteSeries} 

> La réalité vue sous l’angle des mathématiques.

Émission en 
anglais
.

Émission 
vidéo
.

Produit par PBS Digital Studio.


- ![icone media](img/media.png) [youtube.com/channel/UCs4aHmggTfFrpkPcWSaBN9g](https://www.youtube.com/channel/UCs4aHmggTfFrpkPcWSaBN9g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCs4aHmggTfFrpkPcWSaBN9g)

#### Sophie Guichard {#emSGuichard} 

> Cours de maths en vidéo du collège au post-bac (bts, iut, master…), avec des exemples complètement corrigés et des exercices types…

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCUOkby6QB3NiwUWRD8hnOsw](https://www.youtube.com/channel/UCUOkby6QB3NiwUWRD8hnOsw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCUOkby6QB3NiwUWRD8hnOsw)

### Médecine {#med}


#### Climen {#emClimen} 

> Émission sur la médicamentation, la pharmacologie.Pas d’imbécilités homéopathiques ou/et « alternatives ».

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://blogs.lexpress.fr/climen-video-sante/](http://blogs.lexpress.fr/climen-video-sante/)  [![rss](img/rss.png)](http://blogs.lexpress.fr/climen-video-sante/)
- ![icone media](img/media.png) [youtube.com/user/Flexouz](https://www.youtube.com/user/Flexouz) [RSS](https://www.youtube.com/feeds/videos.xml?user=Flexouz)

#### Dans ton corps {#emDTCorps} 

> Chaque semaine sur Dans Ton Corps, Julien Ménielle répond à vos questions sur la santé et le corps humain.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCOchT7ZJ4TXe3stdLW1Sfxw](https://www.youtube.com/channel/UCOchT7ZJ4TXe3stdLW1Sfxw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCOchT7ZJ4TXe3stdLW1Sfxw)

#### Ascl&amp;pios {#emAsclepios} 

> Vulgarisation médicale avec une orientation historique.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCDqEttzOpPbDoeC05HRPPDQ](https://www.youtube.com/channel/UCDqEttzOpPbDoeC05HRPPDQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCDqEttzOpPbDoeC05HRPPDQ)

#### Histomède {#emAsclepios} 

> Chaîne didactique de vulgarisation [vulgarisation et didactique, pléonasme] sur l’histoire de la médecine.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC2-_u-QQ4_nS2cEuRUSsm8Q](https://www.youtube.com/channel/UC2-_u-QQ4_nS2cEuRUSsm8Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC2-_u-QQ4_nS2cEuRUSsm8Q)

#### Un peu pointu {#emUPPointu} 

> Vulgarisation médicale

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCj2sb4hak2L_UpW-1AQ6Qhw](https://www.youtube.com/channel/UCj2sb4hak2L_UpW-1AQ6Qhw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCj2sb4hak2L_UpW-1AQ6Qhw)

#### Osmose {#emOsmose} 

> Expliquer certains sujets médicaux tels que la physiopathologie de manière approfondie et imagée au moyen de vidéos super courtes, amusantes et complètes.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [patreon.com/osmosis](https://www.patreon.com/osmosis)
- ![icone media](img/media.png) [youtube.com/channel/UCNI0qOojpkhsUtaQ4_2NUhQ](https://www.youtube.com/channel/UCNI0qOojpkhsUtaQ4_2NUhQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCNI0qOojpkhsUtaQ4_2NUhQ)

#### Ados j’écoute {#emAdosJEcoute} 

> 
	  À l’Hôtel-Dieu à Paris, l’espace Santé Jeunes accueille les ados en 
	  rupture. Le Dr Dinah Vernant et son équipe sont à l’écoute des 
	  problèmes liés au corps : drogues, surpoids, contraception, 
	  violence… Une série hospitalière sur les maux du corps de Claire 
	  Hauter pour ARTE Radio.
	

Émission 
audio
.

Produit par ARTE Radio.

- ![icone rss](img/rss.png) [](https://www.arteradio.com/xml_sound_serie?seriename=%22ADOS%20J%27%C3%89COUTE%22)- ![icone web](img/firefox.png) [https://www.arteradio.com/serie/ados_j_ecoute](https://www.arteradio.com/serie/ados_j_ecoute)  [![rss](img/rss.png)](https://www.arteradio.com/serie/ados_j_ecoute)

### Physique / Astronomie {#cosmos}


#### Lanterne Cosmique {#emLanterneCosmique} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/lanternecosmique](https://www.youtube.com/user/lanternecosmique) [RSS](https://www.youtube.com/feeds/videos.xml?user=lanternecosmique)

#### ScienceClic {#emScienceClic} 

> En quelques minutes, découvrez et comprenez les théories scientifiques les plus complexes, à la base du cosmos.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/ScienceClic](https://www.youtube.com/user/ScienceClic) [RSS](https://www.youtube.com/feeds/videos.xml?user=ScienceClic)

#### Scilabus {#emScilabus} 

> Chaine de vulgarisation scientifique qui fait la part belle aux expériences.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://scilabus.com/](http://scilabus.com/)  [![rss](img/rss.png)](http://scilabus.com/feed/)
- ![icone media](img/media.png) [youtube.com/user/scilabus](https://www.youtube.com/user/scilabus) [RSS](https://www.youtube.com/feeds/videos.xml?user=scilabus)

#### Science4All {#emScience4AllFR} 

> Une émission de «  médiation scientifique francophone ». Existe aussi en anglais.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://fr.science4all.org/](http://fr.science4all.org/)  [![rss](img/rss.png)](http://fr.science4all.org/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UC0NCbj8CxzeCGIF6sODJ-7A](https://www.youtube.com/channel/UC0NCbj8CxzeCGIF6sODJ-7A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC0NCbj8CxzeCGIF6sODJ-7A)

#### Science4All {#emScience4AllEN} 

>  A channel of science and mathematics popularization ». Existe aussi en français.

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://science4all.org/](http://science4all.org/)  [![rss](img/rss.png)](http://science4all.org/feed/)
- ![icone media](img/media.png) [youtube.com/user/Science4AllOrg](https://www.youtube.com/user/Science4AllOrg) [RSS](https://www.youtube.com/feeds/videos.xml?user=Science4AllOrg)

#### Physics Girl {#emPhysicsGirl} 

>  Physics videos for every atom and eve ».

Émission en 
anglais
.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [https://secure.squarespace.com/commerce/donate?donatePageId=54370b55e4b07f1e91b36df4](https://secure.squarespace.com/commerce/donate?donatePageId=54370b55e4b07f1e91b36df4)
- ![icone media](img/media.png) [youtube.com/user/physicswoman](https://www.youtube.com/user/physicswoman) [RSS](https://www.youtube.com/feeds/videos.xml?user=physicswoman)

#### Florence Porcel {#emFlorencePorcel} 

> L’actualité des sciences de l’Univers et du spatial, c’est dans « La folle histoire de l’Univers ». Les bourdes trouvées à la télévision, c’est dans « Les perles du PAF »  ».

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/florenceporcel](https://www.tipeee.com/florenceporcel)
- ![icone media](img/media.png) [youtube.com/channel/UC9HapjjoLqdDNwKEWQRaiyA](https://www.youtube.com/channel/UC9HapjjoLqdDNwKEWQRaiyA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC9HapjjoLqdDNwKEWQRaiyA)

#### Sense of Wonder {#emSenseOfWonder} 

> Une émission sur la Vie, l’Univers et le Reste… Mais surtout l’Univers.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://lesenseofwonder.blogspot.com/](http://lesenseofwonder.blogspot.com/)  [![rss](img/rss.png)](http://lesenseofwonder.blogspot.com/feeds/posts/default)
- ![icone media](img/media.png) [youtube.com/channel/UCjsHDXUU3BjBCG7OaCbNDyQ](https://www.youtube.com/channel/UCjsHDXUU3BjBCG7OaCbNDyQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCjsHDXUU3BjBCG7OaCbNDyQ)

#### Tentatives {#emTentatives} 

> Science et œuvres d’art.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://essaiornotessai.wixsite.com/tentatives/](http://essaiornotessai.wixsite.com/tentatives/)  [![rss](img/rss.png)](http://essaiornotessai.wixsite.com/tentatives/feed.xml)
- ![icone media](img/media.png) [youtube.com/user/essaiornot](https://www.youtube.com/user/essaiornot) [RSS](https://www.youtube.com/feeds/videos.xml?user=essaiornot)

#### I need space {#emINeedSpace} 

> Lire le ciel.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCW-Qte750fJkyM-geafGf2Q](https://www.youtube.com/channel/UCW-Qte750fJkyM-geafGf2Q) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCW-Qte750fJkyM-geafGf2Q)

#### Scicos {#emScicos} 

> Vidéos courtes sur l’astronomie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCIPAu43SNmPwqbjFmUYxLdg](https://www.youtube.com/channel/UCIPAu43SNmPwqbjFmUYxLdg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCIPAu43SNmPwqbjFmUYxLdg)

#### Le petit astronome {#emLPAstronome} 

> Le Petit Astronome vous invite à découvrir les secrets et les faits incroyables de l’Univers

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCyeoIdJHgQhOrct9mVJ2fTQ](https://www.youtube.com/channel/UCyeoIdJHgQhOrct9mVJ2fTQ) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCyeoIdJHgQhOrct9mVJ2fTQ)

#### StarTalk {#emStarTalk} 

> 
	  Neil deGrasse Tyson, his comic co-hosts, guest celebrities &amp; 
	  scientists discuss astronomy, physics, and everything else about 
	  life in the universe
	

Émission en 
anglais
.

Émission 
audio
.

- ![icone soutien](img/money.png) [patreon.com/startalkradio](https://www.patreon.com/startalkradio)- ![icone rss](img/rss.png) [](https://feeds.soundcloud.com/users/soundcloud:users:38128127/sounds.rss)- ![icone web](img/firefox.png) [http://www.startalkradio.net/](http://www.startalkradio.net/)  [![rss](img/rss.png)](http://www.startalkradio.net/)
- ![icone media](img/media.png) [youtube.com/user/startalkradio](https://www.youtube.com/user/startalkradio) [RSS](https://www.youtube.com/feeds/videos.xml?user=startalkradio)

#### Space Time {#emSpaceTime} 

> 
	  Space Time explores the outer reaches of space, the craziness of 
	  astrophysics, the possibilities of sci-fi, and anything else you can 
	  think of beyond Planet Earth
	

Émission en 
anglais
.

Émission 
vidéo
.

Produit par PBS Digital Studio.

- ![icone soutien](img/money.png) [patreon.com/pbsspacetime](https://www.patreon.com/pbsspacetime)- ![icone rss](img/rss.png) [](http://feeds.feedburner.com/PBSSpaceTime)
- ![icone media](img/media.png) [youtube.com/channel/UC7_gcs09iThXybpVgjHZ_7g](https://www.youtube.com/channel/UC7_gcs09iThXybpVgjHZ_7g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC7_gcs09iThXybpVgjHZ_7g)

#### Ideas in Science {#emLHonnorat} 

> 
	  Une vidéothèque du savoir en sciences, multidisciplinaire, 
	  multiculturelle, pérenne et accessible à tous.
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/HonnoratLh](https://www.youtube.com/user/HonnoratLh) [RSS](https://www.youtube.com/feeds/videos.xml?user=HonnoratLh)

#### Alice au pays des étoiles {#emAliceEtoiles} 

> Communication scientifique sur l’astronomie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCFyTyQRqIj9pcpu7Xsu-U4w](https://www.youtube.com/channel/UCFyTyQRqIj9pcpu7Xsu-U4w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCFyTyQRqIj9pcpu7Xsu-U4w)

### Psychologie {#psycho}


#### Le Psylab {#emPsylab} 

> 

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/le-psylab](https://www.tipeee.com/le-psylab)
- ![icone media](img/media.png) [youtube.com/user/lepsylab](https://www.youtube.com/user/lepsylab) [RSS](https://www.youtube.com/feeds/videos.xml?user=lepsylab)

#### Une araignée au plafond {#emAraigneePlafond} 

> 

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/user/Khertanis](https://www.youtube.com/user/Khertanis) [RSS](https://www.youtube.com/feeds/videos.xml?user=Khertanis)

#### La psy qui parle {#emPsyQuiParle} 

> 
	  [L]a chaine de vulgarisation en PSYCHOLOGIE scientifique qui 
	  s’adresse à tous !
	

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UCjFDAyjR5j8KqzxIkgCYgmA](https://www.youtube.com/channel/UCjFDAyjR5j8KqzxIkgCYgmA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCjFDAyjR5j8KqzxIkgCYgmA)

#### Neurovision {#emNeurovision} 

> [J]'étudie la psychologie. Mon but est de rendre mon public attentif aux fausses croyances dans cette branche passionnante mais qui a tendance à manquer de sceptiques.

Émission 
vidéo
.


- [neurovision_laure_weiss](neurovision_laure_weiss)

- ![icone media](img/media.png) [youtube.com/channel/UCag73lWsCTMOmV7d_uG2ArA](https://www.youtube.com/channel/UCag73lWsCTMOmV7d_uG2ArA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCag73lWsCTMOmV7d_uG2ArA)

#### Psynect {#emPsynect} 

> 

Émission 
vidéo
.

- ![icone soutien](img/money.png) [tipeee.com/psynect](https://www.tipeee.com/psynect)
- ![icone media](img/media.png) [youtube.com/channel/UCwWCBw7M0kskNlv0coVP53w](https://www.youtube.com/channel/UCwWCBw7M0kskNlv0coVP53w) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCwWCBw7M0kskNlv0coVP53w)

#### La psychothèque {#emPsychotheque} 

> Psychologie et pop culture.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC_GkEnNIFPbG2RCzNwDsz6A](https://www.youtube.com/channel/UC_GkEnNIFPbG2RCzNwDsz6A) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC_GkEnNIFPbG2RCzNwDsz6A)

### Écologie {#ecologie}


#### Game Of Hearth {#emGameOfHearth} 

> Émission de vulgarisation sur l’écologie.

Émission 
vidéo
.


- ![icone media](img/media.png) [youtube.com/channel/UC3A_TG1leX0eQEJD1Ew6Ftw](https://www.youtube.com/channel/UC3A_TG1leX0eQEJD1Ew6Ftw) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC3A_TG1leX0eQEJD1Ew6Ftw)

### Électronique {#elec}


#### DEUS EX SILICIUM {#emDESilicium} 

> DEUS EX SILICIUM est le premier projet francophone de chaine d’électronique dédiée à la science des semi-conducteurs et à leurs très nombreuses applications. Imaginée pour les ingénieurs, hackeurs, amateurs, étudiants, geeks ou passionnés de technologie, elle est diffusée en fullHD sur Youtube et Dailymotion.

Émission 
vidéo
.


- ![icone media](img/media.png) [dailymotion.com/dexsilicium](https://www.dailymotion.com/dexsilicium)  [RSS](https://www.dailymotion.com/rss/user/dexsilicium)
- ![icone web](img/firefox.png) [http://www.dexsilicium.com/](http://www.dexsilicium.com/)  [![rss](img/rss.png)](http://www.dexsilicium.com/)
- ![icone media](img/media.png) [youtube.com/user/dexsilicium](https://www.youtube.com/user/dexsilicium) [RSS](https://www.youtube.com/feeds/videos.xml?user=dexsilicium)

#### U=RI {#emURI} 

> Vulgarisation sur l’électronique.

Émission 
vidéo
.

- ![icone soutien](img/money.png) [http://laboutiquedumaker.com/](http://laboutiquedumaker.com/)- ![icone web](img/firefox.png) [http://www.les-electroniciens.com/](http://www.les-electroniciens.com/)  [![rss](img/rss.png)](http://www.les-electroniciens.com/)
- ![icone media](img/media.png) [youtube.com/channel/UCVqx3vXNghSqUcVg2nmegYA](https://www.youtube.com/channel/UCVqx3vXNghSqUcVg2nmegYA) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCVqx3vXNghSqUcVg2nmegYA)

### Éthologie {#ethologie}


#### Cervelle d’oiseau {#emCervelleOiseau} 

> [U]ne chaîne de vulgarisation scientifique […] sur l’intelligence et le comportement animal.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.cervelledoiseau.fr/](http://www.cervelledoiseau.fr/)  [![rss](img/rss.png)](http://www.cervelledoiseau.fr/feed/)
- ![icone media](img/media.png) [youtube.com/channel/UC2EYhsS52ykT6sQH5FXoY7g](https://www.youtube.com/channel/UC2EYhsS52ykT6sQH5FXoY7g) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UC2EYhsS52ykT6sQH5FXoY7g)

#### Le nid de pie {#emNidPie} 

> Agatha, docteure en éthologie, vous emmène à la découverte de bestioles en tous genres.

Émission 
vidéo
.

- ![icone web](img/firefox.png) [http://www.leniddepie.com](http://www.leniddepie.com)  [![rss](img/rss.png)](http://www.leniddepie.com/feeds/posts/default)
- ![icone media](img/media.png) [youtube.com/channel/UCIun3hEuCFdfCK6TMcOjoWg](https://www.youtube.com/channel/UCIun3hEuCFdfCK6TMcOjoWg) [RSS](https://www.youtube.com/feeds/videos.xml?channel_id=UCIun3hEuCFdfCK6TMcOjoWg)
