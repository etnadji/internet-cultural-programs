<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="/"># Internet Cultural Programs

## Français

Une (énorme) liste d’émissions culturelles et/ou sérieuses sur Internet.

Voir la liste complète&#160;:

- [sur ce dépôt](Emissions.md)
- [sur un site dedié](https://etnadji.frama.io)
- [sur mon site](https://etnadji.fr/rsc/emissions/reco.xml)

Cette liste comporte actuellement <xsl:value-of select="count(//emission)"/> émissions,
dont <xsl:value-of select="count(//lang[.!='fr'])"/> en langues étrangères.

## English

A (giant) list of cultural&#160;/&#160;serious shows available on the Internet.

View the full list :

- [on this repository](Emissions.md)
- [on a dedicated website](https://etnadji.frama.io)
- [on my website](https://etnadji.fr/rsc/emissions/reco.xml)

The <xsl:value-of select="count(//emission)"/> shows listed here are mostly in 
french, but there is also <xsl:value-of select="count(//lang[.!='fr'])"/> shows in
english&#160;/&#160;other languages.

## License

[![CC0](http://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, [Etienne Nadji](https://etnadji.fr) has waived all copyright and related or neighboring rights to this work.

## Sommaire / Table of Contents

<xsl:for-each select="/emissions/category">
  <xsl:sort select="@name" order="ascending" data-type="text" />
* <xsl:value-of select="@name" />
    <xsl:choose>
      <xsl:when test="count(./emission) = 0">&#160;(<xsl:value-of select="count(./*/emission)"/>)</xsl:when>
      <xsl:otherwise>&#160;(<xsl:value-of select="count(./emission)"/>)</xsl:otherwise>
    </xsl:choose>

    <xsl:for-each select="subCategory">
      <xsl:sort select="@name" order="ascending" data-type="text" />
    * <xsl:value-of select="@name" /><xsl:if test="count(./emission)">&#160;(<xsl:value-of select="count(./emission)"/>)</xsl:if>
    </xsl:for-each>
</xsl:for-each>

## Stats

### Hébergement des vidéos / Videos hosting

- Peertube&#160;: <xsl:value-of select="count(//url[@type = 'pt'])"/>
- Youtube&#160;: <xsl:value-of select="count(//url[@type = 'yt'])"/>
- Dailymotion&#160;: <xsl:value-of select="count(//url[@type = 'dlm'])"/>

</xsl:template>

</xsl:stylesheet>
