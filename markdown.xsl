<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<!--
     XSL stylesheet for Markdown conversion of reco.xml
-->

<xsl:template match="/"># Émissions culturelles sur Internet

## À propos

### Contenu

Ce document a pour ambition de lister la plupart des émissions
culturelles, ou tout du moins «&#160;sérieuses&#160;» présentes
sur Internet.

À ce jour, <xsl:value-of select="count(//emission)"/> émissions
font partie de cette liste. Dont <xsl:value-of select="count(//lang[.!='fr'])"/> en
langue étrangère.

#### Langues

- Français : <xsl:value-of select="count(//lang[.='fr'])"/>
- Anglais : <xsl:value-of select="count(//lang[.='en'])"/>
- Esperanto : <xsl:value-of select="count(//lang[.='eo'])"/>

### Legende

Ce document n’est qu’une liste d’émissions avec un nombre variable de liens 
en rapport avec chaque émission.

Pour s’y retrouver, chaque lien est préfixé d’une icone.

#### Icones

- ![icone web](img/firefox.png) Sites web divers.

- ![icone rss](img/rss.png) Flux [RSS](http://sametmax.com/quest-ce-quun-flux-rss-et-a-quoi-ca-sert/) seul.

- &#xf1c8; Youtube, liens directs vers de la vidéo.

- &#xf099; Compte Twitter.

- &#xf1ed; Moyens de soutenir l’émission: Tipee, vente de T-Shirts, livres…

### OPML

Une version OPML, mise à jour de temps en temps est 
disponible [ici](http://etnadji.fr/emissions/reco.opml).

Cette version exclut tout les liens ne pointant pas vers du RSS mais
comporte des liens RSS créés automatiquement à partir des
adresses de chaines Youtube.

Pour une obtenir une version  OPML forcément à jour, téléchargez
[le fichier XML](http://etnadji.fr/emissions/reco.xml) et transformez 
le à l’aide de [ce script Python](http://etnadji.fr/emissions/toOpml.py).

<hr/>

Dernière mise à jour : <xsl:value-of select="/emissions/update"/>.

<hr/>

## Sommaire

<!-- Table des matières -->

<xsl:for-each select="/emissions/category">
  <xsl:sort select="@name" order="ascending" data-type="text" />
- [<xsl:value-of select="@name" />](#<xsl:value-of select="@anchor" />) <xsl:choose><xsl:when test="count(./emission) = 0">&#160;<xsl:value-of select="count(./*/emission)"/></xsl:when><xsl:otherwise>&#160;<xsl:value-of select="count(./emission)"/></xsl:otherwise></xsl:choose><xsl:text>
</xsl:text>
<xsl:for-each select="subCategory">
  <xsl:sort select="@name" order="ascending" data-type="text" />
<xsl:text>    </xsl:text>- [<xsl:value-of select="@name" />](#<xsl:value-of select="@anchor" />) <xsl:if test="count(./emission)">&#160;<xsl:value-of select="count(./emission)"/></xsl:if><xsl:text>
</xsl:text>
</xsl:for-each>
</xsl:for-each>

<!-- Contenu -->

<xsl:for-each select="/emissions/category">
<xsl:sort select="@name" order="ascending" data-type="text" />
<xsl:text>
</xsl:text><hr/>

## <xsl:value-of select="@name" /> {#<xsl:value-of select="@anchor" />} 

<xsl:apply-templates select="emission"/>

<xsl:for-each select="subCategory">
<xsl:sort select="@name" order="ascending" data-type="text" />
<xsl:text>
</xsl:text>
### <xsl:value-of select="@name" /> {#<xsl:value-of select="@anchor" />}<xsl:text>
</xsl:text>
<xsl:apply-templates select="emission"/>
</xsl:for-each>

</xsl:for-each>
</xsl:template>

<xsl:template match="/emissions/category/emission">
<xsl:text>
</xsl:text>

<xsl:choose>
<xsl:when test="@id">
### <xsl:value-of select="title" /> {#<xsl:value-of select="@id" />} 
</xsl:when>
<xsl:otherwise>
### <xsl:value-of select="title" />
</xsl:otherwise>
</xsl:choose>

<xsl:text>
</xsl:text>

<xsl:text disable-output-escaping="yes">> </xsl:text><xsl:value-of select="desc" />

<xsl:text>
</xsl:text>

<xsl:call-template name="emLang">
  <xsl:with-param name="lang"><xsl:value-of select="lang" /></xsl:with-param>
</xsl:call-template>

<xsl:call-template name="emMedia">
  <xsl:with-param name="media"><xsl:value-of select="@media" /></xsl:with-param>
</xsl:call-template>

<xsl:if test="producedBy">
<xsl:call-template name="emProducedBy">
  <xsl:with-param name="pb"><xsl:value-of select="producedBy" /></xsl:with-param>
</xsl:call-template>
</xsl:if>

<xsl:if test="links/url"><xsl:text>
</xsl:text></xsl:if>

<xsl:for-each select="links/url">
  <xsl:sort select="@type" order="ascending" data-type="text" />
  <xsl:choose>
    <xsl:when test="@type = 'web'">
    <xsl:call-template name="emWebLink">
      <xsl:with-param name="link"><xsl:value-of select="." /></xsl:with-param>
      <xsl:with-param name="rss"><xsl:value-of select="@rssUrl" /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'rss'">
    <xsl:call-template name="emRssLink">
      <xsl:with-param name="link"><xsl:value-of select="." /></xsl:with-param>
      <xsl:with-param name="text"><xsl:value-of select="@text" /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'yt'">
    <xsl:call-template name="emYtLink">
      <xsl:with-param name="type"><xsl:value-of select="@subType" /></xsl:with-param>
      <xsl:with-param name="id"><xsl:value-of select="." /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'dlm'">
    <xsl:call-template name="emDlmLink">
      <xsl:with-param name="id"><xsl:value-of select="." /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'tw'">
    <xsl:call-template name="emTwLink">
      <xsl:with-param name="id"><xsl:value-of select="." /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'money'">
    <xsl:call-template name="emMoneyLink">
      <xsl:with-param name="id"><xsl:value-of select="." /></xsl:with-param>
      <xsl:with-param name="type"><xsl:value-of select="@subType" /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'live'">
    <xsl:call-template name="emLiveLink">
      <xsl:with-param name="id"><xsl:value-of select="." /></xsl:with-param>
      <xsl:with-param name="type"><xsl:value-of select="@subType" /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="emMiscLink">
	<xsl:with-param name="link"><xsl:value-of select="." /></xsl:with-param>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:for-each>
</xsl:template>

<xsl:template match="/emissions/category/subCategory/emission">
<xsl:text>
</xsl:text>

<xsl:choose>
<xsl:when test="@id">
#### <xsl:value-of select="title" /> {#<xsl:value-of select="@id" />} 
</xsl:when>
<xsl:otherwise>
#### <xsl:value-of select="title" />
</xsl:otherwise>
</xsl:choose>

<xsl:text>
</xsl:text>

<xsl:text disable-output-escaping="yes">> </xsl:text><xsl:value-of select="desc" />

<xsl:text>
</xsl:text>

<xsl:call-template name="emLang">
  <xsl:with-param name="lang"><xsl:value-of select="lang" /></xsl:with-param>
</xsl:call-template>

<xsl:call-template name="emMedia">
  <xsl:with-param name="media"><xsl:value-of select="@media" /></xsl:with-param>
</xsl:call-template>

<xsl:if test="producedBy">
<xsl:call-template name="emProducedBy">
  <xsl:with-param name="pb"><xsl:value-of select="producedBy" /></xsl:with-param>
</xsl:call-template>
</xsl:if>

<xsl:if test="links/url"><xsl:text>
</xsl:text></xsl:if>

<xsl:for-each select="links/url">
  <xsl:sort select="@type" order="ascending" data-type="text" />
  <xsl:choose>
    <xsl:when test="@type = 'web'">
    <xsl:call-template name="emWebLink">
      <xsl:with-param name="link"><xsl:value-of select="." /></xsl:with-param>
      <xsl:with-param name="rss"><xsl:value-of select="@rssUrl" /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'rss'">
    <xsl:call-template name="emRssLink">
      <xsl:with-param name="link"><xsl:value-of select="." /></xsl:with-param>
      <xsl:with-param name="text"><xsl:value-of select="@text" /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'yt'">
    <xsl:call-template name="emYtLink">
      <xsl:with-param name="type"><xsl:value-of select="@subType" /></xsl:with-param>
      <xsl:with-param name="id"><xsl:value-of select="." /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'dlm'">
    <xsl:call-template name="emDlmLink">
      <xsl:with-param name="id"><xsl:value-of select="." /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'tw'">
    <xsl:call-template name="emTwLink">
      <xsl:with-param name="id"><xsl:value-of select="." /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'md'">
    <xsl:call-template name="emMastodonLink">
      <xsl:with-param name="id"><xsl:value-of select="." /></xsl:with-param>
      <xsl:with-param name="instance"><xsl:value-of select="@instance" /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'money'">
    <xsl:call-template name="emMoneyLink">
      <xsl:with-param name="id"><xsl:value-of select="." /></xsl:with-param>
      <xsl:with-param name="type"><xsl:value-of select="@subType" /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:when test="@type = 'live'">
    <xsl:call-template name="emLiveLink">
      <xsl:with-param name="id"><xsl:value-of select="." /></xsl:with-param>
      <xsl:with-param name="type"><xsl:value-of select="@subType" /></xsl:with-param>
    </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="emMiscLink">
	<xsl:with-param name="link"><xsl:value-of select="." /></xsl:with-param>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:for-each>
</xsl:template>

<xsl:template name="emMiscLink">
<xsl:param name="link"></xsl:param>
<xsl:if test="$link">
- [<xsl:value-of select="$link" />](<xsl:value-of select="$link" />)
</xsl:if>
</xsl:template>

<xsl:template name="emYtLink">
<xsl:param name="type"></xsl:param>
<xsl:param name="id"></xsl:param>
<xsl:if test="$type">
<xsl:if test="$id">
- <xsl:choose>
<xsl:when test="$type = 'channel'">![icone media](img/media.png) [youtube.com/channel/<xsl:value-of select="$id" />](https://www.youtube.com/channel/<xsl:value-of select="$id" />)</xsl:when>
<xsl:when test="$type = 'user'">![icone media](img/media.png) [youtube.com/user/<xsl:value-of select="$id" />](https://www.youtube.com/user/<xsl:value-of select="$id" />)</xsl:when>
<xsl:otherwise>![icone media](img/media.png) [<xsl:value-of select="$id" />](<xsl:value-of select="$id" />)</xsl:otherwise>
</xsl:choose>
<xsl:text> </xsl:text>
<xsl:choose>
<xsl:when test="$type = 'user'">[RSS](https://www.youtube.com/feeds/videos.xml?user=<xsl:value-of select="$id" />)</xsl:when>
<xsl:when test="$type = 'channel'">[RSS](https://www.youtube.com/feeds/videos.xml?channel_id=<xsl:value-of select="$id" />)</xsl:when>
</xsl:choose>
</xsl:if>
</xsl:if>
</xsl:template>

<xsl:template name="emDlmLink">
<xsl:param name="id"></xsl:param>
- ![icone media](img/media.png) [dailymotion.com/<xsl:value-of select="$id" />](https://www.dailymotion.com/<xsl:value-of select="$id" />) <xsl:text> </xsl:text>[RSS](https://www.dailymotion.com/rss/user/<xsl:value-of select="$id" />)
</xsl:template>

<xsl:template name="emTwLink">
<xsl:param name="id"></xsl:param>
- ![icone twitter](img/twitter.png) [@<xsl:value-of select="$id" />](https://twitter.com/<xsl:value-of select="$id" />)
</xsl:template>

<xsl:template name="emMastodonLink">
<xsl:param name="id"></xsl:param>
<xsl:param name="instance"></xsl:param>
- ![icone mastodon](img/mastodon.png) [<xsl:value-of select="$id" />@<xsl:value-of select="$instance" />](https://<xsl:value-of select="$instance" />/@<xsl:value-of select="$id" />)
</xsl:template>

<xsl:template name="emMoneyLink">
<xsl:param name="id"></xsl:param>
<xsl:param name="type"></xsl:param>
<xsl:choose>
<xsl:when test="$type = 'tipee'">- ![icone soutien](img/money.png) [tipeee.com/<xsl:value-of select="$id" />](https://www.tipeee.com/<xsl:value-of select="$id" />)</xsl:when>
<xsl:when test="$type = 'patreon'">- ![icone soutien](img/money.png) [patreon.com/<xsl:value-of select="$id" />](https://www.patreon.com/<xsl:value-of select="$id" />)</xsl:when>
<xsl:when test="$type = 'utip'">- ![icone soutien](img/money.png) [utip.io/<xsl:value-of select="$id" />](https://utip.io/<xsl:value-of select="$id" />)</xsl:when>
<xsl:otherwise>- ![icone soutien](img/money.png) [<xsl:value-of select="$id" />](<xsl:value-of select="$id" />)</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="emLiveLink">
<xsl:param name="id"></xsl:param>
<xsl:param name="type"></xsl:param>
<xsl:choose>
<xsl:when test="$type = 'twitch'">- ![icone live](img/live.png) [twitch.tv/<xsl:value-of select="$id" />](https://www.twitch.tv/<xsl:value-of select="$id" />)</xsl:when>
<xsl:otherwise>- ![icone live](img/live.png) [<xsl:value-of select="$id" />](<xsl:value-of select="$id" />)</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="emRssLink">
<xsl:param name="text"></xsl:param>
<xsl:param name="link"></xsl:param>
<xsl:choose>
<xsl:when test="$text">- ![icone rss](img/rss.png) [<xsl:value-of select="$text" />](<xsl:value-of select="$link" />)</xsl:when>
<xsl:otherwise>- ![icone rss](img/rss.png) [<xsl:value-of select="$link" />](<xsl:value-of select="$link" />)</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="emWebLink">
<xsl:param name="rss"></xsl:param>
<xsl:param name="link"></xsl:param>
<xsl:choose>
<xsl:when test="$rss">- ![icone web](img/firefox.png) [<xsl:value-of select="$link" />](<xsl:value-of select="$link" />)<xsl:text> </xsl:text> [![rss](img/rss.png)](<xsl:value-of select="$link" /><xsl:value-of select="$rss" />)</xsl:when>
<xsl:otherwise>- ![icone web](img/firefox.png) [<xsl:value-of select="$link" />](<xsl:value-of select="$link" />)</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="emLang">
<xsl:param name="lang"></xsl:param>
<xsl:if test="$lang != 'fr'">
<xsl:choose>
<xsl:when test="$lang">
Émission en 
<xsl:choose>
<xsl:when test="$lang = 'en'">anglais</xsl:when>
<xsl:when test="$lang = 'fr'">français</xsl:when>
<xsl:when test="$lang = 'eo'">espéranto</xsl:when>
</xsl:choose>
.
</xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:if>
</xsl:template>

<xsl:template name="emMedia">
<xsl:param name="media"></xsl:param>
<xsl:choose>
<xsl:when test="$media">
Émission 
<xsl:choose>
<xsl:when test="$media = 'audio'">audio</xsl:when>
<xsl:when test="$media = 'video'">vidéo</xsl:when>
</xsl:choose>
.
</xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="emProducedBy">
<xsl:param name="pb"></xsl:param>
<xsl:choose>
<xsl:when test="$pb">
Produit par <xsl:value-of select="$pb" />.
</xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:template>

</xsl:stylesheet>
