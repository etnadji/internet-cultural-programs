# Internet Cultural Programs

## Français

Une (énorme) liste d’émissions culturelles et/ou sérieuses sur Internet.

Voir la liste complète :

- [sur ce dépôt](Emissions.md)
- [sur un site dedié](https://etnadji.frama.io)
- [sur mon site](https://etnadji.fr/rsc/emissions/reco.xml)

Cette liste comporte actuellement 413 émissions,
dont 51 en langues étrangères.

## English

A (giant) list of cultural / serious shows available on the Internet.

View the full list :

- [on this repository](Emissions.md)
- [on a dedicated website](https://etnadji.frama.io)
- [on my website](https://etnadji.fr/rsc/emissions/reco.xml)

The 413 shows listed here are mostly in 
french, but there is also 51 shows in
english / other languages.

## License

[![CC0](http://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, [Etienne Nadji](https://etnadji.fr) has waived all copyright and related or neighboring rights to this work.

## Sommaire / Table of Contents


* Arts (171)
    * Architecture (2)
    * Arts appliqués (3)
    * Arts plastiques (13)
    * Cinéma (45)
    * Création (5)
    * Divers (8)
    * Gastronomie (3)
    * Jeu-video (15)
    * Littérature (21)
    * Musique (35)
    * Séries (8)
    * Thématiques (5)
    * Théâtre et jeu de rôle (8)
* Collectifs (9)
* Collectifs institutionnels (25)
* Culture web (2)
* Divers (5)
* Politique (33)
    * Droit (8)
    * Philosophie (8)
    * Scepticisme (16)
    * Urbanisme (1)
* Sciences humaines (87)
    * Anthropologie (2)
    * Divers (3)
    * Ethnologie (2)
    * Géographie (4)
    * Histoire / Archéologie (56)
    * Informatique (2)
    * Linguistique (11)
    * Sociologie (7)
* Sciences naturelles (81)
    * Biologie / Paléontologie (18)
    * Divers (24)
    * Mathématiques (5)
    * Médecine (7)
    * Physique / Astronomie (16)
    * Psychologie (6)
    * Écologie (1)
    * Électronique (2)
    * Éthologie (2)

## Stats

### Hébergement des vidéos / Videos hosting

- Peertube : 16
- Youtube : 379
- Dailymotion : 5
