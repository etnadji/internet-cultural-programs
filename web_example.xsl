<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<!--
     Example XSL file for gitub.com/etnadji/internet-cultural-programs.
-->


<xsl:template match="/">
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>Émissions culturelles sur Internet</title>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet" />
    <link rel='stylesheet' type='text/css' href='style.css'/>
  </head>
  <body>
    <div id="content">

      <header>
	<hr/>
	<h1>Émissions culturelles sur Internet</h1>
	<hr/>
      </header>

      <h2>À propos</h2>

      <h3>Contenu</h3>

      <p>
	Ce  document a  pour ambition  de lister  la plupart  des émissions
	culturelles, ou  tout du moins  «&#160;sérieuses&#160;» présentes
	sur Internet.
      </p>

      <p>
	À  ce jour, <xsl:value-of select="count(//emission)"/> émissions
	font partie de cette liste. Dont <xsl:value-of select="count(//lang[.!='fr'])"/> en
	langue étrangère.
      </p>

      <h4>Langues</h4>

	<p>
	  Français - <xsl:value-of select="count(//lang[.='fr'])"/><br/>
	  Anglais - <xsl:value-of select="count(//lang[.='en'])"/><br/>
	  Espéranto - <xsl:value-of select="count(//lang[.='eo'])"/><br/>
	</p>

      <h3>Legende</h3>

      <p>Ce document n’est qu’une liste d’émissions avec un nombre variable de liens en rapport avec chaque émission.</p>
      <p>Pour s’y retrouver, chaque lien est préfixé d’une icone.</p>

      <h4>Icones</h4>

      <!--<p>&#xf269; Sites web divers.</p>-->
      <p><img src="img/firefox.png" /> Sites web divers.</p>
      <p><img src="img/rss.png"/> Flux <a href="http://sametmax.com/quest-ce-quun-flux-rss-et-a-quoi-ca-sert/">RSS</a> seul.</p>
      <p>&#xf1c8; Youtube, liens directs vers de la vidéo.</p>
      <p>&#xf099; Compte Twitter.</p>
      <p>&#xf1ed; Moyens de soutenir l’émission: Tipee, vente de T-Shirts, livres…</p>

      <h3>OPML</h3>
      <p>
	Une version OPML, mise à jour de temps en temps est 
	disponible <a href="http://etnadji.fr/emissions/reco.opml">ici</a>.
      </p>
      <p>
	Cette version exclut tout les liens ne pointant pas vers du RSS mais
	comporte  des  liens  RSS  créés  automatiquement  à  partir  des
	adresses de chaines Youtube.
      </p>
      <p>
	Pour une obtenir une version  OPML forcément à jour, téléchargez
	<a href="http://etnadji.fr/emissions/reco.xml">ce même document XML</a>
	et transformez le à l’aide de 
	<a href="http://etnadji.fr/emissions/toOpml.py">ce script Python</a>.
      </p>

      <hr/>

      <h2>Sommaire</h2>

      <!-- Table des matières -->

      <xsl:for-each select="/emissions/category">
      <xsl:sort select="@name" order="ascending" data-type="text" />
      <p class="toc">

	<a href="#{@anchor}">
	  <span class="toch">
	    <xsl:value-of select="@name" />
	  </span>
	</a>
	<xsl:choose>
	  <xsl:when test="count(./emission) = 0">&#160;<xsl:value-of select="count(./*/emission)"/></xsl:when>
	  <xsl:otherwise>&#160;<xsl:value-of select="count(./emission)"/></xsl:otherwise>
	</xsl:choose>
	<br/>

	<xsl:for-each select="subCategory">
	<xsl:sort select="@name" order="ascending" data-type="text" />
	<a href="#{@anchor}">
	  <span class="toci">
	    <xsl:value-of select="@name" />
	  </span>
	</a>
	<xsl:if test="count(./emission)">
	  &#160;<xsl:value-of select="count(./emission)"/>
	</xsl:if>
	<br/>
	</xsl:for-each>
      </p>
      </xsl:for-each>

      <!-- Contenu -->

      <xsl:for-each select="/emissions/category">
      <xsl:sort select="@name" order="ascending" data-type="text" />
	<hr/>
	<h2 id="{@anchor}"><xsl:value-of select="@name" /></h2>

	<xsl:apply-templates select="emission"/>

	<xsl:for-each select="subCategory">
	<xsl:sort select="@name" order="ascending" data-type="text" />
	  <h3 id="{@anchor}"><xsl:value-of select="@name" /></h3>
	  <xsl:apply-templates select="emission"/>
	</xsl:for-each>
      </xsl:for-each>

      <hr/>
      <footer>Dernière mise à jour: <xsl:value-of  select="/emissions/update"/>.</footer>

    </div>
  </body>
</html>
</xsl:template>

<xsl:template match="/emissions/category/emission">
	<xsl:choose>
	  <xsl:when test="@id">
	    <h3 id="{@id}"><xsl:value-of select="title" /></h3>
	  </xsl:when>
	  <xsl:otherwise>
	    <h3><xsl:value-of select="title" /></h3>
	  </xsl:otherwise>
	</xsl:choose>
	<p class="desc"><xsl:value-of select="desc" /></p>
	<xsl:call-template name="emLang">
	  <xsl:with-param name="lang"><xsl:value-of select="lang" /></xsl:with-param>
	</xsl:call-template>
	<xsl:call-template name="emMedia">
	  <xsl:with-param name="media"><xsl:value-of select="@media" /></xsl:with-param>
	</xsl:call-template>
	<xsl:if test="producedBy">
	  <xsl:call-template name="emProducedBy">
	    <xsl:with-param name="pb"><xsl:value-of select="producedBy" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:if>
	<ul>
	<xsl:for-each select="links/url">
	<xsl:sort select="@type" order="ascending" data-type="text" />
	<xsl:choose>
	<xsl:when test="@type = 'web'">
	  <xsl:call-template name="emLinkWeb">
	    <xsl:with-param name="link"><xsl:value-of select="." /></xsl:with-param>
	    <xsl:with-param name="rss"><xsl:value-of select="@rssUrl" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:when>
	<xsl:when test="@type = 'rss'">
	  <xsl:choose>
	    <xsl:when test="@text">
	      <li><img src="img/rss.png"/> <a href="{.}"><xsl:value-of select="@text" /></a></li>
	    </xsl:when>
	    <xsl:otherwise>
	      <li><img src="img/rss.png"/> <a href="{.}"><xsl:value-of select="." /></a></li>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:when>
	<xsl:when test="@type = 'yt'">
	  <li>
	  <xsl:choose>
	    <xsl:when test="@subType = 'channel'">
	      <img src="img/media.png" /> <a href="https://www.youtube.com/channel/{.}"> youtube.com/channel/<xsl:value-of select="." /></a>
	    </xsl:when>
	    <xsl:when test="@subType = 'user'">
	      <img src="img/media.png" /> <a href="https://www.youtube.com/user/{.}"> youtube.com/user/<xsl:value-of select="." /></a>
	    </xsl:when>
	    <xsl:otherwise>
	      <img src="img/media.png" /> <a href="{.}"><xsl:value-of select="." /></a>
	    </xsl:otherwise>
	  </xsl:choose>
	  <xsl:text> </xsl:text>
	  <xsl:choose>
	    <xsl:when test="@subType = 'user'">
	      <a href="https://www.youtube.com/feeds/videos.xml?user={.}"><img src="img/rss.png" /></a>
	    </xsl:when>
	    <xsl:when test="@subType = 'channel'">
	      <a href="https://www.youtube.com/feeds/videos.xml?channel_id={.}"><img src="img/rss.png" /></a>
	    </xsl:when>
	  </xsl:choose>
	  </li>
	</xsl:when>
	<xsl:when test="@type = 'dlm'">
	  <li>
	      <img src="img/media.png" /> <a href="https://www.dailymotion.com/{.}"> dailymotion.com/<xsl:value-of select="." /></a>
	      <xsl:text> </xsl:text>
	      <a href="https://www.dailymotion.com/rss/user/{.}"><img src="img/rss.png" /></a>
	  </li>
	</xsl:when>
	<xsl:when test="@type = 'tw'">
	  <li><img src="img/twitter.png" /> <a href="https://twitter.com/{.}">@<xsl:value-of select="." /></a></li>
	</xsl:when>
	<xsl:when test="@type = 'money'">
	  <xsl:choose>
	    <xsl:when test="@subType = 'tipee'">
	      <li>
		<img src="img/money.png" />
		<a href="https://www.tipeee.com/{.}">
		tipeee.com/<xsl:value-of select="." />
		</a>
	      </li>
	    </xsl:when>
	    <xsl:otherwise>
	      <li><img src="img/money.png" /> <a href="{.}"><xsl:value-of select="." /></a></li>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:when>
	<xsl:when test="@type = 'pt'">
	    <xsl:if test="@instance">
	        <li>
		    <a href="https://{@instance}/accounts/{.}/videos">
	                <xsl:value-of select="." />@<xsl:value-of select="@instance" />
	            </a>
		    <a href="https://{@instance}/feeds/videos.xml?accountId={@id}">
		      <img src="img/rss.png" />
		    </a>
	        </li>
	    </xsl:if>
	</xsl:when>
	<xsl:when test="@type = 'live'">
	  <xsl:choose>
	    <xsl:when test="@subType = 'twitch'">
	      <li>
		<img src="img/live.png" />
		<a href="https://www.twitch.tv/{.}">
		twitch.tv/<xsl:value-of select="." />
		</a>
	      </li>
	    </xsl:when>
	    <xsl:otherwise>
	      <li><img src="img/live.png" /> <a href="{.}"><xsl:value-of select="." /></a></li>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:when>
	<xsl:otherwise>
	  <li><a href="{.}"><xsl:value-of select="." /></a></li>
	</xsl:otherwise>
	</xsl:choose>
	</xsl:for-each>
	</ul>
</xsl:template>

<xsl:template name="emLang">
  <xsl:param name="lang"></xsl:param>
  <xsl:choose>
    <xsl:when test="$lang">
	<p class="desc">Émission en 
	<xsl:choose>
	  <xsl:when test="$lang = 'en'">anglais.</xsl:when>
	  <xsl:when test="$lang = 'fr'">français.</xsl:when>
	  <xsl:when test="$lang = 'eo'">espéranto.</xsl:when>
	</xsl:choose>
	</p>
    </xsl:when>
    <xsl:otherwise></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="emMedia">
  <xsl:param name="media"></xsl:param>
  <xsl:choose>
    <xsl:when test="$media">
	<p class="desc">Émission 
	<xsl:choose>
	  <xsl:when test="$media = 'audio'">audio.</xsl:when>
	  <xsl:when test="$media = 'video'">vidéo.</xsl:when>
	</xsl:choose>
	</p>
    </xsl:when>
    <xsl:otherwise></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="emProducedBy">
  <xsl:param name="pb"></xsl:param>
  <xsl:choose>
    <xsl:when test="$pb">
    <p class="desc">Produit par <xsl:value-of select="$pb" />.</p>
    </xsl:when>
    <xsl:otherwise></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="emLinkWeb">
  <xsl:param name="link"></xsl:param>
  <xsl:param name="rss"></xsl:param>
  <xsl:choose>
    <xsl:when test="$rss">
      <li>
      <img src="img/firefox.png"/> <a href="$link"><xsl:value-of select="$link" /></a><xsl:text> </xsl:text>
      <a href="{$link}{$rss}"><img src="img/rss.png" /></a>
      </li>
    </xsl:when>
    <xsl:otherwise>
      <li><img src="img/firefox.png"/> <a href="{$link}"><xsl:value-of select="$link" /></a></li>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="/emissions/category/subCategory/emission">
	  <xsl:choose>
	    <xsl:when test="@id">
	      <h4 id="{@id}"><xsl:value-of select="title" /></h4>
	    </xsl:when>
	    <xsl:otherwise>
	      <h4><xsl:value-of select="title" /></h4>
	    </xsl:otherwise>
	  </xsl:choose>
	  <p class="desc"><xsl:value-of select="desc" /></p>
	  <xsl:call-template name="emLang">
	    <xsl:with-param name="lang"><xsl:value-of select="lang" /></xsl:with-param>
	  </xsl:call-template>
	  <xsl:call-template name="emMedia">
	    <xsl:with-param name="media"><xsl:value-of select="@media" /></xsl:with-param>
	  </xsl:call-template>
	  <xsl:if test="producedBy">
	    <xsl:call-template name="emProducedBy">
	      <xsl:with-param name="pb"><xsl:value-of select="producedBy" /></xsl:with-param>
	    </xsl:call-template>
	  </xsl:if>
	  <ul>
	  <xsl:for-each select="links/url">
	  <xsl:sort select="@type" order="ascending" data-type="text" />
	  <xsl:choose>
	  <xsl:when test="@type = 'web'">
	    <xsl:call-template name="emLinkWeb">
	      <xsl:with-param name="link"><xsl:value-of select="." /></xsl:with-param>
	      <xsl:with-param name="rss"><xsl:value-of select="@rssUrl" /></xsl:with-param>
	    </xsl:call-template>
	  </xsl:when>
	  <xsl:when test="@type = 'rss'">
	    <xsl:choose>
	      <xsl:when test="@text">
		<li><img src="img/rss.png"/> <a href="{.}"><xsl:value-of select="@text" /></a></li>
	      </xsl:when>
	      <xsl:otherwise>
		<li><img src="img/rss.png"/> <a href="{.}"><xsl:value-of select="." /></a></li>
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:when>
	  <xsl:when test="@type = 'yt'">
	    <li>
	    <xsl:choose>
	      <xsl:when test="@subType = 'channel'">
		<img src="img/media.png" /> <a href="https://www.youtube.com/channel/{.}"> youtube.com/channel/<xsl:value-of select="." /></a>
	      </xsl:when>
	      <xsl:when test="@subType = 'user'">
		<img src="img/media.png" /> <a href="https://www.youtube.com/user/{.}"> youtube.com/user/<xsl:value-of select="." /></a>
	      </xsl:when>
	      <xsl:otherwise>
		<img src="img/media.png" /> <a href="{.}"><xsl:value-of select="." /></a>
	      </xsl:otherwise>
	    </xsl:choose>
	    <xsl:text> </xsl:text>
	    <xsl:choose>
	      <xsl:when test="@subType = 'user'">
		<a href="https://www.youtube.com/feeds/videos.xml?user={.}"><img src="img/rss.png" /></a>
	      </xsl:when>
	      <xsl:when test="@subType = 'channel'">
		<a href="https://www.youtube.com/feeds/videos.xml?channel_id={.}"><img src="img/rss.png" /></a>
	      </xsl:when>
	    </xsl:choose>
	    </li>
	  </xsl:when>
	  <xsl:when test="@type = 'dlm'">
	    <li>
		<img src="img/media.png" /> <a href="https://www.dailymotion.com/{.}"> dailymotion.com/<xsl:value-of select="." /></a>
		<xsl:text> </xsl:text>
		<a href="https://www.dailymotion.com/rss/user/{.}"><img src="img/rss.png" /></a>
	    </li>
	  </xsl:when>
	  <xsl:when test="@type = 'tw'">
	    <li><img src="img/twitter.png" /> <a href="https://twitter.com/{.}">@<xsl:value-of select="." /></a></li>
	  </xsl:when>
	  <xsl:when test="@type = 'money'">
	    <xsl:choose>
	      <xsl:when test="@subType = 'tipee'">
		<li>
		  <img src="img/money.png" />
		  <a href="https://www.tipeee.com/{.}">
		  tipeee.com/<xsl:value-of select="." />
		  </a>
		</li>
	      </xsl:when>
	      <xsl:when test="@subType = 'utip'">
		<li>
		  <a href="https://utip.io{.}">
		  utip.io/<xsl:value-of select="." />
		  </a>
		</li>
	      </xsl:when>
	      <xsl:when test="@subType = 'patreon'">
		<li>
		  <a href="https://www.patreon.com/{.}">
		  patreon.com/<xsl:value-of select="." />
		  </a>
		</li>
	      </xsl:when>
	      <xsl:otherwise>
		<li><img src="img/money.png" /> <a href="{.}"><xsl:value-of select="." /></a></li>
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:when>
	  <xsl:when test="@type = 'live'">
	    <xsl:choose>
	      <xsl:when test="@subType = 'twitch'">
		<li>
		  <img src="img/live.png" />
		  <a href="https://www.twitch.tv/{.}">
		  twitch.tv/<xsl:value-of select="." />
		  </a>
		</li>
	      </xsl:when>
	      <xsl:otherwise>
		<li><img src="img/live.png" /> <a href="{.}"><xsl:value-of select="." /></a></li>
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:when>
	  <xsl:otherwise>
	    <li><a href="{.}"><xsl:value-of select="." /></a></li>
	  </xsl:otherwise>
	  </xsl:choose>
	  </xsl:for-each>
	  </ul>
</xsl:template>

</xsl:stylesheet>
